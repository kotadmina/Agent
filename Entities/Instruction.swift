//
//  Instruction.swift
//  Agent
//
//  Created by Sher Locked on 06.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

enum SocialImage: String {
    case instagram = "instagram_icon"
    case ok = "ok_icon"
}

struct Instruction {
    var text: String
    var images: [SocialImage]
    
    init(text: String, images: [SocialImage] = []) {
        self.text = text
        self.images = images
    }
    
}
