//
//  User.swift
//  Agent
//
//  Created by Sher Locked on 15.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct User {
    var name: String
    var surname: String
    var avatarImageName: String
    var levelCount: Int
    var experienceCount: Int
    var cityInfo: String
    var email: String
    var userInfo: UserInformation
}
