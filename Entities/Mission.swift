//
//  Mission.swift
//  Agent
//
//  Created by Sher Locked on 31.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

enum MissionStatus {
    case ready
    case inProcess
    case open
}

struct Mission {
    
    var headerText: String
    var shortDescriptionText: String
    var exampleScreenshotName: String
    var nickname: String
    var comment: String
    var id: String
    var headerImageName: String
    var greetings: String
    var description: String
    var category: String
    var instuctions: [Instruction]
    var rewards: [Reward]
    var status: MissionStatus
    var dateWhenReady: Date?
    var dateWhenTaken: Date?
    var expCount: Int
    var latitude: Double?
    var longitude: Double?
}
