//
//  Terms.swift
//  Agent
//
//  Created by Sher Locked on 14.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct Terms {
    var titleText: String
    var text: String
}
