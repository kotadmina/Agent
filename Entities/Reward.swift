//
//  Reward.swift
//  Agent
//
//  Created by Sher Locked on 17.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct Reward {
    var description: String
    var expireDate: Date?
    var rewardImageName: String?
    var rewardCategoryId: String?
    var rewardCost: String?
    
    init(description: String, expireDate: Date?, rewardImageName: String?, rewardCategoryId: String?) {
        self.description = description
        self.expireDate = expireDate
        self.rewardImageName = rewardImageName
        self.rewardCategoryId = rewardCategoryId
        self.rewardCost = nil
    }
    
    init(description: String, expireDate: Date?, rewardImageName: String?, rewardCost: String?) {
        self.description = description
        self.expireDate = expireDate
        self.rewardImageName = rewardImageName
        self.rewardCategoryId = nil
        self.rewardCost = rewardCost
    }
}
