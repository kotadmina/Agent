//
//  UserInformation.swift
//  Agent
//
//  Created by sisupov on 19.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

enum Gender: String {
    case male = "MALE"
    case female = "FEMALE"
    case notDetermined
}

struct UserInformation {
    var gender: Gender
    var instagramLink: String?
    var vkLink: String?
    var facebookLink: String?
    var okLink: String?
    var birthday: Date?
    
    init(gender: Gender,
         instagramLink: String? = nil,
         vkLink: String? = nil,
         facebookLink: String? = nil,
         okLink: String? = nil,
         birthday: Date? = nil) {
        self.gender = gender
        self.instagramLink = instagramLink
        self.vkLink = vkLink
        self.facebookLink = facebookLink
        self.okLink = okLink
        self.birthday = birthday
    }
}
