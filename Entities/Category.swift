//
//  Category.swift
//  Agent
//
//  Created by Sher Locked on 29.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

struct Category {
    var name: String
    var imageName: String
    var textColor: UIColor
    var id: String
    var iconName: String
}
