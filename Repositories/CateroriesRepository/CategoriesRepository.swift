//
//  CategoriesRepository.swift
//  Agent
//
//  Created by Sher Locked on 19.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class CatergoriesRepository: CategoriesRepositoryProtocol {
    func getCategories() -> [Category] {
        let cat1 = Category(name: "Выставки", imageName: "exhibitionImage", textColor: ColorsLibrary.mainDarkViolet, id: "1", iconName: "exhibition_cat_icon")
        let cat2 = Category(name: "Авто", imageName: "autoImage", textColor: UIColor.white, id: "2", iconName: "auto_cat_icon")
        let cat3 = Category(name: "Путешествия", imageName: "travelImage", textColor: UIColor.white, id: "3", iconName: "travel_cat_icon")
        let cat4 = Category(name: "Шоппинг", imageName: "shoppingImage", textColor: ColorsLibrary.mainDarkViolet, id: "4", iconName: "shopping_cat_icon")
        let cat5 = Category(name: "Кафе", imageName: "cafeImage", textColor: UIColor.white, id: "5", iconName: "cafe_cat_icon")
        let cat6 = Category(name: "Кино", imageName: "cinemaImage", textColor: ColorsLibrary.mainDarkViolet, id: "6", iconName: "cinema_cat_icon")
        let cat7 = Category(name: "Спорт", imageName: "sportImage", textColor: UIColor.white, id: "7", iconName: "sport_cat_icon")
        let cat8 = Category(name: "Красота", imageName: "beautyImage", textColor: UIColor.white, id: "8", iconName: "beauty_cat_icon")
        let cat9 = Category(name: "Здоровье", imageName: "healthImage", textColor: ColorsLibrary.mainDarkViolet, id: "9", iconName: "health_cat_icon")
        return [cat1, cat2, cat3, cat4, cat5, cat6, cat7, cat8, cat9]
    }
    
    
}
