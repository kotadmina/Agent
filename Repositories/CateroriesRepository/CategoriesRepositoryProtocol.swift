//
//  CategoriesRepositoryProtocol.swift
//  Agent
//
//  Created by Sher Locked on 19.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol CategoriesRepositoryProtocol {
    func getCategories() -> [Category]
}
