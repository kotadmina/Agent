//
//  UserRepository.swift
//  Agent
//
//  Created by Sher Locked on 13.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class UserRepository: UserRepositoryProtocol {
    func getUser() -> User {
        let birthdayFormatter = DateFormatter()
        birthdayFormatter.dateFormat = "dd.MM.yyyy"
        let birthday = birthdayFormatter.date(from: "23.09.1995")
        let userInfo = UserInformation(gender: .female,
                                       instagramLink: nil,
                                       vkLink: "vk.com/testagent01",
                                       facebookLink: nil,
                                       okLink: nil,
                                       birthday: birthday)
        let user = User(name: "Анна", surname: "Груздь", avatarImageName: "avatar", levelCount: 5, experienceCount: 5, cityInfo: "Москва, Россия", email: "gruzd90@mail.ru", userInfo: userInfo)
        return user
    }
}
