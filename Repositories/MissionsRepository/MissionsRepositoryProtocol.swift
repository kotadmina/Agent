//
//  MissionsRepositoryProtocol.swift
//  Agent
//
//  Created by Sher Locked on 19.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol MissionsRepositoryProtocol {
    func getFeaturedMissions() -> [Mission]
    func getAllMissions() -> [Mission]
    func getMissions(by category: Category) -> [Mission]
    func getReadyMissions() -> [Mission]
    func getInProcessMissions() -> [Mission]
}
