//
//  OpenCategoryHeaderCell.swift
//  Agent
//
//  Created by Sher Locked on 09.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class OpenCategoryHeaderCell: UITableViewHeaderFooterView {

    @IBOutlet weak var categoryMainImage: UIImageView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var categoryNumberOfMissionLabel: UILabel!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var backButton: UIButton!
    
    weak var delegate: OpenCategoryHeaderDelegate?
    
    func configure(with viewModel: CategoryHeaderCellViewModel) {
        categoryView.backgroundColor = UIColor.clear
        categoryMainImage.image = UIImage(named: viewModel.mainImageName)
        categoryMainImage.contentMode = .scaleAspectFill
        categoryMainImage.clipsToBounds = true
        categoryNameLabel.text = viewModel.categoryNameText
        categoryNameLabel.font = FontLibrary.pragmaticaCondRegular(size: 36)
        categoryNameLabel.textColor = UIColor.white
        categoryNumberOfMissionLabel.text = viewModel.missionCount?.pluralForm(str1: "миссия",
                                                                               str2: "миссии",
                                                                               str3: "миссий",
                                                                               additionalString: "")
        
        categoryNumberOfMissionLabel.font = FontLibrary.pragmaticaCondLight(size: 18)
        categoryNumberOfMissionLabel.textColor = UIColor.white
        let backImage = UIImage(named: "back_button")?.withRenderingMode(.alwaysOriginal)
        backButton.setImage(backImage, for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 15, right: 25)
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
    }
    
    @objc func backButtonPressed() {
        delegate?.buttonPressed()
    }
    
}

protocol OpenCategoryHeaderDelegate: class {
    func buttonPressed()
}
