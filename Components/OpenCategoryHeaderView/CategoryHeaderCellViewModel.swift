//
//  CategoryHeaderCellViewModel.swift
//  Agent
//
//  Created by Sher Locked on 09.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct CategoryHeaderCellViewModel {
    
    var mainImageName: String
    var categoryNameText: String
    var missionCount: Int?
    
}
