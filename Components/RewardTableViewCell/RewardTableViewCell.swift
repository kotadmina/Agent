//
//  RewardTableViewCell.swift
//  Agent
//
//  Created by Sher Locked on 07.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class RewardTableViewCell: UITableViewCell {

    @IBOutlet weak var rewardTextLabel: UILabel!
    @IBOutlet weak var starImageView: UIImageView!
    
    func configure(with viewModel: RewardCellViewModel) {
        starImageView.image = UIImage(named: "star_icon")
        rewardTextLabel.text = viewModel.rewardText
        rewardTextLabel.font = FontLibrary.pragmaticaExtraLightReg(size: 12)
        rewardTextLabel.textColor = UIColor.white
        selectionStyle = .none
    }
}
