//
//  MissionFooterView.swift
//  Agent
//
//  Created by Sher Locked on 07.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class MissionFooterView: UIView {
    
    var readyButton: UIButton!
    var notNowButton: UIButton!
    var availabilityInformationView: UIView!
    var missionAvailableLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createReadyButton()
        createNotNowButton()
        createInfoView()
        createMissionAvailableLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createReadyButton()
        createNotNowButton()
        createInfoView()
        createMissionAvailableLabel()
    }
    
    override func draw(_ rect: CGRect) {
        let firstLine = UIBezierPath()
        firstLine.move(to: CGPoint(x: 0, y: 0.55 * bounds.height))
        firstLine.addLine(to: CGPoint(x: 0.73 * bounds.width, y: bounds.height))
        firstLine.close()
        ColorsLibrary.mainYellow.set()
        firstLine.lineWidth = 3.0
        firstLine.stroke()
        firstLine.fill()
        
        let secondLine = UIBezierPath()
        secondLine.move(to: CGPoint(x: bounds.width, y: 0))
        secondLine.addLine(to: CGPoint(x: 0, y: 0.9 * bounds.height))
        secondLine.close()
        ColorsLibrary.mainYellow.set()
        secondLine.lineWidth = 5.0
        secondLine.stroke()
        secondLine.fill()
    }
    
    func createReadyButton() {
        readyButton = UIButton()
        readyButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(readyButton)
        NSLayoutConstraint.activate([
            readyButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            readyButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            readyButton.widthAnchor.constraint(equalToConstant: 76),
            readyButton.heightAnchor.constraint(equalToConstant: 76)])
        let readyImage = UIImage(named: "readyButton")?.withRenderingMode(.alwaysOriginal)
        readyButton.setImage(readyImage, for: .normal)
    }
    
    func createNotNowButton() {
        notNowButton = UIButton()
        notNowButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(notNowButton)
        NSLayoutConstraint.activate([
            notNowButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            notNowButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -35),
            notNowButton.widthAnchor.constraint(equalToConstant: 75),
            notNowButton.heightAnchor.constraint(equalToConstant: 26)])
        notNowButton.setTitle("Не сейчас", for: .normal)
        notNowButton.titleLabel?.font = FontLibrary.pragmaticaCondLight(size: 20)
        notNowButton.setTitleColor(UIColor.white, for: .normal)
    }
    
    func createInfoView() {
        availabilityInformationView = UIView()
        availabilityInformationView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(availabilityInformationView)
        NSLayoutConstraint.activate([
            availabilityInformationView.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            availabilityInformationView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            availabilityInformationView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.4),
            availabilityInformationView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.3)])
        availabilityInformationView.backgroundColor = UIColor.clear
    }
    
    func createMissionAvailableLabel() {
        missionAvailableLabel = UILabel()
        missionAvailableLabel.translatesAutoresizingMaskIntoConstraints = false
        availabilityInformationView.addSubview(missionAvailableLabel)
        NSLayoutConstraint.activate([
            missionAvailableLabel.topAnchor.constraint(equalTo: availabilityInformationView.topAnchor),
            missionAvailableLabel.leadingAnchor.constraint(equalTo: availabilityInformationView.leadingAnchor)])
        missionAvailableLabel.text = "*Миссия доступна"
        missionAvailableLabel.font = FontLibrary.pragmaticaExtraLightReg(size: 10)
        missionAvailableLabel.textColor = UIColor.white
    }
    
}
