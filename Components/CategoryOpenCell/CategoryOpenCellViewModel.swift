//
//  CategoryOpenCellViewModel.swift
//  Agent
//
//  Created by Sher Locked on 09.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct CategoryOpenCellViewModel {
    
    var missionImageName: String
    var missionDescription: String
    var missionId: String
    var dateString: String?
    var starCount: String?
    
    init(missionImageName: String, missionDescription: String, missionId: String, dateString: String? = nil, starCount: String? = nil) {
        self.missionImageName = missionImageName
        self.missionDescription = missionDescription
        self.missionId = missionId
        self.dateString = dateString
        self.starCount = starCount
    }
}
