//
//  CategoryOpenCell.swift
//  Agent
//
//  Created by Sher Locked on 09.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class CategoryOpenCell: UITableViewCell {

    @IBOutlet weak var missionImage: UIImageView!
    @IBOutlet weak var informationView: UIView!
    @IBOutlet weak var missionForAgentLabel: UILabel!
    @IBOutlet weak var missionDescriptionLabel: UILabel!
    @IBOutlet weak var additionalView: UIView!
    @IBOutlet weak var additionalViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var starNumbersLabel: UILabel!
    
    func configure(with viewModel: CategoryOpenCellViewModel) {
        missionImage.image = UIImage(named: viewModel.missionImageName)
        missionImage.contentMode = .top
        missionImage.clipsToBounds = true
        informationView.backgroundColor = UIColor.white
        missionForAgentLabel.text = "Миссия для агента:"
        missionForAgentLabel.font = FontLibrary.pragmaticaCondLight(size: 18)
        missionForAgentLabel.textColor = ColorsLibrary.mainDarkViolet
        missionDescriptionLabel.text = viewModel.missionDescription
        missionDescriptionLabel.font = FontLibrary.pragmaticaCondLight(size: 24)
        missionDescriptionLabel.textColor = ColorsLibrary.mainDarkViolet
        if let dateString = viewModel.dateString, let starString = viewModel.starCount {
            additionalView.isHidden = false
            additionalViewHeightConstraint.constant = 25
            timeLabel.textColor = ColorsLibrary.mainDarkViolet
            timeLabel.text = dateString
            timeLabel.font = FontLibrary.pragmaticaCondRegular(size: 18)
            starNumbersLabel.textColor = ColorsLibrary.mainDarkViolet
            starNumbersLabel.font = FontLibrary.pragmaticaCondRegular(size: 18)
            starNumbersLabel.text = starString
        } else {
            additionalView.isHidden = true
            additionalViewHeightConstraint.constant = 0
        }
    }
}
