//
//  TaskRewardTableCellViewModel.swift
//  Agent
//
//  Created by Sher Locked on 13.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

struct TaskRewardTableCellViewModel {
    
    var rewardName: String
    var categoryImageName: String?
    var cost: String?
    
}


