//
//  TaskRewardTableViewCell.swift
//  Agent
//
//  Created by Sher Locked on 13.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class TaskRewardTableViewCell: UITableViewCell {

    @IBOutlet weak var rewardView: UIView!
    @IBOutlet weak var rewardName: UILabel!
    @IBOutlet weak var rewardContentView: UIView!
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var costLabel: UILabel!
    
    func configure(with viewModel: TaskRewardTableCellViewModel) {
        if let categoryImageName = viewModel.categoryImageName {
            categoryImage.isHidden = false
            costLabel.isHidden = true
            categoryImage.image = UIImage(named: categoryImageName)
            costLabel.text = nil
        } else {
            categoryImage.isHidden = true
            costLabel.isHidden = false
            costLabel.text = viewModel.cost
            categoryImage.image = nil
        }
        rewardName.text = viewModel.rewardName
        
        stylize()
    }
    
    private func stylize() {
        rewardView.layer.cornerRadius = 8
        rewardView.layer.borderWidth = 1
        rewardView.layer.borderColor = ColorsLibrary.mainYellow.cgColor
        rewardView.backgroundColor = ColorsLibrary.rewardCellYellow
        rewardName.textColor = UIColor.white
        rewardName.font = FontLibrary.pragmaticaCondRegular(size: 18)
        rewardContentView.backgroundColor = UIColor.clear
        categoryImage.backgroundColor = UIColor.clear
        costLabel.textColor = UIColor.white
        costLabel.font = FontLibrary.pragmaticaCondRegular(size: 18)
        costLabel.textAlignment = .right
        selectionStyle = .none
    }
}
