//
//  ProfileTripleSegment.swift
//  Agent
//
//  Created by Sher Locked on 13.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

enum ProfileTripleSegmentState {
    case inProcessMission
    case readyMission
    case rewards
}

@IBDesignable class ProfiletripleSegment: UIView {
    
    var state: ProfileTripleSegmentState = .inProcessMission
    var firstButton: UIButton!
    var secondButton: UIButton!
    var thirdButton: UIButton!
    
    weak var delegate: TripleSegmentDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureFirstButton()
        configureThirdButton()
        configureSecondButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureFirstButton()
        configureThirdButton()
        configureSecondButton()
    }
    
    func configureFirstButton() {
        firstButton = UIButton(type: .custom)
        firstButton.adjustsImageWhenHighlighted = false
        let firstImage = UIImage(named: "firstTripleSegmentButtonImage")?.withRenderingMode(.alwaysTemplate)
        firstButton.setImage(firstImage, for: .normal)
        firstButton.tintColor = ColorsLibrary.mainYellow
        firstButton.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(firstButton)
        
        firstButton.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        firstButton.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        firstButton.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        firstButton.trailingAnchor.constraint(equalTo: self.centerXAnchor, constant: -self.frame.width / 6).isActive = true
        
        firstButton.addTarget(self, action: #selector(firstButtonPressed), for: .touchUpInside)
    }
    
    func configureThirdButton() {
        thirdButton = UIButton(type: .custom)
        thirdButton.adjustsImageWhenHighlighted = false
        let thirdImage = UIImage(named: "thirdTripleSegmentButtonImage")?.withRenderingMode(.alwaysTemplate)
        thirdButton.setImage(thirdImage, for: .normal)
        thirdButton.tintColor = ColorsLibrary.mainYellow
        thirdButton.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(thirdButton)
        
        thirdButton.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        thirdButton.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        thirdButton.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        thirdButton.leadingAnchor.constraint(equalTo: self.centerXAnchor, constant: self.frame.width / 6).isActive = true
        
        thirdButton.addTarget(self, action: #selector(thirdButtonPressed), for: .touchUpInside)
    }
    
    func configureSecondButton() {
        secondButton = UIButton(type: .custom)
        secondButton.adjustsImageWhenHighlighted = false
        let secondImage = UIImage(named: "secondTripleSegmentButtonImage")?.withRenderingMode(.alwaysTemplate)
        secondButton.setImage(secondImage, for: .normal)
        secondButton.tintColor = ColorsLibrary.mainYellow
        secondButton.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(secondButton)
        
        secondButton.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        secondButton.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        secondButton.leadingAnchor.constraint(equalTo: self.centerXAnchor, constant: -self.frame.width / 6).isActive = true
        secondButton.trailingAnchor.constraint(equalTo: self.centerXAnchor, constant: self.frame.width / 6).isActive = true
        
        secondButton.addTarget(self, action: #selector(secondButtonPressed), for: .touchUpInside)
    }
    
    @objc func firstButtonPressed() {
        state = .inProcessMission
        firstButton.tintColor = UIColor.white
        secondButton.tintColor = ColorsLibrary.mainYellow
        thirdButton.tintColor = ColorsLibrary.mainYellow
        secondButton.setNeedsDisplay()
        firstButton.setNeedsDisplay()
        thirdButton.setNeedsDisplay()
        delegate?.segmentChanged(newState: .inProcessMission)
    }
    
    @objc func secondButtonPressed() {
        state = .readyMission
        secondButton.tintColor = UIColor.white
        firstButton.tintColor = ColorsLibrary.mainYellow
        thirdButton.tintColor = ColorsLibrary.mainYellow
        secondButton.setNeedsDisplay()
        firstButton.setNeedsDisplay()
        thirdButton.setNeedsDisplay()
        delegate?.segmentChanged(newState: .readyMission)
    }
    
    @objc func thirdButtonPressed() {
        state = .rewards
        secondButton.tintColor = ColorsLibrary.mainYellow
        firstButton.tintColor = ColorsLibrary.mainYellow
        thirdButton.tintColor = UIColor.white
        secondButton.setNeedsDisplay()
        firstButton.setNeedsDisplay()
        thirdButton.setNeedsDisplay()
        delegate?.segmentChanged(newState: .rewards)

    }
    
}

protocol TripleSegmentDelegate: class {
    func segmentChanged(newState: ProfileTripleSegmentState)
}
