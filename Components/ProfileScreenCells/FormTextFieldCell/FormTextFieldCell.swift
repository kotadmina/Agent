//
//  FormTextFieldCell.swift
//  Agent
//
//  Created by Sher Locked on 03.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

protocol FormTextFieldCellDelegate: class {
    func textFieldTapped(id: String)
}

class FormTextFieldCell: UITableViewCell {

    @IBOutlet weak var fieldNameLabel: UILabel!
    @IBOutlet weak var infoTextField: UITextField!
    @IBOutlet weak var separatorView: UIView!
    
    weak var delegate: FormTextFieldCellDelegate?
    var id: String?
    
    func configure(with viewModel: ProfileScreenCellViewModel) {
        guard let viewModel = viewModel as? FormTextFieldViewModel else {
            return
        }
        infoTextField.delegate = self
        id = viewModel.id
        fieldNameLabel.textColor = UIColor.white
        fieldNameLabel.font = FontLibrary.pragmaticaCondLight(size: 24)
        fieldNameLabel.text = viewModel.name
        separatorView.backgroundColor = ColorsLibrary.mainYellow
        infoTextField.font = FontLibrary.pragmaticaCondLight(size: 20)
        infoTextField.textColor = UIColor.white
        infoTextField.text = viewModel.value
    }
    
}

extension FormTextFieldCell: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let id = id else {
            return
        }
        delegate?.textFieldTapped(id: id)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
