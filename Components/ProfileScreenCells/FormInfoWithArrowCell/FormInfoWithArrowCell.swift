//
//  FormInfoWithArrowCell.swift
//  Agent
//
//  Created by Sher Locked on 05.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class FormInfoWithArrowCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    func configure(with viewModel: ProfileScreenCellViewModel) {
        guard let viewModel = viewModel as? FormInfoWithArrowViewModel else {
            return
        }
        nameLabel.textColor = UIColor.white
        nameLabel.font = FontLibrary.pragmaticaCondLight(size: 24)
        nameLabel.text = viewModel.name
        infoLabel.textColor = ColorsLibrary.mainYellow
        infoLabel.textAlignment = .right
        infoLabel.font = FontLibrary.pragmaticaCondRegular(size: 24)
        infoLabel.text = viewModel.value
    }
    
    
}
