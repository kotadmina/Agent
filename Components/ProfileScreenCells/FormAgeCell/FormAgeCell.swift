//
//  FormAgeCell.swift
//  Agent
//
//  Created by Sher Locked on 04.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class FormAgeCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateOfBirthTextField: UITextField!
    
    
    func configure(with viewModel: ProfileScreenCellViewModel) {
        guard let viewModel = viewModel as? FormAgeViewModel else {
            return
        }
        nameLabel.textColor = UIColor.white
        nameLabel.font = FontLibrary.pragmaticaCondLight(size: 24)
        nameLabel.text = viewModel.name
        dateOfBirthTextField.textColor = ColorsLibrary.mainYellow
        dateOfBirthTextField.font = FontLibrary.pragmaticaCondRegular(size: 24)
        dateOfBirthTextField.text = viewModel.value
    }
    
}
