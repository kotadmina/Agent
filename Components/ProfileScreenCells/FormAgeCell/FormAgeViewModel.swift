//
//  FormAgeViewModel.swift
//  Agent
//
//  Created by Sher Locked on 05.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

struct FormAgeViewModel: ProfileScreenCellViewModel {
    
    var type: CellType = .formAgeCell
    var height: CGFloat = 40
    var id: String
    var name: String
    var value: String?
    
    init(name: String, value: String?, id: String) {
        self.name = name
        self.value = value
        self.id = id
    }
    
}

