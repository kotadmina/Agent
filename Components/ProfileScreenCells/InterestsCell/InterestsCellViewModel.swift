//
//  InterestsCellViewModel.swift
//  Agent
//
//  Created by Sher Locked on 10.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

struct InterestsCellViewModel: ProfileScreenCellViewModel {
    
    var type: CellType = .collectionViewCell
    var height: CGFloat = 50
    var id: String
    var collectionViewModels: [InterestCollectionCellViewModel]
    
    init(id: String, collectionViewModels: [InterestCollectionCellViewModel]) {
        self.id = id
        self.collectionViewModels = collectionViewModels
    }
    
    
}
