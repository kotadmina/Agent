//
//  InterestsCell.swift
//  Agent
//
//  Created by Sher Locked on 09.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

protocol InterestsCellDelegate: class {
    func didPressCategory(with id: String)
}

class InterestsCell: UITableViewCell {

    @IBOutlet weak var interestsCollectionView: UICollectionView!
    var viewModels: [InterestCollectionCellViewModel] = []
    
    weak var delegate: InterestsCellDelegate?
    
    func configure(with viewModel: ProfileScreenCellViewModel) {
        guard let viewModel = viewModel as? InterestsCellViewModel else {
            return
        }
        interestsCollectionView.delegate = self
        interestsCollectionView.dataSource = self
        let nib = UINib(nibName: "InterestCollectionViewCell", bundle: nil)
        interestsCollectionView.register(nib, forCellWithReuseIdentifier: "InterestCollectionViewCell")
        viewModels = viewModel.collectionViewModels
        interestsCollectionView.reloadData()
    }
    
    
}

extension InterestsCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewModel = viewModels[indexPath.row]
        delegate?.didPressCategory(with: viewModel.id)
    }
}

extension InterestsCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = interestsCollectionView.dequeueReusableCell(withReuseIdentifier: "InterestCollectionViewCell", for: indexPath) as! InterestCollectionViewCell
        cell.configure(with: viewModels[indexPath.row])
        return cell
    }
}

extension InterestsCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 19
        let itemWidth = (interestsCollectionView.frame.size.width - 3 * padding) / 4
        
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 19
    }
}
