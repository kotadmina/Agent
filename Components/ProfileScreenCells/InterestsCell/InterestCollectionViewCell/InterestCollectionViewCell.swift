//
//  InterestCollectionViewCell.swift
//  Agent
//
//  Created by Sher Locked on 10.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class InterestCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    func configure(with viewModel: InterestCollectionCellViewModel) {
        cellBackgroundView.layer.borderWidth = 1.0
        cellBackgroundView.layer.cornerRadius = 5
        cellBackgroundView.clipsToBounds = true
        cellBackgroundView.layer.borderColor = ColorsLibrary.grey.cgColor
        mainImage.clipsToBounds = true
        mainImage.contentMode = .scaleAspectFit
        mainImage.image = UIImage(named: viewModel.imageName)?.withRenderingMode(.alwaysTemplate)
        nameLabel.text = viewModel.intetestName
        nameLabel.font = FontLibrary.pragmaticaCondLight(size: 14)
        nameLabel.textColor = viewModel.isSelected ? UIColor.white : ColorsLibrary.mainYellow
        mainImage.tintColor = viewModel.isSelected ? UIColor.white : ColorsLibrary.mainYellow
    }
    

}
