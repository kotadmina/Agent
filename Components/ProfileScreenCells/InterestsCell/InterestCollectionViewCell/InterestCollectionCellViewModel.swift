//
//  InterestCollectionCellViewModel.swift
//  Agent
//
//  Created by Sher Locked on 10.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct InterestCollectionCellViewModel {
    var id: String
    var imageName: String
    var intetestName: String
    var isSelected: Bool
}
