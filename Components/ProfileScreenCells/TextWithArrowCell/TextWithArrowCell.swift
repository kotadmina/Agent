//
//  TextWithArrowCell.swift
//  Agent
//
//  Created by Sher Locked on 29.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class TextWithArrowCell: UITableViewCell {

    @IBOutlet weak var actionNameLabel: UILabel!
    
    func configure(with viewModel: ProfileScreenCellViewModel) {
        guard let viewModel = viewModel as? TextWithArrowViewModel else {
            return
        }
        actionNameLabel.textColor = UIColor.white
        actionNameLabel.font = FontLibrary.pragmaticaCondLight(size: 24)
        actionNameLabel.text = viewModel.name
    }
    
}
