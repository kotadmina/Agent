//
//  TextWithArrowViewModel.swift
//  Agent
//
//  Created by Sher Locked on 29.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

struct TextWithArrowViewModel: ProfileScreenCellViewModel {
    var type: CellType = .textCell
    var height: CGFloat = 40
    var name: String
    var id: String
    
    init(name: String, id: String) {
        self.name = name
        self.id = id
    }
    
}
