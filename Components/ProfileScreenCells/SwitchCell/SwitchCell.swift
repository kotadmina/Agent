//
//  SwitchCell.swift
//  Agent
//
//  Created by Sher Locked on 02.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

protocol SwitchCellDelegate: class {
    func switchChanged(id: String, isOn: Bool)
}

class SwitchCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var switchView: UISwitch!
    
    weak var delegate: SwitchCellDelegate?
    var id: String?
    
    func configure(with viewModel: ProfileScreenCellViewModel) {
        guard let viewModel = viewModel as? SwitchCellViewModel else {
            return
        }
        switchView.backgroundColor = UIColor.white
        switchView.layer.cornerRadius = switchView.frame.height / 2
        id = viewModel.id
        nameLabel.text = viewModel.name
        nameLabel.textColor = UIColor.white
        nameLabel.font = FontLibrary.pragmaticaCondLight(size: 24)
        switchView.isOn = viewModel.switchOn
        switchView.addTarget(self, action: #selector(switchChanged), for: .valueChanged)
    }
    
    @objc func switchChanged() {
        delegate?.switchChanged(id: id ?? "", isOn: switchView.isOn)
    }
}
