//
//  SwitchCellViewModel.swift
//  Agent
//
//  Created by Sher Locked on 02.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit


struct SwitchCellViewModel: ProfileScreenCellViewModel {
    var type: CellType = .switchCell
    var height: CGFloat = 40
    var name: String
    var id: String
    var switchOn: Bool
    
    init(name: String, id: String, switchOn: Bool) {
        self.name = name
        self.id = id
        self.switchOn = switchOn
    }
}
