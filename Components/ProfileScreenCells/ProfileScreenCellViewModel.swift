//
//  ProfileScreenCellViewModel.swift
//  Agent
//
//  Created by Sher Locked on 29.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

enum CellType {
    case textCell
    case switchCell
    case textFieldCell
    case formAgeCell
    case formGenderCell
    case formInfoCell
    case collectionViewCell
    case mapCell
}

protocol ProfileScreenCellViewModel {
    var type: CellType { get }
    var height: CGFloat { get }
    var id: String { get }
}
