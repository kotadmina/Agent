//
//  MapViewCell.swift
//  Agent
//
//  Created by Sher Locked on 10.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import YandexMapKit

class MapViewCell: UITableViewCell {

    @IBOutlet weak var mapView: YMKMapView!
    @IBOutlet weak var mapPlusView: UIImageView!
    @IBOutlet weak var mapMinusView: UIImageView!
    
    func configure(with viewModel: ProfileScreenCellViewModel) {
        guard let viewModel = viewModel as? MapViewViewModel else {
            return
        }
        mapView.mapWindow.map?.mapObjects?.clear()
        if let homeLocation = viewModel.homeLocation {
            addPlacemark(with: "map_icon_home", point: homeLocation)
        }
        if let workLocation = viewModel.workLocation {
            addPlacemark(with: "map_icon_work", point: workLocation)
        }
        configureControls()
    }
    
    private func addPlacemark(with imageName: String, point: YMKPoint) {
        let placemark = mapView.mapWindow.map?.mapObjects?
            .addPlacemark(with: point, image: UIImage(named: imageName))
        
        placemark?.setIconStyleWith(YMKIconStyle(anchor: nil,
                                                 rotationType: nil,
                                                 zIndex: nil,
                                                 flat: nil,
                                                 visible: nil,
                                                 scale: 1,
                                                 tappableArea: nil))
    }
    
    private func configureControls() {
        let plusGesture = UITapGestureRecognizer(target: self, action: #selector(plusButtonPressed))
        mapPlusView.addGestureRecognizer(plusGesture)
        mapPlusView.isUserInteractionEnabled = true
        mapPlusView.image = UIImage(named: "map_icon_plus")
        
        let minusGesture = UITapGestureRecognizer(target: self, action: #selector(minusButtonPressed))
        mapMinusView.addGestureRecognizer(minusGesture)
        mapMinusView.isUserInteractionEnabled = true
        mapMinusView.image = UIImage(named: "map_icon_minus")
    }
    
    @objc private func plusButtonPressed() {
        guard let cameraPosition = mapView.mapWindow.map?.cameraPosition else {
            return
        }
        let newZoom = cameraPosition.zoom + 1
        let newCameraPosition = YMKCameraPosition(target: cameraPosition.target, zoom: newZoom, azimuth: cameraPosition.azimuth, tilt: cameraPosition.tilt)
        let animation = YMKAnimation(type: .smooth, duration: 0.3)
        mapView.mapWindow.map?.move(with: newCameraPosition, animationType: animation, cameraCallback: nil)
    }
    
    @objc private func minusButtonPressed() {
        guard let cameraPosition = mapView.mapWindow.map?.cameraPosition else {
            return
        }
        let newZoom = cameraPosition.zoom - 1
        let newCameraPosition = YMKCameraPosition(target: cameraPosition.target, zoom: newZoom, azimuth: cameraPosition.azimuth, tilt: cameraPosition.tilt)
        let animation = YMKAnimation(type: .smooth, duration: 0.3)
        mapView.mapWindow.map?.move(with: newCameraPosition, animationType: animation, cameraCallback: nil)
    }
    
}
