//
//  MapViewViewModel.swift
//  Agent
//
//  Created by Sher Locked on 13.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation
import YandexMapKit

struct MapViewViewModel: ProfileScreenCellViewModel {
    
    var type: CellType = .mapCell
    var height: CGFloat = 328
    var id: String
    var homeLocation: YMKPoint?
    var workLocation: YMKPoint?
    
    init(id: String, homeLocation: YMKPoint?, workLocation: YMKPoint?) {
        self.id = id
        self.homeLocation = homeLocation
        self.workLocation = workLocation
    }
    
}
