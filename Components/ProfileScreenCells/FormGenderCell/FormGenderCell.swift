//
//  FormGenderCell.swift
//  Agent
//
//  Created by Sher Locked on 05.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class FormGenderCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderSegmentControl: UISegmentedControl!
    
    func configure(with viewModel: ProfileScreenCellViewModel) {
        
        guard let viewModel = viewModel as? FormGenderViewModel else {
            return
        }
        
        nameLabel.textColor = UIColor.white
        nameLabel.font = FontLibrary.pragmaticaCondLight(size: 24)
        nameLabel.text = viewModel.name
        genderSegmentControl.tintColor = ColorsLibrary.mainYellow
        let segmentFont = FontLibrary.pragmaticaCondRegular(size: 23)
        genderSegmentControl.setTitleTextAttributes([NSAttributedStringKey.font: segmentFont],
                                                    for: .normal)
        genderSegmentControl.setTitle("Мужской", forSegmentAt: 0)
        genderSegmentControl.setTitle("Женский", forSegmentAt: 1)
        switch viewModel.gender {
        case .male:
            genderSegmentControl.selectedSegmentIndex = 0
        case .female:
            genderSegmentControl.selectedSegmentIndex = 1
        case .notDetermined:
            genderSegmentControl.selectedSegmentIndex = UISegmentedControlNoSegment
        }
    }
    
    
}
