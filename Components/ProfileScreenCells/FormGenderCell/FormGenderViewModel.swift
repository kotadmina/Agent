//
//  FormGenderViewModel.swift
//  Agent
//
//  Created by Sher Locked on 05.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

struct FormGenderViewModel: ProfileScreenCellViewModel {
    
    var type: CellType = .formGenderCell
    var height: CGFloat = 40
    var id: String
    var name: String
    var gender: Gender
    
    init(name: String, id: String, gender: Gender) {
        self.name = name
        self.id = id
        self.gender = gender
    }
    
}
