//
//  MissionMapView.swift
//  Agent
//
//  Created by Sher Locked on 27.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class MissionMapView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var missionBackgroundImage: UIImageView!
    @IBOutlet weak var missionForLabel: UILabel!
    @IBOutlet weak var missionHeaderLabel: UILabel!
    @IBOutlet weak var missionAdress: UILabel!
    @IBOutlet weak var expLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("MissionMapView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func configure(with viewModel: OpenMissionMapViewModel) {
        missionBackgroundImage.image = UIImage(named: viewModel.backgroundImageName)
        missionBackgroundImage.contentMode = .top
        missionBackgroundImage.clipsToBounds = true
        missionForLabel.text = "Миссия для агента:"
        missionForLabel.textColor = ColorsLibrary.mainDarkViolet
        missionForLabel.font = FontLibrary.pragmaticaCondLight(size: 18)
        missionHeaderLabel.text = viewModel.headerString
        missionHeaderLabel.textColor = ColorsLibrary.mainDarkViolet
        missionHeaderLabel.font = FontLibrary.pragmaticaCondLight(size: 24)
        missionAdress.text = viewModel.adressString
        missionAdress.textColor = ColorsLibrary.mainDarkViolet
        missionAdress.font = FontLibrary.pragmaticaCondRegular(size: 18)
        expLabel.text = "\(viewModel.expCount)"
        expLabel.textColor = ColorsLibrary.mainDarkViolet
        expLabel.font = FontLibrary.pragmaticaRegular(size: 18)
    }
    

}
