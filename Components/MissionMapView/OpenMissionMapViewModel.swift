//
//  OpenMissionMapViewModel.swift
//  Agent
//
//  Created by Sher Locked on 27.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct OpenMissionMapViewModel {
    var backgroundImageName: String
    var headerString: String
    var expCount: Int
    var adressString: String
}
