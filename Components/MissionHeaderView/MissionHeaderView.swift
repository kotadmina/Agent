//
//  MissionHeaderView.swift
//  Agent
//
//  Created by Sher Locked on 02.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class MissionHeaderView: UIView {
    
    var maskLayer = CAShapeLayer()
    var headerImage: UIImageView!
    var lineView: MissionHeaderLineView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        addImageView()
        addLineView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addImageView()
        addLineView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let path = UIBezierPath()
        path.move(to: CGPoint(x: headerImage.bounds.width, y: 0))
        path.addLine(to: CGPoint(x: headerImage.bounds.width, y: 0.9 * headerImage.bounds.height))
        path.addLine(to: CGPoint(x: 0.143 * headerImage.bounds.width, y: headerImage.bounds.height * 0.6))
        path.addLine(to: CGPoint(x: 0.41 * headerImage.bounds.width, y: 0))
        path.addLine(to: CGPoint(x: headerImage.bounds.width, y: 0))
        maskLayer.path = path.cgPath
        headerImage.layer.mask = maskLayer
        setNeedsDisplay()
    }
    
    func addImageView() {
        headerImage = UIImageView()
        headerImage.translatesAutoresizingMaskIntoConstraints = false
        addSubview(headerImage)
        NSLayoutConstraint.activate([
            headerImage.topAnchor.constraint(equalTo: topAnchor),
            headerImage.trailingAnchor.constraint(equalTo: trailingAnchor),
            headerImage.heightAnchor.constraint(equalTo: heightAnchor),
            headerImage.widthAnchor.constraint(equalTo: widthAnchor)])
        headerImage.contentMode = .scaleAspectFill
        headerImage.clipsToBounds = true
        headerImage.backgroundColor = UIColor.green
    }
    
 
    
    func addLineView() {
        lineView = MissionHeaderLineView()
        lineView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(lineView)
        NSLayoutConstraint.activate([
            lineView.leadingAnchor.constraint(equalTo: leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: trailingAnchor),
            lineView.bottomAnchor.constraint(equalTo: bottomAnchor),
            lineView.topAnchor.constraint(equalTo: topAnchor)])
        lineView.backgroundColor = UIColor.clear
    }
    
    func configure(with viewModel: MissionHeaderViewModel) {
        headerImage.image = UIImage(named: viewModel.headerImageName)
    }

}
