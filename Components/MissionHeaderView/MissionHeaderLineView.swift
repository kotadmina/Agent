//
//  MissionHeaderLineView.swift
//  Agent
//
//  Created by Sher Locked on 06.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class MissionHeaderLineView: UIView {
    
    override func draw(_ rect: CGRect) {
        let firstLine = UIBezierPath()
        firstLine.move(to: CGPoint(x: 0, y: 0.9 * bounds.height))
        firstLine.addLine(to: CGPoint(x: 0.41 * bounds.width, y: 0))
        firstLine.close()
        ColorsLibrary.mainYellow.set()
        firstLine.lineWidth = 3.0
        firstLine.stroke()
        firstLine.fill()
        
        
        let secondLine = UIBezierPath()
        secondLine.move(to: CGPoint(x: 0, y: 0.55 * bounds.height))
        secondLine.addLine(to: CGPoint(x: bounds.width, y: 0.9 * bounds.height))
        secondLine.close()
        ColorsLibrary.mainYellow.set()
        secondLine.lineWidth = 7.0
        secondLine.stroke()
        secondLine.fill()
    }
    
}
