//
//  MissionHeaderViewModel.swift
//  Agent
//
//  Created by Sher Locked on 06.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

struct MissionHeaderViewModel {
    
    var headerImageName: String
    
    init(headerImageName: String) {
        self.headerImageName = headerImageName
        
    }
    
}
