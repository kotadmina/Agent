//
//  CollectionHeaderView.swift
//  Agent
//
//  Created by Sher Locked on 20.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class CollectionHeaderView: UICollectionReusableView {

    @IBOutlet weak var headerLabel: UILabel!
    
    func configure() {
        headerLabel.textColor = ColorsLibrary.mainYellow
        headerLabel.text = "Награды:"
        headerLabel.font = FontLibrary.pragmaticaCondLight(size: 30)
    }
    
    
}
