//
//  RewardsCollectionViewCell.swift
//  Agent
//
//  Created by Sher Locked on 17.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class RewardsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewWithImage: UIView!
    @IBOutlet weak var rewardImage: UIImageView!
    @IBOutlet weak var rewardDescription: UILabel!
    @IBOutlet weak var dateInfoLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    
    func configure(with viewModel: RewardCollectionCellViewModel) {
        viewWithImage.layer.cornerRadius = 9
        viewWithImage.clipsToBounds = true
        rewardImage.contentMode = .scaleAspectFill
        rewardImage.image = UIImage(named: viewModel.rewardImageName)
        rewardDescription.textColor = ColorsLibrary.mainYellow
        rewardDescription.font = FontLibrary.pragmaticaCondLight(size: 20)
        rewardDescription.text = viewModel.rewardText
        dateInfoLabel.textColor = ColorsLibrary.mainYellow
        dateInfoLabel.font = FontLibrary.pragmaticaCondRegular(size: 14)
        guard let date = viewModel.date else {
            return
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.YYYY"
        let dateString = dateFormatter.string(from: date)
        dateInfoLabel.text = "до " + dateString
    }
    
}
