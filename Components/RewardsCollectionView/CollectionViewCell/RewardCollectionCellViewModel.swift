//
//  RewardCollectionCellViewModel.swift
//  Agent
//
//  Created by Sher Locked on 17.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct RewardCollectionCellViewModel {
    
    var rewardImageName: String
    var rewardText: String
    var date: Date?
    
}
