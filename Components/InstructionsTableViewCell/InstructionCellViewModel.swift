//
//  InstructionCellViewModel.swift
//  Agent
//
//  Created by Sher Locked on 06.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct InstructionCellViewModel {
    var number: String
    var text: String
    var images: [String]
}
