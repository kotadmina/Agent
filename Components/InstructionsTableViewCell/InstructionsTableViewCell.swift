//
//  InstructionsTableViewCell.swift
//  Agent
//
//  Created by Sher Locked on 06.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class InstructionsTableViewCell: UITableViewCell {

    @IBOutlet weak var circleImage: UIImageView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var instructionLabel: UILabel!
    
    func configure(with viewModel: InstructionCellViewModel) {
        circleImage.image = UIImage(named: "circle")
        numberLabel.text = viewModel.number
        numberLabel.textColor = ColorsLibrary.mainYellow
        numberLabel.font = FontLibrary.pragmaticaRegular(size: 12)
        instructionLabel.textColor = UIColor.white
        instructionLabel.font = FontLibrary.pragmaticaExtraLightReg(size: 13)
        let fullString = NSMutableAttributedString(string: viewModel.text)
        for image in viewModel.images {
            let spaceString = NSAttributedString(string: " ")
            fullString.append(spaceString)
            let attachment = NSTextAttachment()
            attachment.image = UIImage(named: image)
            attachment.bounds = CGRect(x: 0, y: 0, width: 14, height: 14)
            let attachmentString = NSAttributedString(attachment: attachment)
            fullString.append(attachmentString)
        }
        instructionLabel.attributedText = fullString
        selectionStyle = .none
    }
    
}
