//
//  CategoriesIconViewModel.swift
//  Agent
//
//  Created by Sher Locked on 20.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct CategoriesIconViewModel {
    var id: String?
    var iconName: String
    var isSelected: Bool
}
