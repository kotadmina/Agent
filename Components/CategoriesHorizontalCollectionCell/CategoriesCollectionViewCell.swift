//
//  CategoriesCollectionViewCell.swift
//  Agent
//
//  Created by Sher Locked on 20.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var categoryImageView: UIImageView!
    
    func configure(with viewModel: CategoriesIconViewModel) {
        let iconColor = viewModel.isSelected ? UIColor.white : ColorsLibrary.mainYellow
        categoryImageView.image = UIImage(named: viewModel.iconName)?.withRenderingMode(.alwaysTemplate)
        categoryImageView.tintColor = iconColor
        categoryImageView.contentMode = .scaleAspectFit
    }
    
}
