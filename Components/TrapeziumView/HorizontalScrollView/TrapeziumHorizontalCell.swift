//
//  TrapeziumHorizontalCell.swift
//  Agent
//
//  Created by Sher Locked on 30.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class TrapeziumHorizontalCell: TrapeziumView {
    
    var backgroundImage: UIImageView!
    var backgroundImageLeadingConstraint: NSLayoutConstraint!
    var backgroundImageTrailingConstraint: NSLayoutConstraint!
    var titleLabel: UILabel!
    var informationView: UIView!
    var headerView: UIView!
    var headerLabel: UILabel!
    var descriptionLabelLeadingConstraint: NSLayoutConstraint!
    var descriptionLabelTrailingConstraint: NSLayoutConstraint!
    var descriptionLabel: UILabel!
    var instagramView: UIView!
    var hearthIcon: UIImageView!
    var hearthIconLeadingConstraint: NSLayoutConstraint!
    var commentIcon: UIImageView!
    var bookMarkIcon: UIImageView!
    var bookmarkIconTrailingConstraint: NSLayoutConstraint!
    var missionTaskLabel: UILabel!
    var missionLabelLeadingConstraint: NSLayoutConstraint!
    var missionLabelTrailingConstraint: NSLayoutConstraint!
    
    var viewModel: TrapeziumHorizontalCellViewModel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createBackgroundImage()
        setInfoView()
        setHeaderView()
        createDescriptionLabel()
        createHeaderLabel()
        createInstagramView()
        addHearthIcon()
        addCommentIcon()
        addBookmarkIcon()
        createMissionTaskLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createBackgroundImage()
        setInfoView()
        setHeaderView()
        createDescriptionLabel()
        createHeaderLabel()
        createInstagramView()
        addHearthIcon()
        addCommentIcon()
        addBookmarkIcon()
        createMissionTaskLabel()
    }
    
    func createBackgroundImage() {
        backgroundImage = UIImageView()
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        addSubview(backgroundImage)
        backgroundImageLeadingConstraint = backgroundImage.leadingAnchor.constraint(equalTo: leadingAnchor)
        backgroundImageTrailingConstraint = backgroundImage.trailingAnchor.constraint(equalTo: trailingAnchor)
        NSLayoutConstraint.activate([
            backgroundImage.topAnchor.constraint(equalTo: topAnchor),
            backgroundImageLeadingConstraint,
            backgroundImageTrailingConstraint,
            backgroundImage.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5)])
        backgroundImage.contentMode = .scaleAspectFill
        backgroundImage.backgroundColor = UIColor.white
        backgroundImage.clipsToBounds = true
    }
    
    func setInfoView() {
        informationView = UIView()
        informationView.translatesAutoresizingMaskIntoConstraints = false
        informationView.backgroundColor = UIColor.white
        addSubview(informationView)
        NSLayoutConstraint.activate([
            informationView.leadingAnchor.constraint(equalTo: leadingAnchor),
            informationView.trailingAnchor.constraint(equalTo: trailingAnchor),
            informationView.bottomAnchor.constraint(equalTo: bottomAnchor),
            informationView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5)])
    }
    
    func setHeaderView() {
        headerView = UIView()
        headerView.translatesAutoresizingMaskIntoConstraints = false
        informationView.addSubview(headerView)
        NSLayoutConstraint.activate([
            headerView.leadingAnchor.constraint(equalTo: informationView.leadingAnchor),
            headerView.trailingAnchor.constraint(equalTo: informationView.trailingAnchor),
            headerView.topAnchor.constraint(equalTo: informationView.topAnchor),
            headerView.heightAnchor.constraint(equalTo: informationView.heightAnchor, multiplier: 0.45)])
    }
    
    func createDescriptionLabel() {
        descriptionLabel = UILabel()
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(descriptionLabel)
        descriptionLabelLeadingConstraint = descriptionLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 25)
        descriptionLabelTrailingConstraint = descriptionLabel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -25)
        NSLayoutConstraint.activate([
            descriptionLabelLeadingConstraint,
            descriptionLabelTrailingConstraint,
            descriptionLabel.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 25)])
        descriptionLabel.font = FontLibrary.pragmaticaCondLight(size: 12)
    }
    
    func createHeaderLabel() {
        headerLabel = UILabel()
        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(headerLabel)
        NSLayoutConstraint.activate([
            headerLabel.leadingAnchor.constraint(equalTo: descriptionLabel.leadingAnchor),
            headerLabel.trailingAnchor.constraint(equalTo: descriptionLabel.trailingAnchor),
            headerLabel.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 7)])
        headerLabel.font = FontLibrary.pragmaticaCondLight(size: 15)
    }
    
    func createInstagramView() {
        instagramView = UIView()
        instagramView.translatesAutoresizingMaskIntoConstraints = false
        informationView.addSubview(instagramView)
        NSLayoutConstraint.activate([
            instagramView.leadingAnchor.constraint(equalTo: informationView.leadingAnchor),
            instagramView.trailingAnchor.constraint(equalTo: informationView.trailingAnchor),
            instagramView.bottomAnchor.constraint(equalTo: informationView.bottomAnchor),
            instagramView.heightAnchor.constraint(equalTo: informationView.heightAnchor, multiplier: 0.55)])
        instagramView.backgroundColor = UIColor.white
        
    }
    
    func addHearthIcon() {
        hearthIcon = UIImageView()
        hearthIcon.translatesAutoresizingMaskIntoConstraints = false
        instagramView.addSubview(hearthIcon)
        hearthIconLeadingConstraint = hearthIcon.leadingAnchor.constraint(equalTo: instagramView.leadingAnchor, constant: 15)
        NSLayoutConstraint.activate([
            hearthIcon.widthAnchor.constraint(equalToConstant: 17),
            hearthIcon.heightAnchor.constraint(equalToConstant: 17),
            hearthIconLeadingConstraint,
            hearthIcon.topAnchor.constraint(equalTo: instagramView.topAnchor, constant: 5)])
        hearthIcon.image = UIImage(named: "hearthIcon")
    }
    
    func addCommentIcon() {
        commentIcon = UIImageView()
        commentIcon.translatesAutoresizingMaskIntoConstraints = false
        instagramView.addSubview(commentIcon)
        NSLayoutConstraint.activate([
            commentIcon.widthAnchor.constraint(equalToConstant: 17),
            commentIcon.heightAnchor.constraint(equalToConstant: 17),
            commentIcon.centerYAnchor.constraint(equalTo: hearthIcon.centerYAnchor),
            commentIcon.leadingAnchor.constraint(equalTo: hearthIcon.trailingAnchor, constant: 10)])
        commentIcon.image = UIImage(named: "commentIcon")
    }
    
    func addBookmarkIcon() {
        bookMarkIcon = UIImageView()
        bookMarkIcon.translatesAutoresizingMaskIntoConstraints = false
        instagramView.addSubview(bookMarkIcon)
        bookmarkIconTrailingConstraint = bookMarkIcon.trailingAnchor.constraint(equalTo: instagramView.trailingAnchor, constant: 15)
        NSLayoutConstraint.activate([
            bookMarkIcon.widthAnchor.constraint(equalToConstant: 17),
            bookMarkIcon.heightAnchor.constraint(equalToConstant: 20),
            bookMarkIcon.centerYAnchor.constraint(equalTo: hearthIcon.centerYAnchor),
            bookmarkIconTrailingConstraint])
        bookMarkIcon.image = UIImage(named: "bookmarkIcon")
    }
    
    func createMissionTaskLabel() {
        missionTaskLabel = UILabel()
        missionTaskLabel.numberOfLines = 3
        missionTaskLabel.translatesAutoresizingMaskIntoConstraints = false
        instagramView.addSubview(missionTaskLabel)
        missionLabelTrailingConstraint = missionTaskLabel.trailingAnchor.constraint(equalTo: instagramView.trailingAnchor, constant: 15)
        missionLabelLeadingConstraint = missionTaskLabel.leadingAnchor.constraint(equalTo: instagramView.leadingAnchor, constant: 15)
        NSLayoutConstraint.activate([
            missionTaskLabel.topAnchor.constraint(equalTo: bookMarkIcon.bottomAnchor),
            missionTaskLabel.bottomAnchor.constraint(equalTo: instagramView.bottomAnchor, constant: -5),
            missionLabelLeadingConstraint,
            missionLabelTrailingConstraint])
    }
    
    func configure(with viewModel: TrapeziumHorizontalCellViewModel) {
        self.viewModel = viewModel
        let image = UIImage(named: viewModel.imageName)
        backgroundImage.image = image
        headerLabel.text = viewModel.headerText
        headerLabel.textColor = viewModel.textColor
        descriptionLabel.text = viewModel.descriptionText
        descriptionLabel.textColor = viewModel.textColor
        headerView.backgroundColor = viewModel.headerViewBackgroundColor
        let fullAttributes: [NSAttributedStringKey: Any] = [.font: UIFont.systemFont(ofSize: 10)]
        let boldAttributes: [NSAttributedStringKey: Any] = [.font: UIFont.systemFont(ofSize: 10, weight: .bold)]
        let fullComment = viewModel.nickname + " " + viewModel.comment
        let attributedComment = NSMutableAttributedString(string: fullComment, attributes: fullAttributes)
        let rangeOfNickname = (fullComment as NSString).range(of: viewModel.nickname)
        attributedComment.addAttributes(boldAttributes, range: rangeOfNickname)
        missionTaskLabel.attributedText = attributedComment
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        descriptionLabelLeadingConstraint.constant = leadingOffset(for: descriptionLabel, point: descriptionLabel.center) + 5
        descriptionLabelTrailingConstraint.constant = -trailingOffset(for: descriptionLabel, point: descriptionLabel.center) - 5
        hearthIconLeadingConstraint.constant = leadingOffset(for: hearthIcon, point: hearthIcon.center) + 15
        bookmarkIconTrailingConstraint.constant = -trailingOffset(for: bookMarkIcon, point: bookMarkIcon.center) - 15
        missionLabelTrailingConstraint.constant = -trailingOffset(for: missionTaskLabel, point: missionTaskLabel.center) - 15
        missionLabelLeadingConstraint.constant = leadingOffset(for: missionTaskLabel, point: missionTaskLabel.center) + 15
        let leftTopImagePoint = CGPoint(x: backgroundImage.frame.minX, y: backgroundImage.frame.minY)
        let rightBottomImagePoint = CGPoint(x: backgroundImage.frame.maxX, y: backgroundImage.frame.maxY)
        backgroundImageLeadingConstraint.constant = leadingOffset(for: backgroundImage, point: leftTopImagePoint)
        backgroundImageTrailingConstraint.constant = -trailingOffset(for: backgroundImage, point: rightBottomImagePoint)
    }
    
    func leadingOffset(for view: UIView, point: CGPoint) -> CGFloat {
        if let controlPoints = controlPoints, let superview = view.superview {
            let viewCenter = superview.convert(point, to: self)
            let topCoeff = viewCenter.y / frame.height
            let offset = frame.width * (controlPoints.topLeft +  (controlPoints.bottomLeft - controlPoints.topLeft) * topCoeff)
            return offset
        } else {
            return 0
        }
    }
    
    func trailingOffset(for view: UIView, point: CGPoint) -> CGFloat {
        if let controlPoints = controlPoints, let superview = view.superview {
            let viewCenter = superview.convert(point, to: self)
            let topCoeff = viewCenter.y / frame.height
            let offset = frame.width * (1 - controlPoints.topRight - topCoeff * (controlPoints.bottomRight - controlPoints.topRight))
            return offset
        } else {
            return 0
        }
    }
    
    
    
}
