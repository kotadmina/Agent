//
//  TrapeziumHorizontalCellViewModel.swift
//  Agent
//
//  Created by Sher Locked on 30.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

struct TrapeziumHorizontalCellViewModel {
    
    var headerText: String
    var imageName: String
    var descriptionText: String
    var headerViewBackgroundColor: UIColor
    var textColor: UIColor
    var nickname: String
    var comment: String
    var id: String
    
    init(headerText: String, imageName: String, descriptionText: String, headerViewBackgroundColor: UIColor, textColor: UIColor, nickname: String, comment: String, id: String) {
        self.headerText = headerText
        self.imageName = imageName
        self.descriptionText = descriptionText
        self.headerViewBackgroundColor = headerViewBackgroundColor
        self.textColor = textColor
        self.nickname = nickname
        self.comment = comment
        self.id = id
    }
    
}
