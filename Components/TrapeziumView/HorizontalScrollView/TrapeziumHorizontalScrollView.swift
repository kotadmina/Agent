//
//  TrapeziumHorizontalScrollView.swift
//  Agent
//
//  Created by Sher Locked on 26.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

protocol TrapeziumHorizontalScrollViewDelegate: class {
    func horizontalItemPressed(with viewModel: TrapeziumHorizontalCellViewModel)
}

class TrapeziumHorizontalScrollView: UIScrollView {
    
    weak var trapeziumDelegate: TrapeziumHorizontalScrollViewDelegate?

    var horizontalItems: [TrapeziumHorizontalCell] = [] {
        didSet {
            reloadViews()
        }
    }
    
    var itemsWidth: CGFloat {
        return 444 * UIScreen.main.bounds.height / 667
    }
    let topCoeff: CGFloat = 0.55
    let bottomCoeff: CGFloat = 0.7
    let padding: CGFloat = 0
    
    func reloadViews() {
        for subview in subviews {
            subview.removeFromSuperview()
        }
        
        if horizontalItems.count == 1, let firstItem = horizontalItems.first {
            let controlPoints = TrapeziumMaskControlPoints(topLeft: 0, topRight: 0, bottomLeft: 1, bottomRight: 1)
            firstItem.createMaskLayer(with: controlPoints)
            firstItem.translatesAutoresizingMaskIntoConstraints = false
            addSubview(firstItem)
            NSLayoutConstraint.activate([
                firstItem.topAnchor.constraint(equalTo: topAnchor),
                firstItem.leadingAnchor.constraint(equalTo: leadingAnchor),
                firstItem.trailingAnchor.constraint(equalTo: trailingAnchor),
                firstItem.heightAnchor.constraint(equalTo: heightAnchor),
                firstItem.centerXAnchor.constraint(equalTo: centerXAnchor),
                firstItem.bottomAnchor.constraint(equalTo: bottomAnchor)])
        } else if horizontalItems.count > 1, let firstItem = horizontalItems.first, let lastItem = horizontalItems.last {
            let firstControlPoints = TrapeziumMaskControlPoints(topLeft: 0, topRight: topCoeff, bottomLeft: 0, bottomRight: bottomCoeff)
            let lastControlPoints = TrapeziumMaskControlPoints(topLeft: 1 - bottomCoeff, topRight: 1, bottomLeft: 1 - topCoeff, bottomRight: 1)
            let middleControlPoints = TrapeziumMaskControlPoints(topLeft: 0, topRight: bottomCoeff, bottomLeft: bottomCoeff - topCoeff, bottomRight: 2 * bottomCoeff - topCoeff)
            
            for (i, item) in horizontalItems.enumerated() {
                if item == firstItem {
                    item.createHorizontalMaskLayer(with: firstControlPoints)
                } else if item == lastItem {
                    item.createHorizontalMaskLayer(with: lastControlPoints)
                } else {
                    item.createHorizontalMaskLayer(with: middleControlPoints)
                }
                item.translatesAutoresizingMaskIntoConstraints = false
                addSubview(item)
                var offset: CGFloat = 0
                if i != 0 {
                    offset = (topCoeff * itemsWidth) + CGFloat(i - 1) * (bottomCoeff * itemsWidth) + CGFloat(i) * padding
                }
                if item == lastItem {
                    offset -= itemsWidth * (1 - bottomCoeff) + 1
                }
                NSLayoutConstraint.activate([
                    item.topAnchor.constraint(equalTo: topAnchor),
                    item.bottomAnchor.constraint(equalTo: bottomAnchor),
                    item.heightAnchor.constraint(equalTo: heightAnchor),
                    item.widthAnchor.constraint(equalToConstant: itemsWidth),
                    item.leadingAnchor.constraint(equalTo: leadingAnchor, constant: offset)])
                if item == lastItem {
                    NSLayoutConstraint.activate([
                        item.trailingAnchor.constraint(equalTo: trailingAnchor)])
                }
            }
        }
        
        for item in horizontalItems {
            item.delegate = self
        }
        
        layoutIfNeeded()
    }

}

extension TrapeziumHorizontalScrollView: TrapeziumViewDelegate {
    func trapeziumViewTapped(view: TrapeziumView) {
        guard let cell = view as? TrapeziumHorizontalCell, let viewModel = cell.viewModel else {
            return
        }
        trapeziumDelegate?.horizontalItemPressed(with: viewModel)
    }
}
