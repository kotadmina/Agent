//
//  TrapeziumView.swift
//  Agent
//
//  Created by Sher Locked on 24.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

struct TrapeziumMaskControlPoints {
    var topLeft: CGFloat
    var topRight: CGFloat
    var bottomLeft: CGFloat
    var bottomRight: CGFloat
}

protocol TrapeziumViewDelegate: class {
    func trapeziumViewTapped(view: TrapeziumView)
}

class TrapeziumView: UIView {
    
    var maskLayer = CAShapeLayer()
    var controlPoints: TrapeziumMaskControlPoints!
    var isVertical: Bool = false
    
    weak var delegate: TrapeziumViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addGestures()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addGestures()
    }
    
    func addGestures() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        self.addGestureRecognizer(gesture)
    }
    
    @objc func viewTapped(_ gesture: UITapGestureRecognizer) {
        delegate?.trapeziumViewTapped(view: self)
    }
    
    func isPointInsideTrapezium(_ point: CGPoint) -> Bool {
        guard let maskPath = maskLayer.path else {
            return true
        }
        let bezierPath = UIBezierPath(cgPath: maskPath)
        return bezierPath.contains(point)
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return isPointInsideTrapezium(point)
    }
    
    func createMaskLayer(with controlPoints: TrapeziumMaskControlPoints) {
        self.controlPoints = controlPoints
        isVertical = true
        let path = UIBezierPath()
        path.move(to: CGPoint(x: bounds.minX, y: bounds.minY + controlPoints.topLeft * bounds.height))
        path.addLine(to: CGPoint(x: bounds.maxX, y: bounds.minY + controlPoints.topRight * bounds.height))
        path.addLine(to: CGPoint(x: bounds.maxX, y: bounds.minY + controlPoints.bottomRight * bounds.height))
        path.addLine(to: CGPoint(x: bounds.minX, y: bounds.minY + controlPoints.bottomLeft * bounds.height))
        maskLayer.path = path.cgPath
    }
    
    func createHorizontalMaskLayer(with controlPoints: TrapeziumMaskControlPoints) {
        self.controlPoints = controlPoints
        isVertical = false
        let path = UIBezierPath()
        path.move(to: CGPoint(x: bounds.minX + controlPoints.topLeft * bounds.width, y: bounds.minY))
        path.addLine(to: CGPoint(x: bounds.minX + controlPoints.topRight * bounds.width, y: bounds.minY))
        path.addLine(to: CGPoint(x: bounds.minY + controlPoints.bottomRight * bounds.width, y: bounds.maxY))
        path.addLine(to: CGPoint(x: bounds.minY + controlPoints.bottomLeft * bounds.width, y: bounds.maxY))
        maskLayer.path = path.cgPath
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.mask = maskLayer
        if let controlPoints = controlPoints {
            if isVertical {
                createMaskLayer(with: controlPoints)
            } else {
                createHorizontalMaskLayer(with: controlPoints)
            }
        }
    }
    

}
