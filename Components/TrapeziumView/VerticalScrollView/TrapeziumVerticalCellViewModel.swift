//
//  TrapeziumVerticalCellViewModel.swift
//  Agent
//
//  Created by Sher Locked on 29.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

struct TrapeziumVerticalCellViewModel {
    
    var name: String
    var imageName: String
    var alignment: NSTextAlignment
    var textColor: UIColor
    var id: String
    
    init(name: String, imageName: String, alignment: NSTextAlignment, textColor: UIColor, id: String) {
        self.name = name
        self.imageName = imageName
        self.alignment = alignment
        self.textColor = textColor
        self.id = id
    }
    
}
