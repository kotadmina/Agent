//
//  TrapeziumScrollView.swift
//  Agent
//
//  Created by Sher Locked on 25.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

protocol TrapeziumVerticalScrollViewDelegate: class {
    func verticalItemPressed(with viewModel: TrapeziumVerticalCellViewModel)
}

class TrapeziumScrollView: UIScrollView {
    
    weak var trapeziumDelegate: TrapeziumVerticalScrollViewDelegate?

    var items: [TrapeziumVerticalCell] = [] {
        didSet {
            reloadViews()
        }
    }
    
    let itemHeight: CGFloat = 225
    let leftCoeff: CGFloat = 0.5
    let rightCoeff: CGFloat = 0.8
    let insetCoeff: CGFloat = 0.25
    let padding: CGFloat = 10
    
    func reloadViews() {
        for subview in subviews {
            subview.removeFromSuperview()
        }
        
        if items.count == 1, let firstItem = items.first {
            let controlPoints = TrapeziumMaskControlPoints(topLeft: 0, topRight: 0, bottomLeft: 1, bottomRight: 1)
            firstItem.createMaskLayer(with: controlPoints)
            firstItem.translatesAutoresizingMaskIntoConstraints = false
            addSubview(firstItem)
            NSLayoutConstraint.activate([
                firstItem.topAnchor.constraint(equalTo: topAnchor),
                firstItem.leadingAnchor.constraint(equalTo: leadingAnchor),
                firstItem.trailingAnchor.constraint(equalTo: trailingAnchor),
                firstItem.heightAnchor.constraint(equalToConstant: itemHeight),
                firstItem.bottomAnchor.constraint(equalTo: bottomAnchor),
                firstItem.widthAnchor.constraint(equalTo: widthAnchor)])
        } else if items.count > 1, let firstItem = items.first, let lastItem = items.last {
            let firstControlPoints = TrapeziumMaskControlPoints(topLeft: 0, topRight: 0, bottomLeft: leftCoeff - insetCoeff, bottomRight: rightCoeff - insetCoeff)
            let lastOddControlPoints = TrapeziumMaskControlPoints(topLeft: 1 - rightCoeff + insetCoeff, topRight: 1 - leftCoeff + insetCoeff, bottomLeft: 1, bottomRight: 1)
            let lastEvenControlPoints = TrapeziumMaskControlPoints(topLeft: 1 - leftCoeff + insetCoeff, topRight: 1 - rightCoeff + insetCoeff, bottomLeft: 1, bottomRight: 1)
            let oddControlPoints = TrapeziumMaskControlPoints(topLeft: 0, topRight: rightCoeff - leftCoeff, bottomLeft: rightCoeff, bottomRight: leftCoeff)
            let evenControlPoint = TrapeziumMaskControlPoints(topLeft: rightCoeff - leftCoeff, topRight: 0, bottomLeft: leftCoeff, bottomRight: rightCoeff)
            
            for (i, item) in items.enumerated() {
                if item == firstItem {
                    item.createMaskLayer(with: firstControlPoints)
                } else if item == lastItem {
                    if i % 2 == 1 {
                        item.createMaskLayer(with: lastOddControlPoints)
                    } else {
                        item.createMaskLayer(with: lastEvenControlPoints)
                    }
                } else if i % 2 == 1 {
                    item.createMaskLayer(with: oddControlPoints)
                } else if i % 2 == 0 {
                    item.createMaskLayer(with: evenControlPoint)
                }
                var offset = CGFloat(i) * (itemHeight * leftCoeff + padding)
                if item != firstItem {
                    offset -= itemHeight * insetCoeff
                }
                if item == lastItem {
                    offset -= itemHeight * (1 - rightCoeff + insetCoeff)
                }
                item.translatesAutoresizingMaskIntoConstraints = false
                addSubview(item)
                NSLayoutConstraint.activate([
                    item.topAnchor.constraint(equalTo: topAnchor, constant: offset),
                    item.leadingAnchor.constraint(equalTo: leadingAnchor),
                    item.trailingAnchor.constraint(equalTo: trailingAnchor),
                    item.heightAnchor.constraint(equalToConstant: itemHeight),
                    item.widthAnchor.constraint(equalTo: widthAnchor)])
                
                if item == lastItem {
                    NSLayoutConstraint.activate([item.bottomAnchor.constraint(equalTo: bottomAnchor)])
                }
            }
        }
        
        for item in items {
            item.delegate = self
        }
        
        layoutIfNeeded()
    }

}

extension TrapeziumScrollView: TrapeziumViewDelegate {
    func trapeziumViewTapped(view: TrapeziumView) {
        guard let cell = view as? TrapeziumVerticalCell, let viewModel = cell.viewModel else {
            return
        }
        trapeziumDelegate?.verticalItemPressed(with: viewModel)
    }
}
