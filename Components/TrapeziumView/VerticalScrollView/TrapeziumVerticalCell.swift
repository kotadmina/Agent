//
//  TrapeziumVerticalCell.swift
//  Agent
//
//  Created by Sher Locked on 28.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class TrapeziumVerticalCell: TrapeziumView {
    
    var backgroundImage: UIImageView!
    var titleLabel: UILabel!
    var labelVerticalConstraint: NSLayoutConstraint!
    
    var viewModel: TrapeziumVerticalCellViewModel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createImageView()
        createLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createImageView()
        createLabel()
    }
    
    func createImageView() {
        backgroundImage = UIImageView()
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        addSubview(backgroundImage)
        NSLayoutConstraint.activate([
            backgroundImage.topAnchor.constraint(equalTo: topAnchor),
            backgroundImage.bottomAnchor.constraint(equalTo: bottomAnchor),
            backgroundImage.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundImage.trailingAnchor.constraint(equalTo: trailingAnchor)])
        backgroundImage.contentMode = .scaleAspectFill
    }
    
    func createLabel() {
        titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(titleLabel)
        labelVerticalConstraint = titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        NSLayoutConstraint.activate([
            labelVerticalConstraint,
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10)])
        titleLabel.font = FontLibrary.pragmaticaCondRegular(size: 36)
    }
    
    func configure(with viewModel: TrapeziumVerticalCellViewModel) {
        let image = UIImage(named: viewModel.imageName)
        backgroundImage.image = image
        titleLabel.text = viewModel.name.uppercased()
        titleLabel.textAlignment = viewModel.alignment
        titleLabel.textColor = viewModel.textColor
        self.viewModel = viewModel
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let controlPoints = controlPoints {
            var offsetFromTop: CGFloat
            switch titleLabel.textAlignment {
            case .left:
                offsetFromTop = frame.height * (controlPoints.topLeft + controlPoints.bottomLeft) / 2
            case .right:
                offsetFromTop = frame.height * (controlPoints.topRight + controlPoints.bottomRight) / 2
            default:
                offsetFromTop = frame.height / 2
            }
            NSLayoutConstraint.deactivate([labelVerticalConstraint])
            labelVerticalConstraint = titleLabel.centerYAnchor.constraint(equalTo: topAnchor, constant: offsetFromTop)
            NSLayoutConstraint.activate([labelVerticalConstraint])
        }
    }
    
}

