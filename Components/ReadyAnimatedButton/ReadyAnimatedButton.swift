//
//  ReadyAnimatedButton.swift
//  Agent
//
//  Created by Sher Locked on 25.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class ReadyAnimatedButton: UIButton {
    
    let animationViewWidth: CGFloat = 50

    override var isHighlighted: Bool {
        didSet {
            super.isHighlighted = false
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        animate {
            self.isHighlighted = false
            self.sendActions(for: .touchUpInside)
        }
    }
    
    func animate(completion: @escaping () -> Void) {
        let animationView = UIView(frame: CGRect(x: -animationViewWidth, y: 0, width: animationViewWidth, height: self.frame.height))
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = animationView.bounds
        let edgeColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.1).cgColor
        let middleColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.6).cgColor
        let centerColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.8).cgColor
        gradientLayer.colors = [edgeColor, middleColor, centerColor, middleColor, edgeColor]
        gradientLayer.startPoint = CGPoint(x: 0.1, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 0.9, y: 0.5)
        let transform = CGAffineTransform.identity.scaledBy(x: 1, y: 2).rotated(by: .pi / 9)
        gradientLayer.setAffineTransform(transform)
        animationView.clipsToBounds = true
        animationView.layer.addSublayer(gradientLayer)
        if let titleLabel = self.titleLabel {
            self.insertSubview(animationView, belowSubview: titleLabel)
        } else {
            self.addSubview(animationView)
        }
        self.layoutIfNeeded()
        UIView.animate(withDuration: 1, animations: {
            animationView.frame = CGRect(x: self.frame.width, y: 0, width: self.animationViewWidth, height: self.frame.height)
        }) { _ in
            animationView.removeFromSuperview()
            completion()
        }
    }
    

}
