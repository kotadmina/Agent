#import <YandexMapKit/YMKSearchResponse.h>

#import <YandexRuntime/YRTPlatformBinding.h>

@interface YMKPointCloudLayer : YRTPlatformBinding

/**
 * Updates the layer.
 *
 * The given response from the online search generates the tile URL
 * pattern from the response. If the pattern was not changed, it does
 * nothing.
 *
 * The given response from offline search requests a new point cloud
 * from offline search. If the point cloud was not updated, it does
 * nothing.
 *
 * In both cases, if the pattern or parameters were changed, it clears
 * the current layer, determines if "point cloud" should be shown, and
 * shows it.
 */
- (void)updateWithResponse:(nonnull YMKSearchResponse *)response;


/**
 * Clears the current "point cloud".
 */
- (void)clear;


/**
 * Switches the layer on the map on or off.
 */
- (void)activate:(BOOL)on;


/**
 * Tells if this object is valid or no. Any method called on an invalid
 * object will throw an exception. The object becomes invalid only on UI
 * thread, and only when its implementation depends on objects already
 * destroyed by now. Please refer to general docs about the interface for
 * details on its invalidation.
 */
@property (nonatomic, readonly, getter=isValid) BOOL valid;

@end

