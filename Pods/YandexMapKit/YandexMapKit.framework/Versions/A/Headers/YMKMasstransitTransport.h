#import <YandexMapKit/YMKMasstransitAlert.h>
#import <YandexMapKit/YMKMasstransitLine.h>
#import <YandexMapKit/YMKMasstransitStop.h>
#import <YandexMapKit/YMKMasstransitThread.h>

@class YMKMasstransitRoute;
@class YMKMasstransitTransportTransportThread;

/**
 * @brief Contains information about the mass transit ride section of a
 * YMKMasstransitRoute for a specific mass transit YMKMasstransitLine.
 */
@interface YMKMasstransitTransport : NSObject

/**
 * Mass transit line.
 */
@property (nonatomic, readonly, nonnull) YMKMasstransitLine *line;

/**
 * Collection of mass transit threads of the specified line suitable for
 * the constructed route.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKMasstransitTransportTransportThread *> *transports;


+ (nonnull YMKMasstransitTransport *)transportWithLine:(nonnull YMKMasstransitLine *)line
                                            transports:(nonnull NSArray<YMKMasstransitTransportTransportThread *> *)transports;


@end


/**
 * YMKMasstransitThread specific properties of a mass transit ride
 * section of a YMKMasstransitRoute.
 */
@interface YMKMasstransitTransportTransportThread : NSObject

/**
 * Mass transit thread.
 */
@property (nonatomic, readonly, nonnull) YMKMasstransitThread *thread;

/**
 * Indicates that the mass transit router considers this thread the best
 * one for the current section of the constucted route.
 */
@property (nonatomic, readonly) BOOL isRecommended;

/**
 * Collection of important annotations for the section.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKMasstransitAlert *> *alerts;

/**
 * @brief If alternateDepartureStop is specified, it specifies the
 * departure location for this particular Thread instead of the first
 * Stop of the Section.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKMasstransitStop *alternateDepartureStop;


+ (nonnull YMKMasstransitTransportTransportThread *)transportThreadWithThread:(nonnull YMKMasstransitThread *)thread
                                                                isRecommended:( BOOL)isRecommended
                                                                       alerts:(nonnull NSArray<YMKMasstransitAlert *> *)alerts
                                                       alternateDepartureStop:(nullable YMKMasstransitStop *)alternateDepartureStop;


@end

