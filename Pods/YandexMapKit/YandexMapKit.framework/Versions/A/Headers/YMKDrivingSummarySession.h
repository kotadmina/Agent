#import <YandexMapKit/YMKDrivingSummary.h>

#import <YandexRuntime/YRTPlatformBinding.h>

typedef void(^YMKDrivingSummarySessionSummaryHandler)(
    NSArray<YMKDrivingSummary *> *summaries,
    NSError *error);

@interface YMKDrivingSummarySession : YRTPlatformBinding

- (void)cancel;


- (void)retryWithSummaryHandler:(nullable YMKDrivingSummarySessionSummaryHandler)summaryHandler;


@end

