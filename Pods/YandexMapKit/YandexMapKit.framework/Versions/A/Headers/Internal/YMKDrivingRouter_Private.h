#import <YandexMapKit/YMKDrivingRouter.h>

#import <YandexRuntime/YRTSubscription.h>

#import <yandex/maps/mapkit/driving/driving_router.h>
#import <yandex/maps/runtime/ios/object.h>

#import <memory>

@interface YMKDrivingRouter ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::driving::DrivingRouter>)native;

- (::yandex::maps::mapkit::driving::DrivingRouter *)nativeDrivingRouter;

@end
