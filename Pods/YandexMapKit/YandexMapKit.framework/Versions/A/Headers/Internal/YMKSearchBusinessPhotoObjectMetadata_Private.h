#import <YandexMapKit/YMKSearchBusinessPhotoObjectMetadata.h>

#import <yandex/maps/mapkit/search/business_photo_object_metadata.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>



namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::search::BusinessPhotoObjectMetadata::Photo, YMKSearchBusinessPhotoObjectMetadataPhoto, void> {
    static ::yandex::maps::mapkit::search::BusinessPhotoObjectMetadata::Photo from(
        YMKSearchBusinessPhotoObjectMetadataPhoto* platformPhoto);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::search::BusinessPhotoObjectMetadata::Photo, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKSearchBusinessPhotoObjectMetadataPhoto*>::value>::type> {
    static ::yandex::maps::mapkit::search::BusinessPhotoObjectMetadata::Photo from(
        PlatformType platformPhoto)
    {
        return ToNative<::yandex::maps::mapkit::search::BusinessPhotoObjectMetadata::Photo, YMKSearchBusinessPhotoObjectMetadataPhoto>::from(
            platformPhoto);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::search::BusinessPhotoObjectMetadata::Photo> {
    static YMKSearchBusinessPhotoObjectMetadataPhoto* from(
        const ::yandex::maps::mapkit::search::BusinessPhotoObjectMetadata::Photo& photo);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
