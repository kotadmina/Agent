#import <YandexMapKit/YMKMasstransitSession.h>

#import <yandex/maps/mapkit/masstransit/session.h>

#import <memory>

namespace yandex {
namespace maps {
namespace mapkit {
namespace masstransit {
namespace ios {

Session::OnMasstransitRoutes onMasstransitRoutes(
    YMKMasstransitSessionRouteHandler handler);
Session::OnMasstransitRoutesError onMasstransitRoutesError(
    YMKMasstransitSessionRouteHandler handler);

} // namespace ios
} // namespace masstransit
} // namespace mapkit
} // namespace maps
} // namespace yandex

@interface YMKMasstransitSession ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::masstransit::Session>)native;

- (::yandex::maps::mapkit::masstransit::Session *)nativeSession;

@end
