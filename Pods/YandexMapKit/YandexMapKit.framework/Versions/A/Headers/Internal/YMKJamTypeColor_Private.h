#import <YandexMapKit/YMKJamTypeColor.h>

#import <yandex/maps/mapkit/map/route_helper.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::map::JamTypeColor, YMKJamTypeColor, void> {
    static ::yandex::maps::mapkit::map::JamTypeColor from(
        YMKJamTypeColor* platformJamTypeColor);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::map::JamTypeColor, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKJamTypeColor*>::value>::type> {
    static ::yandex::maps::mapkit::map::JamTypeColor from(
        PlatformType platformJamTypeColor)
    {
        return ToNative<::yandex::maps::mapkit::map::JamTypeColor, YMKJamTypeColor>::from(
            platformJamTypeColor);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::map::JamTypeColor> {
    static YMKJamTypeColor* from(
        const ::yandex::maps::mapkit::map::JamTypeColor& jamTypeColor);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
