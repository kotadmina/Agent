#import <YandexMapKit/YMKFasterAlternative.h>

#import <yandex/maps/mapkit/guidance/faster_alternative.h>

#import <memory>

@interface YMKFasterAlternative ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::guidance::FasterAlternative>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::guidance::FasterAlternative>)nativeFasterAlternative;
- (std::shared_ptr<::yandex::maps::mapkit::guidance::FasterAlternative>)native;

@end
