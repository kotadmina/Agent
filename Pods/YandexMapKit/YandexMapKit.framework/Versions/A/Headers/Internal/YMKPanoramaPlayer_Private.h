#import <YandexMapKit/YMKPanoramaPlayer.h>

#import <YandexRuntime/YRTSubscription.h>

#import <yandex/maps/mapkit/panorama/player.h>
#import <yandex/maps/runtime/ios/object.h>

#import <memory>

@interface YMKPanoramaPlayer ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::panorama::Player>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::panorama::Player>)nativePlayer;
- (std::shared_ptr<::yandex::maps::mapkit::panorama::Player>)native;

@end
