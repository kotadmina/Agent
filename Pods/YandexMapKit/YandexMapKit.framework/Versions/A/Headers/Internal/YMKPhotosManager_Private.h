#import <YandexMapKit/YMKPhotosManager.h>

#import <yandex/maps/mapkit/photos/photos_manager.h>

#import <memory>

@interface YMKPhotosManager ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::photos::PhotosManager>)native;

- (::yandex::maps::mapkit::photos::PhotosManager *)nativePhotosManager;

@end
