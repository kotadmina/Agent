#import <YandexMapKit/YMKDrivingDrivingOptions.h>

#import <yandex/maps/mapkit/driving/driving_router.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::driving::DrivingOptions, YMKDrivingDrivingOptions, void> {
    static ::yandex::maps::mapkit::driving::DrivingOptions from(
        YMKDrivingDrivingOptions* platformDrivingOptions);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::driving::DrivingOptions, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKDrivingDrivingOptions*>::value>::type> {
    static ::yandex::maps::mapkit::driving::DrivingOptions from(
        PlatformType platformDrivingOptions)
    {
        return ToNative<::yandex::maps::mapkit::driving::DrivingOptions, YMKDrivingDrivingOptions>::from(
            platformDrivingOptions);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::driving::DrivingOptions> {
    static YMKDrivingDrivingOptions* from(
        const ::yandex::maps::mapkit::driving::DrivingOptions& drivingOptions);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
