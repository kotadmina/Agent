#import <YandexMapKit/YMKPanoramaSpanChangeDelegate.h>

#import <yandex/maps/mapkit/panorama/player.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace mapkit {
namespace panorama {
namespace ios {

class SpanChangeListenerBinding : public ::yandex::maps::mapkit::panorama::SpanChangeListener {
public:
    explicit SpanChangeListenerBinding(
        id<YMKPanoramaSpanChangeDelegate> platformListener);

    virtual void onPanoramaSpanChanged(
        ::yandex::maps::mapkit::panorama::Player* player) override;

    id<YMKPanoramaSpanChangeDelegate> platformReference() const { return platformListener_; }

private:
    __weak id<YMKPanoramaSpanChangeDelegate> platformListener_;
};

} // namespace ios
} // namespace panorama
} // namespace mapkit
} // namespace maps
} // namespace yandex

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::panorama::SpanChangeListener>, id<YMKPanoramaSpanChangeDelegate>, void> {
    static std::shared_ptr<::yandex::maps::mapkit::panorama::SpanChangeListener> from(
        id<YMKPanoramaSpanChangeDelegate> platformSpanChangeListener);
};
template <typename PlatformType>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::panorama::SpanChangeListener>, PlatformType> {
    static std::shared_ptr<::yandex::maps::mapkit::panorama::SpanChangeListener> from(
        PlatformType platformSpanChangeListener)
    {
        return ToNative<std::shared_ptr<::yandex::maps::mapkit::panorama::SpanChangeListener>, id<YMKPanoramaSpanChangeDelegate>>::from(
            platformSpanChangeListener);
    }
};

template <>
struct ToPlatform<std::shared_ptr<::yandex::maps::mapkit::panorama::SpanChangeListener>> {
    static id<YMKPanoramaSpanChangeDelegate> from(
        const std::shared_ptr<::yandex::maps::mapkit::panorama::SpanChangeListener>& nativeSpanChangeListener);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
