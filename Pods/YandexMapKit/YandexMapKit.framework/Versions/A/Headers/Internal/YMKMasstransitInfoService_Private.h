#import <YandexMapKit/YMKMasstransitInfoService.h>

#import <yandex/maps/mapkit/masstransit/masstransit_info_service.h>

#import <memory>

@interface YMKMasstransitInfoService ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::masstransit::MasstransitInfoService>)native;

- (::yandex::maps::mapkit::masstransit::MasstransitInfoService *)nativeMasstransitInfoService;

@end
