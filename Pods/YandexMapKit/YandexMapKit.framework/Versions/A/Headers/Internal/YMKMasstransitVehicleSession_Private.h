#import <YandexMapKit/YMKMasstransitVehicleSession.h>

#import <yandex/maps/mapkit/masstransit/session.h>

#import <memory>

namespace yandex {
namespace maps {
namespace mapkit {
namespace masstransit {
namespace ios {

VehicleSession::OnVehicleResponse onVehicleResponse(
    YMKMasstransitVehicleSessionVehicleHandler handler);
VehicleSession::OnVehicleError onVehicleError(
    YMKMasstransitVehicleSessionVehicleHandler handler);

} // namespace ios
} // namespace masstransit
} // namespace mapkit
} // namespace maps
} // namespace yandex

@interface YMKMasstransitVehicleSession ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::masstransit::VehicleSession>)native;

- (::yandex::maps::mapkit::masstransit::VehicleSession *)nativeVehicleSession;

@end
