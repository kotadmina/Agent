#import <YandexMapKit/YMKMasstransitLineSession.h>

#import <yandex/maps/mapkit/masstransit/session.h>

#import <memory>

namespace yandex {
namespace maps {
namespace mapkit {
namespace masstransit {
namespace ios {

LineSession::OnLineResponse onLineResponse(
    YMKMasstransitLineSessionLineHandler handler);
LineSession::OnLineError onLineError(
    YMKMasstransitLineSessionLineHandler handler);

} // namespace ios
} // namespace masstransit
} // namespace mapkit
} // namespace maps
} // namespace yandex

@interface YMKMasstransitLineSession ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::masstransit::LineSession>)native;

- (::yandex::maps::mapkit::masstransit::LineSession *)nativeLineSession;

@end
