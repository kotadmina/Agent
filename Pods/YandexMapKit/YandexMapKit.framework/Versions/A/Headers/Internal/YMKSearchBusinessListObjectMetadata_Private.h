#import <YandexMapKit/YMKSearchBusinessListObjectMetadata.h>

#import <yandex/maps/mapkit/search/business_list_object_metadata.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>



namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::search::BusinessListObjectMetadata::Business, YMKSearchBusinessListObjectMetadataBusiness, void> {
    static ::yandex::maps::mapkit::search::BusinessListObjectMetadata::Business from(
        YMKSearchBusinessListObjectMetadataBusiness* platformBusiness);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::search::BusinessListObjectMetadata::Business, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKSearchBusinessListObjectMetadataBusiness*>::value>::type> {
    static ::yandex::maps::mapkit::search::BusinessListObjectMetadata::Business from(
        PlatformType platformBusiness)
    {
        return ToNative<::yandex::maps::mapkit::search::BusinessListObjectMetadata::Business, YMKSearchBusinessListObjectMetadataBusiness>::from(
            platformBusiness);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::search::BusinessListObjectMetadata::Business> {
    static YMKSearchBusinessListObjectMetadataBusiness* from(
        const ::yandex::maps::mapkit::search::BusinessListObjectMetadata::Business& business);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
