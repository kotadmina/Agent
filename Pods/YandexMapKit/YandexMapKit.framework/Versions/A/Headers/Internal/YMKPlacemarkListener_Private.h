#import <YandexMapKit/YMKPlacemarkListener.h>

#import <yandex/maps/mapkit/search_layer/search_layer.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace mapkit {
namespace search_layer {
namespace ios {

class PlacemarkListenerBinding : public ::yandex::maps::mapkit::search_layer::PlacemarkListener {
public:
    explicit PlacemarkListenerBinding(
        id<YMKPlacemarkListener> platformListener);

    virtual bool onTap(
        ::yandex::maps::mapkit::search_layer::SearchResultItem* searchResultItem) override;

    id<YMKPlacemarkListener> platformReference() const { return platformListener_; }

private:
    __weak id<YMKPlacemarkListener> platformListener_;
};

} // namespace ios
} // namespace search_layer
} // namespace mapkit
} // namespace maps
} // namespace yandex

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::search_layer::PlacemarkListener>, id<YMKPlacemarkListener>, void> {
    static std::shared_ptr<::yandex::maps::mapkit::search_layer::PlacemarkListener> from(
        id<YMKPlacemarkListener> platformPlacemarkListener);
};
template <typename PlatformType>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::search_layer::PlacemarkListener>, PlatformType> {
    static std::shared_ptr<::yandex::maps::mapkit::search_layer::PlacemarkListener> from(
        PlatformType platformPlacemarkListener)
    {
        return ToNative<std::shared_ptr<::yandex::maps::mapkit::search_layer::PlacemarkListener>, id<YMKPlacemarkListener>>::from(
            platformPlacemarkListener);
    }
};

template <>
struct ToPlatform<std::shared_ptr<::yandex::maps::mapkit::search_layer::PlacemarkListener>> {
    static id<YMKPlacemarkListener> from(
        const std::shared_ptr<::yandex::maps::mapkit::search_layer::PlacemarkListener>& nativePlacemarkListener);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
