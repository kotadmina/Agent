#import <YandexMapKit/YMKCarparksEventsLayer.h>

#import <YandexRuntime/YRTSubscription.h>

#import <yandex/maps/mapkit/carparks/carparks_events_layer.h>
#import <yandex/maps/runtime/ios/object.h>

#import <memory>

@interface YMKCarparksEventsLayer ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::carparks::CarparksEventsLayer>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::carparks::CarparksEventsLayer>)nativeCarparksEventsLayer;
- (std::shared_ptr<::yandex::maps::mapkit::carparks::CarparksEventsLayer>)native;

@end
