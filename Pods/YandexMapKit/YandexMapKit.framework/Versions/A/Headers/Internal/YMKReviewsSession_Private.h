#import <YandexMapKit/YMKReviewsSession.h>

#import <yandex/maps/mapkit/reviews/reviews_manager.h>

#import <memory>

namespace yandex {
namespace maps {
namespace mapkit {
namespace reviews {
namespace ios {

ReviewSession::OnReviewsFeedReceived onReviewsFeedReceived(
    YMKReviewsSessionDataHandler handler);
ReviewSession::OnReviewsFeedError onReviewsFeedError(
    YMKReviewsSessionDataHandler handler);

} // namespace ios
} // namespace reviews
} // namespace mapkit
} // namespace maps
} // namespace yandex

@interface YMKReviewsSession ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::reviews::ReviewSession>)native;

- (::yandex::maps::mapkit::reviews::ReviewSession *)nativeReviewSession;

@end
