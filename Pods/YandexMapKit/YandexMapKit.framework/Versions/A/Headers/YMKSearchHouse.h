#import <YandexMapKit/YMKPoint.h>

/**
 * Structure describing house
 */
@interface YMKSearchHouse : NSObject

/**
 * House name, like "16" or "42a".
 */
@property (nonatomic, readonly, nonnull) NSString *name;

/**
 * House point.
 */
@property (nonatomic, readonly, nonnull) YMKPoint *position;


+ (nonnull YMKSearchHouse *)houseWithName:(nonnull NSString *)name
                                 position:(nonnull YMKPoint *)position;


@end

