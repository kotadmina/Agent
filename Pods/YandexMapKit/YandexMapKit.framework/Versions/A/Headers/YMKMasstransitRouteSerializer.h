#import <YandexRuntime/YRTPlatformBinding.h>

@class YMKMasstransitRoute;

@interface YMKMasstransitRouteSerializer : YRTPlatformBinding

/**
 * @return Route's serialized representation. Empty array in case of any
 * errors.
 */
+ (nonnull NSData *)saveWithRoute:(nullable YMKMasstransitRoute *)route;


/**
 * @return Deserialized Route. Null in case of any errors.
 */
+ (nullable YMKMasstransitRoute *)loadWithData:(nonnull NSData *)data;


@end
