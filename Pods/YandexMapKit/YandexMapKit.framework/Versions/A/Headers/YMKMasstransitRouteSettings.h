#import <Foundation/Foundation.h>

/**
 * Route settings that were used by the mass transit router for a
 * specific route.
 */
@interface YMKMasstransitRouteSettings : NSObject

/**
 * Transport types that the router avoided.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSString *> *avoidTypes;

/**
 * Transport types that were allowed even if they are in the list of
 * avoided types.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSString *> *acceptTypes;


+ (nonnull YMKMasstransitRouteSettings *)routeSettingsWithAvoidTypes:(nonnull NSArray<NSString *> *)avoidTypes
                                                         acceptTypes:(nonnull NSArray<NSString *> *)acceptTypes;


@end

