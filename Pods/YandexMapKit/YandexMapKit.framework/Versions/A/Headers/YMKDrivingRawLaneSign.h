#import <YandexMapKit/YMKDrivingLane.h>

@interface YMKDrivingRawLaneSign : NSObject

@property (nonatomic, readonly) NSUInteger position;

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingLane *> *lanes;


+ (nonnull YMKDrivingRawLaneSign *)rawLaneSignWithPosition:( NSUInteger)position
                                                     lanes:(nonnull NSArray<YMKDrivingLane *> *)lanes;


@end

