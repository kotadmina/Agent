#import <YandexMapKit/YMKGeoObject.h>

/// @cond EXCLUDE
/**
 * Displays the banner.
 */
@interface YMKDirectBanner : NSObject

/**
 * Company title.
 */
@property (nonatomic, readonly, nonnull) NSString *title;

/**
 * Ad text.
 */
@property (nonatomic, readonly, nonnull) NSString *text;

/**
 * Additional ad text.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *extra;

/**
 * Disclaimer list for the ad.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSString *> *disclaimers;

/**
 * Company domain.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *domain;

/**
 * When a user clicks the banner, this is the URL to go to.
 */
@property (nonatomic, readonly, nonnull) NSString *url;

/**
 * URL of the YABS counter that should be used when displaying the
 * banner.
 */
@property (nonatomic, readonly, nonnull) NSString *counter;

/**
 * optional link to company
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKGeoObject *geo_object;

/**
 * an opaque string to be logged for this banner
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *logId;


+ (nonnull YMKDirectBanner *)bannerWithTitle:(nonnull NSString *)title
                                        text:(nonnull NSString *)text
                                       extra:(nullable NSString *)extra
                                 disclaimers:(nonnull NSArray<NSString *> *)disclaimers
                                      domain:(nullable NSString *)domain
                                         url:(nonnull NSString *)url
                                     counter:(nonnull NSString *)counter
                                  geo_object:(nullable YMKGeoObject *)geo_object
                                       logId:(nullable NSString *)logId;


@end
/// @endcond

