#import <YandexMapKit/YMKCallback.h>
#import <YandexMapKit/YMKIconStyle.h>

#import <YandexRuntime/YRTAnimatedImageProvider.h>
#import <YandexRuntime/YRTPlatformBinding.h>

/**
 * Animated placemark icon. Provides interface to load and control
 * animation.
 */
@interface YMKAnimatedIcon : YRTPlatformBinding

/**
 * Sets animated icon and its style. Animation is created in paused
 * state.
 */
- (void)setIconWithImage:(nullable id<YRTAnimatedImageProvider>)image
                   style:(nonnull YMKIconStyle *)style;


/**
 * Sets animated icon and its style. Animation is created in paused
 * state.
 *
 * @param onFinished is called on icon loaded.
 */
- (void)setIconWithImage:(nullable id<YRTAnimatedImageProvider>)image
                   style:(nonnull YMKIconStyle *)style
                callback:(nullable YMKCallback)callback;


/**
 * Changes icon style.
 */
- (void)setIconStyleWithStyle:(nonnull YMKIconStyle *)style;


/**
 * Starts animation. Removes current play callback. Same as play(null).
 */
- (void)play;


/**
 * Starts animation.
 *
 * @param onFinished is called on animation finished, replaces previous
 * callback.
 */
- (void)playWithCallback:(nullable YMKCallback)callback;


/**
 * Resumes paused animation. Callback (if any) is NOT removed.
 */
- (void)resume;


/**
 * Stops animation. Animation returns to initial paused state.
 */
- (void)stop;


/**
 * Pauses animation.
 */
- (void)pause;


/**
 * If true animation will be played in the reverse direction. Default
 * value is false.
 */
@property (nonatomic, getter=isReversed) BOOL reversed;

/**
 * Tells if this object is valid or no. Any method called on an invalid
 * object will throw an exception. The object becomes invalid only on UI
 * thread, and only when its implementation depends on objects already
 * destroyed by now. Please refer to general docs about the interface for
 * details on its invalidation.
 */
@property (nonatomic, readonly, getter=isValid) BOOL valid;

@end

