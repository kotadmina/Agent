#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, YMKPointOfView) {

    /**
     * Point of View is center of the screen
     */
    YMKPointOfViewScreenCenter,

    /**
     * Point of View has x-coordinate of center of FocusRect and
     * y-coordinate of center of screen
     */
    YMKPointOfViewAdaptToFocusRectHorizontally
};

