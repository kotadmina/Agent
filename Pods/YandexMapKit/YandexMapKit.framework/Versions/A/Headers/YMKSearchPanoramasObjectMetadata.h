#import <YandexMapKit/YMKSearchPanorama.h>

/// @cond EXCLUDE
/**
 * Snippet data to get panoramas info.
 */
@interface YMKSearchPanoramasObjectMetadata : NSObject

/**
 * List of panoramas.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKSearchPanorama *> *panoramas;


+ (nonnull YMKSearchPanoramasObjectMetadata *)panoramasObjectMetadataWithPanoramas:(nonnull NSArray<YMKSearchPanorama *> *)panoramas;


@end
/// @endcond

