#import <Foundation/Foundation.h>

/// @cond EXCLUDE
typedef NS_ENUM(NSUInteger, YMKReviewsVote) {

    YMKReviewsVotePositive,

    YMKReviewsVoteNegative
};
/// @endcond


/// @cond EXCLUDE
/**
 * Status of the review on the server.
 */
typedef NS_ENUM(NSUInteger, YMKReviewsStatus) {

    YMKReviewsStatusNew,

    YMKReviewsStatusInProgress,

    YMKReviewsStatusAccepted,

    YMKReviewsStatusDeclined
};
/// @endcond

