#import <YandexMapKit/YMKMasstransitBriefSchedule.h>
#import <YandexMapKit/YMKMasstransitThread.h>

/// @cond EXCLUDE
/**
 * @brief Contains information about a mass transit thread that runs
 * through a specified stop.
 */
@interface YMKMasstransitThreadAtStop : NSObject

/**
 * Mass transit thread.
 */
@property (nonatomic, readonly, nonnull) YMKMasstransitThread *thread;

/**
 * Indicates that boarding is not allowed at the specified stop on the
 * specified thread.
 */
@property (nonatomic, readonly) BOOL noBoarding;

/**
 * Indicates that drop off is not allowed at the specified stop on the
 * specified thread.
 */
@property (nonatomic, readonly) BOOL noDropOff;

/**
 * Information about upcoming vehicle arrivals and departures.
 */
@property (nonatomic, readonly, nonnull) YMKMasstransitBriefSchedule *briefSchedule;


+ (nonnull YMKMasstransitThreadAtStop *)threadAtStopWithThread:(nonnull YMKMasstransitThread *)thread
                                                    noBoarding:( BOOL)noBoarding
                                                     noDropOff:( BOOL)noDropOff
                                                 briefSchedule:(nonnull YMKMasstransitBriefSchedule *)briefSchedule;


@end
/// @endcond

