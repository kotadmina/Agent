#import <YandexMapKit/YMKMasstransitStop.h>

@interface YMKMasstransitRawThreadStopMetadata : NSObject

@property (nonatomic, readonly, nonnull) YMKMasstransitStop *stop;


+ (nonnull YMKMasstransitRawThreadStopMetadata *)rawThreadStopMetadataWithStop:(nonnull YMKMasstransitStop *)stop;


@end

