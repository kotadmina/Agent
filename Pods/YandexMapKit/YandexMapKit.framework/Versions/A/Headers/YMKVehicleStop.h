#import <YandexMapKit/YMKMasstransitStop.h>
#import <YandexMapKit/YMKTime.h>

@class YMKVehicleStopEstimation;

/// @cond EXCLUDE
/**
 * Upcoming vehicle stop data with arrival time forecast (if available).
 */
@interface YMKVehicleStop : NSObject

@property (nonatomic, readonly, nonnull) YMKMasstransitStop *stop;

/**
 * Estimated arrival time at the specified stop.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKVehicleStopEstimation *estimation;


+ (nonnull YMKVehicleStop *)vehicleStopWithStop:(nonnull YMKMasstransitStop *)stop
                                     estimation:(nullable YMKVehicleStopEstimation *)estimation;


@end
/// @endcond


/**
 * Contains the estimated time for the specified stop.
 */
@interface YMKVehicleStopEstimation : NSObject

/**
 * Estimated arrival time of the specified vehicle for the specified
 * stop.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKTime *arrivalTime;


+ (nonnull YMKVehicleStopEstimation *)estimationWithArrivalTime:(nullable YMKTime *)arrivalTime;


@end

