#import <YandexMapKit/YMKLocationManager.h>
#import <YandexMapKit/YMKLocationSimulatorListener.h>
#import <YandexMapKit/YMKPolyline.h>
#import <YandexMapKit/YMKPolylinePosition.h>

@interface YMKLocationSimulator : YMKLocationManager

@property (nonatomic, nonnull) YMKPolyline *geometry;

@property (nonatomic, readonly, nonnull) YMKPolylinePosition *polylinePosition;

@property (nonatomic) double speed;

- (void)subscribeForSimulatorEventsWithSimulatorListener:(nullable id<YMKLocationSimulatorListener>)simulatorListener;


- (void)unsubscribeFromSimulatorEventsWithSimulatorListener:(nullable id<YMKLocationSimulatorListener>)simulatorListener;


/**
 * true if simulator is not suspended
 */
@property (nonatomic, readonly, getter=isActive) BOOL active;

@end

