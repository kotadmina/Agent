#import <YandexMapKit/YMKMasstransitRouteStop.h>
#import <YandexMapKit/YMKMasstransitSectionMetadata.h>
#import <YandexMapKit/YMKSubpolyline.h>

@class YMKMasstransitRoute;

/**
 * Contains information about an individual section of a mass transit
 * YMKMasstransitRoute. The only fields that are always set are
 * YMKMasstransitSection::metadata.YMKMasstransitSectionMetadata::weight,
 * YMKMasstransitSection::geometry and
 * YMKMasstransitSection::metadata.YMKMasstransitSectionMetadata::data.
 */
@interface YMKMasstransitSection : NSObject

/**
 * General information about a section of a route.
 */
@property (nonatomic, readonly, nonnull) YMKMasstransitSectionMetadata *metadata;

/**
 * Geometry of the section as a fragment of a YMKMasstransitRoute
 * polyline.
 */
@property (nonatomic, readonly, nonnull) YMKSubpolyline *geometry;

/**
 * Vector of stops along the route. The first stop in the vector is the
 * stop for boarding the transport, and the last stop in the vector is
 * the stop for exiting the transport.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKMasstransitRouteStop *> *stops;

/**
 * Vector of polylines each connecting two consecutive stops. This
 * vector is only filled for mass transit ride sections, so this
 * geometry represents a part of the mass transit thread geometry
 * between two stops.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKSubpolyline *> *rideLegs;


+ (nonnull YMKMasstransitSection *)sectionWithMetadata:(nonnull YMKMasstransitSectionMetadata *)metadata
                                              geometry:(nonnull YMKSubpolyline *)geometry
                                                 stops:(nonnull NSArray<YMKMasstransitRouteStop *> *)stops
                                              rideLegs:(nonnull NSArray<YMKSubpolyline *> *)rideLegs;


@end

