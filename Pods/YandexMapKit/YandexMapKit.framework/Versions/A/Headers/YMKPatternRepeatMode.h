#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, YMKPatternRepeatMode) {

    /**
     * repeat image horizontally and vertically
     */
    YMKPatternRepeatModeRepeat,

    /**
     * repeat image horizontally and vertically, alternating mirror images
     * so that adjacent images always seam
     */
    YMKPatternRepeatModeMirror
};

