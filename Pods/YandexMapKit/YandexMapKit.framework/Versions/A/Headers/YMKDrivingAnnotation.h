#import <YandexMapKit/YMKDrivingAction.h>
#import <YandexMapKit/YMKDrivingActionMetadata.h>
#import <YandexMapKit/YMKDrivingLandmark.h>
#import <YandexMapKit/YMKDrivingToponymPhrase.h>

typedef NS_ENUM(NSUInteger, YMKDrivingAnnotationSchemeID) {

    YMKDrivingAnnotationSchemeIDSmall,

    YMKDrivingAnnotationSchemeIDMedium,

    YMKDrivingAnnotationSchemeIDLarge,

    YMKDrivingAnnotationSchemeIDHighway
};


/**
 * The annotation that is displayed on the map.
 */
@interface YMKDrivingAnnotation : NSObject

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *action;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *toponym;

@property (nonatomic, readonly, nonnull) NSString *descriptionText;

@property (nonatomic, readonly, nonnull) YMKDrivingActionMetadata *actionMetadata;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *landmarks;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingToponymPhrase *toponymPhrase;


+ (nonnull YMKDrivingAnnotation *)annotationWithAction:(nullable NSNumber *)action
                                               toponym:(nullable NSString *)toponym
                                       descriptionText:(nonnull NSString *)descriptionText
                                        actionMetadata:(nonnull YMKDrivingActionMetadata *)actionMetadata
                                             landmarks:(nonnull NSArray<NSNumber *> *)landmarks
                                         toponymPhrase:(nullable YMKDrivingToponymPhrase *)toponymPhrase;


@end

