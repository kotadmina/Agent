#import <YandexMapKit/YMKTaxiMoney.h>

/**
 * Single currency exchange prices.
 */
@interface YMKSearchCurrencyExchangeType : NSObject

/**
 * ISO-4217 currency name, e.g. "USD" or "RUB" or "EUR".
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *name;

/**
 * Buy rate.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKTaxiMoney *buy;

/**
 * Sell rate.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKTaxiMoney *sell;


+ (nonnull YMKSearchCurrencyExchangeType *)currencyExchangeTypeWithName:(nullable NSString *)name
                                                                    buy:(nullable YMKTaxiMoney *)buy
                                                                   sell:(nullable YMKTaxiMoney *)sell;


@end

