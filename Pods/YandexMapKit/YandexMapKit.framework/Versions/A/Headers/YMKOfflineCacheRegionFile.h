#import <Foundation/Foundation.h>

/**
 * @attention This feature is not available in the free MapKit version.
 */
@interface YMKOfflineCacheRegionFile : NSObject

@property (nonatomic, readonly, nonnull) NSString *cacheType;

@property (nonatomic, readonly, nonnull) NSString *version;

@property (nonatomic, readonly, nonnull) NSString *downloadUrl;

@property (nonatomic, readonly) long long downloadSize;

@property (nonatomic, readonly) long long downloaded;

/**
 * This counter was moved to offline_cache::CacheConsumer. Not used any
 * more.
 */
@property (nonatomic, readonly) long long unused_;

@property (nonatomic, readonly, nonnull) NSString *downloadId;


+ (nonnull YMKOfflineCacheRegionFile *)regionFileWithCacheType:(nonnull NSString *)cacheType
                                                       version:(nonnull NSString *)version
                                                   downloadUrl:(nonnull NSString *)downloadUrl
                                                  downloadSize:( long long)downloadSize
                                                    downloaded:( long long)downloaded
                                                       unused_:( long long)unused_
                                                    downloadId:(nonnull NSString *)downloadId;


@end

