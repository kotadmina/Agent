#import <YandexMapKit/YMKDrivingRawSpot.h>

@interface YMKDrivingRawSpots : NSObject

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingRawSpot *> *spots;


+ (nonnull YMKDrivingRawSpots *)rawSpotsWithSpots:(nonnull NSArray<YMKDrivingRawSpot *> *)spots;


@end

