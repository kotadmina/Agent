#import <YandexRuntime/YRTError.h>

/**
 * No cache available for offline search for the given request.
 */
@interface YMKSearchCacheUnavailableError : YRTError

@end

