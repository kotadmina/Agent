#import <YandexMapKit/YMKPoint.h>

/**
 * Defines the visible region.
 */
@interface YMKVisibleRegion : NSObject

@property (nonatomic, readonly, nonnull) YMKPoint *topLeft;

@property (nonatomic, readonly, nonnull) YMKPoint *topRight;

@property (nonatomic, readonly, nonnull) YMKPoint *bottomLeft;

@property (nonatomic, readonly, nonnull) YMKPoint *bottomRight;


+ (nonnull YMKVisibleRegion *)visibleRegionWithTopLeft:(nonnull YMKPoint *)topLeft
                                              topRight:(nonnull YMKPoint *)topRight
                                            bottomLeft:(nonnull YMKPoint *)bottomLeft
                                           bottomRight:(nonnull YMKPoint *)bottomRight;


@end

