#import <YandexRuntime/YRTPlatformBinding.h>

@class YMKLocationManager;
@class YMKLocationViewSource;

@interface YMKLocationViewSourceFactory : YRTPlatformBinding

+ (nullable YMKLocationViewSource *)createLocationViewSourceWithLocationManager:(nullable YMKLocationManager *)locationManager;


@end
