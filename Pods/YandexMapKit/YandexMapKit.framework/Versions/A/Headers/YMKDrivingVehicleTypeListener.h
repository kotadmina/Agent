#import <Foundation/Foundation.h>

@protocol YMKDrivingVehicleTypeListener <NSObject>

- (void)onVehicleTypeUpdated;


@end
