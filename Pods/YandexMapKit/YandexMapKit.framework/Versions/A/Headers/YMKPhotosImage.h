#import <Foundation/Foundation.h>

/// @cond EXCLUDE
/**
 * Photo with a particular size.
 */
@interface YMKPhotosImage : NSObject

@property (nonatomic, readonly, nonnull) NSString *imageId;

/**
 * Available sizes are listed here
 * http://api.yandex.ru/fotki/doc/format-ref/f-img.xml .
 */
@property (nonatomic, readonly, nonnull) NSString *size;

@property (nonatomic, readonly) NSUInteger width;

@property (nonatomic, readonly) NSUInteger height;


+ (nonnull YMKPhotosImage *)imageWithImageId:(nonnull NSString *)imageId
                                        size:(nonnull NSString *)size
                                       width:( NSUInteger)width
                                      height:( NSUInteger)height;


@end
/// @endcond

