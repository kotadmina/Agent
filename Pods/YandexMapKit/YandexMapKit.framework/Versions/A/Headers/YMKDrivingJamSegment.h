#import <Foundation/Foundation.h>

/**
 * The intensity of traffic.
 */
typedef NS_ENUM(NSUInteger, YMKDrivingJamType) {

    YMKDrivingJamTypeUnknown,

    YMKDrivingJamTypeBlocked,

    YMKDrivingJamTypeFree,

    YMKDrivingJamTypeLight,

    YMKDrivingJamTypeHard,

    YMKDrivingJamTypeVeryHard
};


/**
 * A segment of a traffic jam that has specific traffic conditions.
 */
@interface YMKDrivingJamSegment : NSObject

@property (nonatomic, readonly) YMKDrivingJamType jamType;

@property (nonatomic, readonly) double speed;


+ (nonnull YMKDrivingJamSegment *)jamSegmentWithJamType:( YMKDrivingJamType)jamType
                                                  speed:( double)speed;


@end

