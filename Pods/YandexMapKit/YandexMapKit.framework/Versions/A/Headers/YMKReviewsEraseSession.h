#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
typedef void(^YMKReviewsEraseSessionHandler)(
    NSError *error);

/**
 * Handles review erasure.
 */
@interface YMKReviewsEraseSession : YRTPlatformBinding

/**
 * Cancels a pending delete operation, if there is one.
 */
- (void)cancel;


/**
 * Re-runs the delete operation for a particular review.
 */
- (void)retryWithHandler:(nullable YMKReviewsEraseSessionHandler)handler;


@end
/// @endcond

