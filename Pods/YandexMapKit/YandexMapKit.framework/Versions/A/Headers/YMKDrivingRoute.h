#import <YandexMapKit/YMKDrivingAnnotation.h>
#import <YandexMapKit/YMKDrivingAnnotationLang.h>
#import <YandexMapKit/YMKDrivingConditionsListener.h>
#import <YandexMapKit/YMKDrivingEvent.h>
#import <YandexMapKit/YMKDrivingJamSegment.h>
#import <YandexMapKit/YMKDrivingLaneSign.h>
#import <YandexMapKit/YMKDrivingRequestPoint.h>
#import <YandexMapKit/YMKDrivingRouteMetadata.h>
#import <YandexMapKit/YMKDrivingSection.h>
#import <YandexMapKit/YMKDrivingSpot.h>
#import <YandexMapKit/YMKDrivingVehicleType.h>
#import <YandexMapKit/YMKPolyline.h>
#import <YandexMapKit/YMKPolylinePosition.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/**
 * Driving route. A route consists of multiple sections. Each section
 * has a corresponding annotation that describes the action at the
 * beginning of the section.
 */
@interface YMKDrivingRoute : YRTPlatformBinding

@property (nonatomic, readonly, nonnull) NSString *routeId;

@property (nonatomic, readonly, nonnull) YMKDrivingRouteMetadata *metadata;

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingSection *> *sections;

@property (nonatomic, readonly, nonnull) YMKPolyline *geometry;

/**
 * Traffic conditions on the given route.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingJamSegment *> *jamSegments;

/**
 * Events on the given route.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingEvent *> *events;

/**
 * Speed limits for segments in the geometry.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *speedLimits;

/**
 * Annotation schemes for segments in the geometry.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *annotationSchemes;

/**
 * Vehicle type (e.g. Taxi).
 */
@property (nonatomic, readonly) YMKDrivingVehicleType vehicleType;

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingLaneSign *> *laneSigns;

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingSpot *> *spots;

/**
 * Language of string annotations (e.g. street names) in this route
 * object.
 *
 * Optional property, can be nil.
 */
@property (nonatomic, readonly, nullable) NSNumber *annotationLanguage;

/**
 * Request points that were specified in the router request that this
 * route originated from.
 *
 * Optional property, can be nil.
 */
@property (nonatomic, readonly, nullable) NSArray<YMKDrivingRequestPoint *> *requestPoints;

/**
 * The reached position on the given route. The 'RouteMetadata::weight'
 * field contains data for the part of the route beyond this position.
 */
@property (nonatomic, nonnull) YMKPolylinePosition *position;

/**
 * Indicates whether driving conditions (jamSegments and events) have
 * become outdated when we are not able to fetch updates for some
 * predefined time.
 */
@property (nonatomic, readonly, getter=isAreConditionsOutdated) BOOL areConditionsOutdated;

- (void)addConditionsListenerWithConditionsListener:(nullable id<YMKDrivingConditionsListener>)conditionsListener;


- (void)removeConditionsListenerWithConditionsListener:(nullable id<YMKDrivingConditionsListener>)conditionsListener;


- (NSUInteger)sectionIndexWithSegmentIndex:(NSUInteger)segmentIndex;


- (nonnull YMKDrivingRouteMetadata *)metadataAtWithPosition:(nonnull YMKPolylinePosition *)position;


@end

