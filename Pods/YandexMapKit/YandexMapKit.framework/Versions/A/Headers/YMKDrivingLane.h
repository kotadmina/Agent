#import <Foundation/Foundation.h>

/**
 * The direction of the lane.
 */
typedef NS_ENUM(NSUInteger, YMKDrivingLaneDirection) {

    YMKDrivingLaneDirectionUnknownDirection,

    YMKDrivingLaneDirectionLeft180,

    YMKDrivingLaneDirectionLeft135,

    YMKDrivingLaneDirectionLeft90,

    YMKDrivingLaneDirectionLeft45,

    YMKDrivingLaneDirectionStraightAhead,

    YMKDrivingLaneDirectionRight45,

    YMKDrivingLaneDirectionRight90,

    YMKDrivingLaneDirectionRight135,

    YMKDrivingLaneDirectionRight180,

    YMKDrivingLaneDirectionLeftFromRight,

    YMKDrivingLaneDirectionRightFromLeft,

    YMKDrivingLaneDirectionLeftShift,

    YMKDrivingLaneDirectionRightShift
};


/**
 * The type of lane.
 */
typedef NS_ENUM(NSUInteger, YMKDrivingLaneKind) {

    YMKDrivingLaneKindUnknownKind,

    YMKDrivingLaneKindPlainLane,

    YMKDrivingLaneKindBusLane,

    YMKDrivingLaneKindTramLane,

    YMKDrivingLaneKindTaxiLane,

    YMKDrivingLaneKindBikeLane
};


/**
 * The lane object.
 */
@interface YMKDrivingLane : NSObject

@property (nonatomic, readonly) YMKDrivingLaneKind laneKind;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *directions;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *highlightedDirection;


+ (nonnull YMKDrivingLane *)laneWithLaneKind:( YMKDrivingLaneKind)laneKind
                                  directions:(nonnull NSArray<NSNumber *> *)directions
                        highlightedDirection:(nullable NSNumber *)highlightedDirection;


@end

