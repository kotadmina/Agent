#import <YandexMapKit/YMKLocalizedValue.h>
#import <YandexMapKit/YMKPoint.h>

@class YMKSearchStopStyle;

/// @cond EXCLUDE
/**
 * Describes masstransit stop.
 */
@interface YMKSearchStop : NSObject

/**
 * Masstransit stop name.
 */
@property (nonatomic, readonly, nonnull) NSString *name;

/**
 * Localized distance to the stop.
 */
@property (nonatomic, readonly, nonnull) YMKLocalizedValue *distance;

/**
 * Nearest stop style, for example its color.
 */
@property (nonatomic, readonly, nonnull) YMKSearchStopStyle *style;

/**
 * Masstransit stop point.
 */
@property (nonatomic, readonly, nonnull) YMKPoint *point;


+ (nonnull YMKSearchStop *)stopWithName:(nonnull NSString *)name
                               distance:(nonnull YMKLocalizedValue *)distance
                                  style:(nonnull YMKSearchStopStyle *)style
                                  point:(nonnull YMKPoint *)point;


@end
/// @endcond


/**
 * Masstransit stop style.
 */
@interface YMKSearchStopStyle : NSObject

/**
 * Masstransit stop color.
 */
@property (nonatomic, readonly) NSInteger color;


+ (nonnull YMKSearchStopStyle *)styleWithColor:( NSInteger)color;


@end

