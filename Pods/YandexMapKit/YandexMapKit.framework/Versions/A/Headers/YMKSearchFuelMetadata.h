#import <YandexMapKit/YMKSearchFuelType.h>

/**
 * Fuel snippet.
 */
@interface YMKSearchFuelMetadata : NSObject

/**
 * Fuel list.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKSearchFuelType *> *fuels;


+ (nonnull YMKSearchFuelMetadata *)fuelMetadataWithFuels:(nonnull NSArray<YMKSearchFuelType *> *)fuels;


@end

