#import <YandexMapKit/YMKDirection.h>
#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKSpan.h>

/// @cond EXCLUDE
/**
 * Panorama info.
 */
@interface YMKSearchPanorama : NSObject

/**
 * Machine readable panorama identifier.
 */
@property (nonatomic, readonly, nonnull) NSString *id;

/**
 * Direction of the panorama center.
 */
@property (nonatomic, readonly, nonnull) YMKDirection *direction;

/**
 * H-Span and V-Span hints for the panorama player.
 */
@property (nonatomic, readonly, nonnull) YMKSpan *span;

/**
 * Panorama point.
 */
@property (nonatomic, readonly, nonnull) YMKPoint *point;


+ (nonnull YMKSearchPanorama *)panoramaWithId:(nonnull NSString *)id
                                    direction:(nonnull YMKDirection *)direction
                                         span:(nonnull YMKSpan *)span
                                        point:(nonnull YMKPoint *)point;


@end
/// @endcond

