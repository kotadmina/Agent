#import <Foundation/Foundation.h>

/// @cond EXCLUDE
@interface YRTTestSupportTestStructure : NSObject

@property (nonatomic, readonly) BOOL b;

@property (nonatomic, readonly, nonnull) NSString *text;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *optionalText;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *intVector;

@property (nonatomic, readonly) NSTimeInterval interval;

@property (nonatomic, readonly, nonnull) NSDate *timestamp;


+ (nonnull YRTTestSupportTestStructure *)testStructureWithB:( BOOL)b
                                                       text:(nonnull NSString *)text
                                               optionalText:(nullable NSString *)optionalText
                                                  intVector:(nonnull NSArray<NSNumber *> *)intVector
                                                   interval:( NSTimeInterval)interval
                                                  timestamp:(nonnull NSDate *)timestamp;


@end
/// @endcond

