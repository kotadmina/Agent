#import <YandexRuntime/YRTPlatformBinding.h>

@class YRTEventLogging;

@interface YRTEventLoggingFactory : YRTPlatformBinding

/// @cond EXCLUDE
+ (nullable YRTEventLogging *)getEventLogging;
/// @endcond


@end
