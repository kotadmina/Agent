#import <YandexRuntime/YRTPlatformBinding.h>

@class YRTLogging;

@interface YRTLoggingFactory : YRTPlatformBinding

/// @cond EXCLUDE
+ (nullable YRTLogging *)getLogging;
/// @endcond


@end
