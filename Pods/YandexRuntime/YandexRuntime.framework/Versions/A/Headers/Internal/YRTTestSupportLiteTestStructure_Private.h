#import <YandexRuntime/YRTTestSupportLiteTestStructure.h>

#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>
#import <yandex/maps/runtime/internal/test_support/test_types.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::runtime::internal::test_support::LiteTestStructure, YRTTestSupportLiteTestStructure, void> {
    static ::yandex::maps::runtime::internal::test_support::LiteTestStructure from(
        YRTTestSupportLiteTestStructure* platformLiteTestStructure);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::runtime::internal::test_support::LiteTestStructure, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YRTTestSupportLiteTestStructure*>::value>::type> {
    static ::yandex::maps::runtime::internal::test_support::LiteTestStructure from(
        PlatformType platformLiteTestStructure)
    {
        return ToNative<::yandex::maps::runtime::internal::test_support::LiteTestStructure, YRTTestSupportLiteTestStructure>::from(
            platformLiteTestStructure);
    }
};

template <>
struct ToPlatform<::yandex::maps::runtime::internal::test_support::LiteTestStructure> {
    static YRTTestSupportLiteTestStructure* from(
        const ::yandex::maps::runtime::internal::test_support::LiteTestStructure& liteTestStructure);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
