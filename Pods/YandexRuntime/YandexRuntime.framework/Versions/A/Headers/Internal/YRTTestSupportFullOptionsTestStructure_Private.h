#import <YandexRuntime/YRTTestSupportFullOptionsTestStructure.h>

#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>
#import <yandex/maps/runtime/internal/test_support/test_types.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::runtime::internal::test_support::FullOptionsTestStructure, YRTTestSupportFullOptionsTestStructure, void> {
    static ::yandex::maps::runtime::internal::test_support::FullOptionsTestStructure from(
        YRTTestSupportFullOptionsTestStructure* platformFullOptionsTestStructure);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::runtime::internal::test_support::FullOptionsTestStructure, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YRTTestSupportFullOptionsTestStructure*>::value>::type> {
    static ::yandex::maps::runtime::internal::test_support::FullOptionsTestStructure from(
        PlatformType platformFullOptionsTestStructure)
    {
        return ToNative<::yandex::maps::runtime::internal::test_support::FullOptionsTestStructure, YRTTestSupportFullOptionsTestStructure>::from(
            platformFullOptionsTestStructure);
    }
};

template <>
struct ToPlatform<::yandex::maps::runtime::internal::test_support::FullOptionsTestStructure> {
    static YRTTestSupportFullOptionsTestStructure* from(
        const ::yandex::maps::runtime::internal::test_support::FullOptionsTestStructure& fullOptionsTestStructure);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
