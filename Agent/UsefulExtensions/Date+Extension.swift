//
//  Date+Extension.swift
//  Agent
//
//  Created by Sher Locked on 15.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

extension Date {
    func timeFromNow() -> String {
        let timeInterval = abs(timeIntervalSinceNow)
        
        if timeInterval == 0 {
            return "сейчас"
        }
        
        let intInterval = Int(timeInterval)
        
        if (intInterval < 60) {
            return intInterval.pluralForm(str1: "секунда", str2: "секунды", str3: "секунд", additionalString: " назад")
        }
        if (intInterval < 3600) {
            let interval = intInterval/60
            return interval.pluralForm(str1: "минута", str2: "минуты", str3: "минут", additionalString: " назад")
        }
        if (intInterval < 3600 * 24) {
            let interval = intInterval/3600
            return interval.pluralForm(str1: "час", str2: "часа", str3: "часов", additionalString: " назад")
        }
        if (intInterval < 3600*24*7) {
            let interval = intInterval/86400
            return interval.pluralForm(str1: "день", str2: "дня", str3: "дней", additionalString: " назад")
        }
        let interval = intInterval/604800
        return interval.pluralForm(str1: "неделя", str2: "недели", str3: "недель", additionalString: " назад")
    }
    
}
