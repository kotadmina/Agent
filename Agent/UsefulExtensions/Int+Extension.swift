//
//  Int+Extension.swift
//  Agent
//
//  Created by Sher Locked on 19.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

extension Int {
    func pluralForm(str1: String, str2: String, str3: String, additionalString: String) -> String {
        let value100 = self % 100
        if value100 >= 11 && value100 <= 19 {
            return "\(self) " + str3 + additionalString
        }
        switch self % 10 {
        case 1:
            return "\(self) " + str1 + additionalString
        case 2...4:
            return "\(self) " + str2 + additionalString
        default:
            return "\(self) " + str3 + additionalString
        }
    }
}
