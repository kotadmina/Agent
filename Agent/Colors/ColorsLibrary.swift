//
//  ColorsLibrary.swift
//  Agent
//
//  Created by Sher Locked on 30.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

struct ColorsLibrary {
    
    static let mainDarkViolet = UIColor(red: 67/255, green: 57/255, blue: 68/255, alpha: 1)
    static let mainYellow = UIColor(red: 255/255, green: 216/255, blue: 71/255, alpha: 1)
    static let fairIvory = UIColor(red: 247/255, green: 244/255, blue: 244/255, alpha: 1)
    static let mapAccuracy = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 0.6)
    static let grey = UIColor(white: 151/255, alpha: 1)
    static let rewardCellYellow = UIColor(red: 255/255, green: 215/255, blue: 71/255, alpha: 0.5)
    static let rewardLockedGrey = UIColor(red: 120/255, green: 118/255, blue: 118/255, alpha: 1)
    static let rewardLockedBorderGrey = UIColor(white: 67/255, alpha: 1)
}
