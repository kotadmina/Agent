//
//  FontLibrary.swift
//  Agent
//
//  Created by Sher Locked on 29.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class FontLibrary {
    
    static func checkFonts() {
        for fontFamilyName in UIFont.familyNames{
            for fontName in UIFont.fontNames(forFamilyName: fontFamilyName){
                print("Family: \(fontFamilyName)     Font: \(fontName)")
            }
        }
    }
    
    static func pragmaticaCondRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "PragmaticaCondBook-Reg", size: size)!
    }
    
    static func pragmaticaCondLight(size: CGFloat) -> UIFont {
        return UIFont(name: "PragmaticaCondExtraLight-Reg", size: size)!
    }
    
    static func pragmaticaRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "PragmaticaBook-Reg", size: size)!
    }
    
    //PTSans-CaptionBold PTSans-Caption PTSans-Bold PTSans-Regular PTSans-BoldItalic PTSans-Italic PragmaticaExtraLight-Reg
    
    static func pragmaticaExtraLightReg(size: CGFloat) -> UIFont {
        return UIFont(name: "PragmaticaExtraLight-Reg", size: size)!
    }
    
}
