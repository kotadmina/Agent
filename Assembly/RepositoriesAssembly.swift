//
//  RepositoriesAssembly.swift
//  Agent
//
//  Created by Sher Locked on 19.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation
import Swinject

class RepositoriesAssembly: BaseAssembly {
    
    static func configure() {
        let container = defaultContainer()
        
        container.register(CategoriesRepositoryProtocol.self) { _ in
            return CatergoriesRepository()
            }.inObjectScope(.container)
        
        container.register(MissionsRepositoryProtocol.self) { _ in
            return MissionsRepository()
            }.inObjectScope(.container)
        
        container.register(UserRepositoryProtocol.self) { _ in
            return UserRepository()
            }.inObjectScope(.container)
    }
}
