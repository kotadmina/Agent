//
//  ModulesAssembly.swift
//  Agent
//
//  Created by Sher Locked on 24.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class ModulesAssembly: BaseAssembly {
    
    static func configure() {
        HomeScreenAssembly.configure()
        TabBarAssembly.configure()
        MissionsMapScreenAssembly.configure()
        ProfileScreenAssembly.configure()
        TaskScreenAssembly.configure()
        ChatScreenAssembly.configure()
        MissionDetailAssembly.configure()
        CategoryScreenAssembly.configure()
        InProcessMissionsAssembly.configure()
        ReadyMissionsAssembly.configure()
        RewardsAssembly.configure()
        SettingsScreenAssembly.configure()
        FormScreenAssembly.configure()
        TermsScreenAssembly.configure()
    }
}
