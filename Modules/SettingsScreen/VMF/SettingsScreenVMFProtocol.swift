//
//  SettingsScreenVMFProtocol.swift
//  Agent
//
//  Created by Sher Locked on 02.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol SettingsScreenVMFProtocol {
    func createCellViewModels() -> [[ProfileScreenCellViewModel]]
}
