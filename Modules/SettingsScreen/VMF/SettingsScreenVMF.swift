//
//  SettingsScreenVMF.swift
//  Agent
//
//  Created by Sher Locked on 02.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

enum SettingsScreenCellId: String {
    case form
    case notification
    case geo
    case about
    case customOffer
    case privacyPolicy
    case faq
    case partners
}

class SettingsScreenVMF: SettingsScreenVMFProtocol {
    
    func createCellViewModels() -> [[ProfileScreenCellViewModel]] {
        let viewModel1 = TextWithArrowViewModel(name: "Анкета", id: SettingsScreenCellId.form.rawValue)
        let section1 = [viewModel1]
        
        let viewModel2 = SwitchCellViewModel(name: "Уведомления", id: SettingsScreenCellId.notification.rawValue, switchOn: true)
        let viewModel3 = SwitchCellViewModel(name: "Геолокация", id: SettingsScreenCellId.geo.rawValue, switchOn: true)
        let section2 = [viewModel2, viewModel3]
        
        let viewModel4 = TextWithArrowViewModel(name: "О Agent01", id: SettingsScreenCellId.about.rawValue)
        let viewModel5 = TextWithArrowViewModel(name: "Пользовательская оферта", id: SettingsScreenCellId.customOffer.rawValue)
        let viewModel6 = TextWithArrowViewModel(name: "Политика конфиденциальности", id: SettingsScreenCellId.privacyPolicy.rawValue)
        let viewModel7 = TextWithArrowViewModel(name: "FAQ", id: SettingsScreenCellId.faq.rawValue)
        let viewModel8 = TextWithArrowViewModel(name: "Партнеры Agent01", id: SettingsScreenCellId.partners.rawValue)
        let section3 = [viewModel4, viewModel5, viewModel6, viewModel7, viewModel8]
        
        return [section1, section2, section3]
    }
    
}
