//
//  SettingsScreenAssembly.swift
//  Agent
//
//  Created by Sher Locked on 28.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class SettingsScreenAssembly: BaseAssembly {
    
    static func configure() {
        let container = defaultContainer()
        
        //register view
        container.register(SettingsScreenViewInput.self) { r in
            let storyboard = UIStoryboard(name: "SettingsScreenView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SettingsScreenView") as! SettingsScreenView
            vc.output = r.resolve(SettingsScreenViewOutput.self, argument: (vc as SettingsScreenViewInput))!
            return vc
            }.inObjectScope(.transient)
        
        //register presenter
        container.register(SettingsScreenViewOutput.self) { (r: Resolver, view: SettingsScreenViewInput) in
            let presenter = SettingsScreenPresenter()
            presenter.view = view
            presenter.vmf = r.resolve(SettingsScreenVMFProtocol.self)!
            presenter.interactor = r.resolve(SettingsScreenInteractorInput.self)!
            presenter.router = r.resolve(SettingsScreenRouterInput.self, argument: view as! UIViewController)!
            return presenter
            }.inObjectScope(.transient)
        
        //register interactor
        container.register(SettingsScreenInteractorInput.self) { r in
            let interactor = SettingsScreenInteractor()
            interactor.userRepository = r.resolve(UserRepositoryProtocol.self)!
            return interactor
            }.inObjectScope(.transient)
        
        //register vmf
        container.register(SettingsScreenVMFProtocol.self) { _ in
            let vmf = SettingsScreenVMF()
            return vmf
            }.inObjectScope(.transient)
        
        //register router
        container.register(SettingsScreenRouterInput.self) { (r: Resolver, view: UIViewController) in
            let router = SettingsScreenRouter()
            router.view = view
            return router
            }.inObjectScope(.transient)
    }
    
    
}
