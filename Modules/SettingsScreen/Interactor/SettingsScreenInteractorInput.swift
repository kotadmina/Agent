//
//  SettingsScreenInteractorInput.swift
//  Agent
//
//  Created by Sher Locked on 28.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol SettingsScreenInteractorInput {
    func getUserInfo() -> User
    func getTermsOfUse() -> Terms
    func getUserAgreement() -> Terms
}
