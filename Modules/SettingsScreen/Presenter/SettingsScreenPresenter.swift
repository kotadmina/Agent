//
//  SettingsScreenPresenter.swift
//  Agent
//
//  Created by Sher Locked on 28.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class SettingsScreenPresenter: SettingsScreenViewOutput {
    
    weak var view: SettingsScreenViewInput!
    var vmf: SettingsScreenVMFProtocol!
    var interactor: SettingsScreenInteractorInput!
    var router: SettingsScreenRouterInput!
    
    func viewDidLoad() {
        let viewModels = vmf.createCellViewModels()
        view.updateTable(with: viewModels)
        let user = interactor.getUserInfo()
        view.configureHeader(fullName: user.name.uppercased() + " " + user.surname.uppercased(), geo: user.cityInfo, email: user.email, avatarName: user.avatarImageName)
    }
    
    func cellTaped(with id: String) {
        guard let type = SettingsScreenCellId(rawValue: id) else {
            return
        }
        
        switch type {
        case .form:
            router.selectForm()
        case .notification, .geo:
            break
        case .about:
            print("about")
        case .customOffer:
            let term = interactor.getTermsOfUse()
            router.selectTermsOfUse(with: term)
        case .privacyPolicy:
            let term = interactor.getUserAgreement()
            router.selectTermsOfUse(with: term)
        case .faq:
            print("faq")
        case .partners:
            print("partners")
        }
    }
    
    func backButtonTaped() {
        router.back()
    }
    
    func switchChanged(with id: String, newState: Bool) {
        print(id + " \(newState)")
    }
    
}
