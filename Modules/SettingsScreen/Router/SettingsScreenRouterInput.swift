//
//  SettingsScreenRouterInput.swift
//  Agent
//
//  Created by Sher Locked on 28.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol SettingsScreenRouterInput {
    func back()
    func selectForm()
    func selectTermsOfUse(with term: Terms)
}
