//
//  SettingsScreenRouter.swift
//  Agent
//
//  Created by Sher Locked on 28.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class SettingsScreenRouter: SettingsScreenRouterInput {
    weak var view: UIViewController!
    
    func back() {
        view.navigationController?.popViewController(animated: true)
    }
    
    func selectForm() {
        let vc = ModulesAssembly.resolve(type: FormScreenViewInput.self) as! UIViewController
        vc.hidesBottomBarWhenPushed = true
        view.navigationController?.pushViewController(vc, animated: true)
    }
    
    func selectTermsOfUse(with term: Terms) {
        let vc = ModulesAssembly.resolve(type: TermsScreenViewInput.self) as! UIViewController
        ((vc as? TermsScreenView)?.output as? TermsScreenPresenter)?.terms = term
        vc.hidesBottomBarWhenPushed = true
        view.navigationController?.pushViewController(vc, animated: true)
    }
}
