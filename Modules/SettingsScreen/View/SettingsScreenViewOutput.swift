//
//  SettingsScreenViewOutput.swift
//  Agent
//
//  Created by Sher Locked on 28.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol SettingsScreenViewOutput {
    func viewDidLoad()
    func cellTaped(with id: String)
    func backButtonTaped()
    func switchChanged(with id: String, newState: Bool)
}
