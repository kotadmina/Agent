//
//  SettingsScreenView.swift
//  Agent
//
//  Created by Sher Locked on 28.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class SettingsScreenView: UIViewController, SettingsScreenViewInput {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var agentNameLabel: UILabel!
    @IBOutlet weak var geoLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var boundView: UIView!
    @IBOutlet weak var settingsTableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    
    var output: SettingsScreenViewOutput!
    var cellViewModels: [[ProfileScreenCellViewModel]] = [[]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        output.viewDidLoad()
    }
    
    func configure() {
        view.backgroundColor = ColorsLibrary.mainDarkViolet
        avatarImageView.layer.cornerRadius = 9
        avatarImageView.contentMode = .scaleToFill
        avatarImageView.clipsToBounds = true
        agentNameLabel.textColor = UIColor.white
        agentNameLabel.font = FontLibrary.pragmaticaCondRegular(size: 24)
        geoLabel.textColor = UIColor.white
        geoLabel.font = FontLibrary.pragmaticaCondLight(size: 20)
        emailLabel.textColor = UIColor.white
        emailLabel.font = FontLibrary.pragmaticaCondLight(size: 20)
        boundView.backgroundColor = ColorsLibrary.mainYellow
        settingsTableView.delegate = self
        settingsTableView.dataSource = self
        let nib1 = UINib(nibName: "TextWithArrowCell", bundle: nil)
        settingsTableView.register(nib1, forCellReuseIdentifier: "TextWithArrowCell")
        let nib2 = UINib(nibName: "SwitchCell", bundle: nil)
        settingsTableView.register(nib2, forCellReuseIdentifier: "SwitchCell")
        settingsTableView.tableFooterView = createFooterView()
        let backImage = UIImage(named: "back_button")?.withRenderingMode(.alwaysOriginal)
        backButton.setImage(backImage, for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 25)
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
    }
    
    func updateTable(with viewModels: [[ProfileScreenCellViewModel]]) {
        cellViewModels = viewModels
        settingsTableView.reloadData()
    }
    
    func createFooterView() -> UIView {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 80))
        view.backgroundColor = UIColor.clear
        let footerImageView = UIImageView(image: UIImage(named: "footer_image"))
        footerImageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(footerImageView)
        NSLayoutConstraint.activate([footerImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                                     footerImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                     footerImageView.widthAnchor.constraint(equalToConstant: 149),
                                     footerImageView.heightAnchor.constraint(equalToConstant: 49)])
        return view
    }
    
    func configureHeader(fullName: String, geo: String, email: String, avatarName: String) {
        avatarImageView.image = UIImage(named: avatarName)
        agentNameLabel.text = fullName
        geoLabel.text = geo
        emailLabel.text = email
    }
    
    @objc func backButtonPressed() {
        output.backButtonTaped()
    }


}

extension SettingsScreenView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 16
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellViewModels[indexPath.section][indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let id = cellViewModels[indexPath.section][indexPath.row].id
        output.cellTaped(with: id)
    }
    
}

extension SettingsScreenView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: .zero)
        view.backgroundColor = UIColor.clear
        let yellowView = UIView()
        yellowView.backgroundColor = ColorsLibrary.mainYellow
        view.addSubview(yellowView)
        yellowView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([yellowView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                                     yellowView.heightAnchor.constraint(equalToConstant: 1),
                                     yellowView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 19),
                                     yellowView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -19)])
        return view
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return cellViewModels.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellViewModels[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = cellViewModels[indexPath.section][indexPath.row]
        switch viewModel.type {
        case .textCell:
            let cell = settingsTableView.dequeueReusableCell(withIdentifier: "TextWithArrowCell") as! TextWithArrowCell
            cell.configure(with: viewModel)
            return cell
        case .switchCell:
            let cell = settingsTableView.dequeueReusableCell(withIdentifier: "SwitchCell") as! SwitchCell
            cell.configure(with: viewModel)
            cell.delegate = self
            return cell
        default:
            return UITableViewCell()
        }
        
    }
}

extension SettingsScreenView: SwitchCellDelegate {
    func switchChanged(id: String, isOn: Bool) {
        output.switchChanged(with: id, newState: isOn)
    }
}
