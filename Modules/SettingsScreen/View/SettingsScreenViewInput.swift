//
//  SettingsScreenViewInput.swift
//  Agent
//
//  Created by Sher Locked on 28.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol SettingsScreenViewInput: class {
    func updateTable(with viewModels: [[ProfileScreenCellViewModel]])
    func configureHeader(fullName: String, geo: String, email: String, avatarName: String)
}
