//
//  TabBarAssembly.swift
//  Agent
//
//  Created by Sher Locked on 26.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class TabBarAssembly: BaseAssembly {
    
    
    static func configure() {
        let container = defaultContainer()
        
        //RegisterView
        container.register(TabBarViewInput.self) { r in
            let tabBar = TabBarView()
            tabBar.tabBar.barTintColor = ColorsLibrary.mainDarkViolet
            
            let firstVC = r.resolve(HomeScreenViewInput.self)! as! HomeScreenView
            let navFirst = UINavigationController(rootViewController: firstVC)
            navFirst.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "homeIcon_yellow")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "homeIcon_white")?.withRenderingMode(.alwaysOriginal))
            
            let secondVC = r.resolve(MissionsMapScreenInput.self) as! MissionsMapScreenView
            let navSecond = UINavigationController(rootViewController: secondVC)
            navSecond.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "mapIcon_yellow")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "mapIcon_white")?.withRenderingMode(.alwaysOriginal))
            
            let thirdVC = r.resolve(ProfileScreenInput.self) as! ProfileScreenView
            let navThird = UINavigationController(rootViewController: thirdVC)
            navThird.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "profileIcon_yellow")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "profileIcon_white")?.withRenderingMode(.alwaysOriginal))
            
            let forthVC = r.resolve(TaskScreenViewInput.self) as! TaskScreenView
            forthVC.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "taskIcon_yellow")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "taskIcon_white")?.withRenderingMode(.alwaysOriginal))
            
            let fifthVC = r.resolve(ChatScreenViewInput.self) as! ChatScreenView
            fifthVC.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "chatIcon_yellow")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "chatIcon_white")?.withRenderingMode(.alwaysOriginal))
            
            tabBar.viewControllers = [navFirst, navSecond, navThird, forthVC, fifthVC]
            let imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            navFirst.tabBarItem.imageInsets = imageInsets
            secondVC.tabBarItem.imageInsets = imageInsets
            thirdVC.tabBarItem.imageInsets = imageInsets
            forthVC.tabBarItem.imageInsets = imageInsets
            fifthVC.tabBarItem.imageInsets = imageInsets
            return tabBar
        }.inObjectScope(.transient)
    }
    
    
}
