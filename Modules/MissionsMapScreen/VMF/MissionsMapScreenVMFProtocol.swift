//
//  MissionsMapScreenVMFProtocol.swift
//  Agent
//
//  Created by Sher Locked on 20.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol MissionsMapScreenVMFProtocol {
    
    func createCategoryViewModels(with categories: [Category], selectedId: String?) -> [CategoriesIconViewModel]
    func createOpenMissionViewModel(with mission: Mission) -> OpenMissionMapViewModel
    
}
