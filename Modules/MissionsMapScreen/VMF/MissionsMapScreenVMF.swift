//
//  MissionsMapScreenVMF.swift
//  Agent
//
//  Created by Sher Locked on 20.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class MissionsMapScreenVMF: MissionsMapScreenVMFProtocol {
    
    func createCategoryViewModels(with categories: [Category], selectedId: String?) -> [CategoriesIconViewModel] {
        var viewModels: [CategoriesIconViewModel] = [CategoriesIconViewModel(id: nil,
                                                                             iconName: "all_categories_icon",
                                                                             isSelected: selectedId == nil)]
        let categoriesVM = categories.map { category -> CategoriesIconViewModel in
            let isSelected = category.id == selectedId
            return CategoriesIconViewModel(id: category.id,
                                           iconName: category.iconName,
                                           isSelected: isSelected)
        }
        viewModels.append(contentsOf: categoriesVM)
        return viewModels
    }
    
    func createOpenMissionViewModel(with mission: Mission) -> OpenMissionMapViewModel {
        let viewModel = OpenMissionMapViewModel(backgroundImageName: mission.headerImageName, headerString: mission.shortDescriptionText, expCount: mission.expCount, adressString: "улица Крымский вал, 10")
        return viewModel
    }
}
