//
//  MissionsMapScreenPresenter.swift
//  Agent
//
//  Created by Sher Locked on 27.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation
import YandexMapKit

class MissionsMapScreenPresenter: NSObject, MissionsMapScreenOutput {
    
    weak var view: MissionsMapScreenInput!
    var interactor: MissionsMapScreenInteractorInput!
    var vmf: MissionsMapScreenVMFProtocol!
    var router: MissionsMapScreenRouterInput!
    var missions: [Mission] = []
    var categories: [Category] = []
    var selectedCategoryId: String?
    var selectedMission: Mission?
    
    let locationManager = YMKMapKit.sharedInstance().createLocationManager()
    
    func viewDidLoad() {
        locationManager?.subscribeForLocationUpdates(withDesiredAccuracy: 5, minTime: 3000, minDistance: 5, allowUseInBackground: false, locationListener: self)
        
        categories = interactor.getCategories()
        let viewModels = vmf.createCategoryViewModels(with: categories, selectedId: nil)
        view.updateCollectionView(with: viewModels)
        loadMissions(with: nil)
        
    }
    
    func missionTapped() {
        guard let mission = selectedMission else {
            return
        }
        router.selectMission(with: mission)
    }
    
    func loadMissions(with category: Category?) {
        if let category = category {
            missions = interactor.getMissions(by: category)
        } else {
            missions = interactor.getAllMissions()
        }
        view.updateMap(with: missions)
    }
    
    func viewWillAppear() {

    }
    
    func selectCategory(with id: String?) {
        guard id != selectedCategoryId else {
            return
        }
        selectedCategoryId = id
        let viewModels = vmf.createCategoryViewModels(with: categories, selectedId: id)
        view.updateCollectionView(with: viewModels)
        let selectedCategory = categories.first(where: { $0.id == id })
        loadMissions(with: selectedCategory)
    }
    
    func mapObjectPressed(with missionId: String) {
        guard let mission = missions.first(where: {$0.id == missionId}) else {
            return
        }
        selectedMission = mission
        let viewModel = vmf.createOpenMissionViewModel(with: mission)
        view.openMissionView(with: viewModel)
    }
    
    
    
}

extension MissionsMapScreenPresenter: YMKLocationDelegate {
    func onLocationUpdated(with location: YMKLocation) {
        view.updateMap(with: location)
        locationManager?.suspend()
    }
    
    func onLocationStatusUpdated(with status: YMKLocationStatus) {
        
    }
    
}
