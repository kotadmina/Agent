//
//  MissionsMapScreenOutput.swift
//  Agent
//
//  Created by Sher Locked on 27.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol MissionsMapScreenOutput {
    func viewDidLoad()
    func viewWillAppear()
    func selectCategory(with id: String?)
    func mapObjectPressed(with missionId: String)
    func missionTapped()
}
