//
//  MissionsMapScreenView.swift
//  Agent
//
//  Created by Sher Locked on 27.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import YandexMapKit

class MissionsMapScreenView: UIViewController, MissionsMapScreenInput {
    
    @IBOutlet weak var missionViewSecondConstraint: NSLayoutConstraint!
    @IBOutlet weak var missionView: MissionMapView!
    @IBOutlet weak var missionViewFirstConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewHeaderLabel: UILabel!
    @IBOutlet weak var mapView: YMKMapView!
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var mapPlusView: UIImageView!
    @IBOutlet weak var mapMinusView: UIImageView!
    @IBOutlet weak var mapLocationView: UIImageView!
    @IBOutlet weak var mapControlsCenterClosedConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapControlsCenterOpenedConstraint: NSLayoutConstraint!
    var cellViewModels: [CategoriesIconViewModel] = []
    var userLocation: YMKLocation?
    var openMissionStatus = false
    
    var output: MissionsMapScreenOutput!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        configure()
        output.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output.viewWillAppear()
    }
    
    func configure() {
        let userLocationLayer = mapView.mapWindow.map?.userLocationLayer
        userLocationLayer?.isEnabled = true
        userLocationLayer?.setObjectListenerWith(self)
        mapView.mapWindow.map?.addInputListener(with: self)
        let logoAlignment = YMKLogoAlignment(horizontalAlignment: .left, verticalAlignment: .top)
        mapView.mapWindow.map?.logo?.setAlignmentWith(logoAlignment)
        viewHeaderLabel.text = "Карта миссий:".uppercased()
        viewHeaderLabel.textColor = ColorsLibrary.mainDarkViolet
        viewHeaderLabel.font = FontLibrary.pragmaticaCondRegular(size: 36)
        categoriesCollectionView.backgroundColor = ColorsLibrary.mainDarkViolet
        categoriesCollectionView.delegate = self
        categoriesCollectionView.dataSource = self
        let nib = UINib(nibName: "CategoriesCollectionViewCell", bundle: nil)
        categoriesCollectionView.register(nib, forCellWithReuseIdentifier: "CategoriesCollectionViewCell")
        let tap = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        missionView.addGestureRecognizer(tap)
        configureControls()
    }
    
    func configureControls() {
        let plusGesture = UITapGestureRecognizer(target: self, action: #selector(plusButtonPressed))
        mapPlusView.addGestureRecognizer(plusGesture)
        mapPlusView.isUserInteractionEnabled = true
        mapPlusView.image = UIImage(named: "map_icon_plus")
        
        let minusGesture = UITapGestureRecognizer(target: self, action: #selector(minusButtonPressed))
        mapMinusView.addGestureRecognizer(minusGesture)
        mapMinusView.isUserInteractionEnabled = true
        mapMinusView.image = UIImage(named: "map_icon_minus")
        
        let navigateGesture = UITapGestureRecognizer(target: self, action: #selector(navigateButtonPressed))
        mapLocationView.addGestureRecognizer(navigateGesture)
        mapLocationView.isUserInteractionEnabled = true
        mapLocationView.image = UIImage(named: "map_icon_navigate")
    }
    
    @objc func plusButtonPressed() {
        guard let cameraPosition = mapView.mapWindow.map?.cameraPosition else {
            return
        }
        let newZoom = cameraPosition.zoom + 1
        let newCameraPosition = YMKCameraPosition(target: cameraPosition.target, zoom: newZoom, azimuth: cameraPosition.azimuth, tilt: cameraPosition.tilt)
        let animation = YMKAnimation(type: .smooth, duration: 0.3)
        mapView.mapWindow.map?.move(with: newCameraPosition, animationType: animation, cameraCallback: nil)
    }
    
    @objc func minusButtonPressed() {
        guard let cameraPosition = mapView.mapWindow.map?.cameraPosition else {
            return
        }
        let newZoom = cameraPosition.zoom - 1
        let newCameraPosition = YMKCameraPosition(target: cameraPosition.target, zoom: newZoom, azimuth: cameraPosition.azimuth, tilt: cameraPosition.tilt)
        let animation = YMKAnimation(type: .smooth, duration: 0.3)
        mapView.mapWindow.map?.move(with: newCameraPosition, animationType: animation, cameraCallback: nil)
    }
    
    @objc func navigateButtonPressed() {
        guard let location = userLocation else {
            return
        }
        let position = YMKCameraPosition(target: location.position, zoom: 15, azimuth: 0, tilt: 0)
        let animation = YMKAnimation(type: .smooth, duration: 0.3)
        mapView.mapWindow.map?.move(with: position, animationType: animation, cameraCallback: nil)
    }
    
    func updateCollectionView(with viewModels: [CategoriesIconViewModel]) {
        cellViewModels = viewModels
        categoriesCollectionView.reloadData()
    }
    
    @objc func viewTapped() {
        output.missionTapped()
    }
    
    func updateMap(with userLocation: YMKLocation) {
        self.userLocation = userLocation
        let position = YMKCameraPosition(target: userLocation.position, zoom: 15, azimuth: 0, tilt: 0)
        mapView.mapWindow.map?.move(with: position)
    }
    
    func updateMap(with missions: [Mission]) {
        mapView.mapWindow.map?.mapObjects?.clear()
        for mission in missions {
            guard let lat = mission.latitude, let lon = mission.longitude else {
                continue
            }
            
            let point = YMKPoint(latitude: lat, longitude: lon)
            let placemark = mapView.mapWindow.map?.mapObjects?
                .addPlacemark(with: point, image: UIImage(named: "mission_map_marker_yellow"))
            
            placemark?.setIconStyleWith(YMKIconStyle(anchor: nil,
                                                     rotationType: nil,
                                                     zIndex: nil,
                                                     flat: nil,
                                                     visible: nil,
                                                     scale: 1.5,
                                                     tappableArea: nil))
            placemark?.userData = mission.id
            placemark?.addTapListener(with: self)
        }
    }
    
    func openMissionView(with viewModel: OpenMissionMapViewModel) {
        if openMissionStatus {
            closeMissionView {
                self.setupMissionView(with: viewModel)
            }
        } else {
            setupMissionView(with: viewModel)
        }
    }
    
    func setupMissionView(with viewModel: OpenMissionMapViewModel) {
        missionView.configure(with: viewModel)
        missionViewFirstConstraint.isActive = false
        missionViewSecondConstraint.isActive = true
        mapControlsCenterOpenedConstraint.isActive = true
        mapControlsCenterClosedConstraint.isActive = false
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        }) { _ in
            self.openMissionStatus = true
        }
        
    }
    
    func closeMissionView(completion: @escaping () -> Void) {
        missionViewFirstConstraint.isActive = true
        missionViewSecondConstraint.isActive = false
        mapControlsCenterOpenedConstraint.isActive = false
        mapControlsCenterClosedConstraint.isActive = true
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        }) { _ in
            self.openMissionStatus = false
            completion()
        }
    }
    
  

}

extension MissionsMapScreenView: YMKUserLocationObjectListener {
    func onObjectAdded(with view: YMKUserLocationView?) {
        view?.pin?.setIconWith(UIImage(named: "user_location_circle"))
        view?.arrow?.setIconWith(UIImage(named: "user_location_circle"))
        view?.accuracyCircle?.fillColor = ColorsLibrary.mapAccuracy
    }
    
    func onObjectRemoved(with view: YMKUserLocationView?) {
        
    }
    
    func onObjectUpdated(with view: YMKUserLocationView?, event: YMKObjectEvent?) {
        
    }
}

extension MissionsMapScreenView: YMKMapObjectTapListener {
    func onMapObjectTap(with mapObject: YMKMapObject?, point: YMKPoint) -> Bool {
        guard let missionId = mapObject?.userData as? String else {
            return true
        }
        output.mapObjectPressed(with: missionId)
        return true
    }
}

extension MissionsMapScreenView: YMKMapInputListener {
    func onMapTap(with map: YMKMap?, point: YMKPoint) {
        closeMissionView {
        }
    }
    
    func onMapLongTap(with map: YMKMap?, point: YMKPoint) {
        closeMissionView {
        }
    }
}

extension MissionsMapScreenView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewModel = cellViewModels[indexPath.row]
        output.selectCategory(with: viewModel.id)
    }
}

extension MissionsMapScreenView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellViewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = categoriesCollectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCollectionViewCell", for: indexPath) as! CategoriesCollectionViewCell
        cell.configure(with: cellViewModels[indexPath.row])
        return cell
    }
    
    
}

extension MissionsMapScreenView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewHeight = categoriesCollectionView.frame.size.height
        
        return CGSize(width: 50, height: collectionViewHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
