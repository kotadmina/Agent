//
//  MissionsMapScreenInput.swift
//  Agent
//
//  Created by Sher Locked on 27.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation
import YandexMapKit

protocol MissionsMapScreenInput: class {
    func updateMap(with userLocation: YMKLocation)
    func updateMap(with missions: [Mission])
    func updateCollectionView(with viewModels: [CategoriesIconViewModel])
    func openMissionView(with viewModel: OpenMissionMapViewModel)
}
