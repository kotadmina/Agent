//
//  MissionsMapScreenAssembly.swift
//  Agent
//
//  Created by Sher Locked on 27.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class MissionsMapScreenAssembly: BaseAssembly {
    
    
    static func configure() {
        let container = defaultContainer()
        
        //RegisterView
        container.register(MissionsMapScreenInput.self) { r in
            let storyboard = UIStoryboard(name: "MissionsMapScreenView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MissionsMapScreenView") as! MissionsMapScreenView
            vc.output = r.resolve(MissionsMapScreenOutput.self, argument: (vc as MissionsMapScreenInput))!
            return vc
        }.inObjectScope(.transient)
        
        //Register Presenter
        container.register(MissionsMapScreenOutput.self) { (r: Resolver, view: MissionsMapScreenInput) in
            let presenter = MissionsMapScreenPresenter()
            presenter.view = view
            presenter.interactor = r.resolve(MissionsMapScreenInteractorInput.self)!
            presenter.vmf = r.resolve(MissionsMapScreenVMFProtocol.self)!
            presenter.router = r.resolve(MissionsMapScreenRouterInput.self, argument: view as! UIViewController)!
            return presenter
        }.inObjectScope(.transient)
        
        //register Interactor
        container.register(MissionsMapScreenInteractorInput.self) { r in
            let interactor = MissionsMapScreenInteractor()
            interactor.categoriesRepository = r.resolve(CategoriesRepositoryProtocol.self)!
            interactor.missionsRepository = r.resolve(MissionsRepositoryProtocol.self)!
            return interactor
        }.inObjectScope(.transient)
        
        //register VMF
        container.register(MissionsMapScreenVMFProtocol.self) { _ in
            let vmf = MissionsMapScreenVMF()
            return vmf
        }.inObjectScope(.transient)
        
        //register router
        container.register(MissionsMapScreenRouterInput.self) { (r: Resolver, view: UIViewController) in
            let router = MissionsMapScreenRouter()
            router.view = view
            return router
            }.inObjectScope(.transient)
        
    }
    
    
    
}
