//
//  MissionMapScreenRouterInput.swift
//  Agent
//
//  Created by Sher Locked on 13.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol MissionsMapScreenRouterInput {
    func selectMission(with mission: Mission)
}
