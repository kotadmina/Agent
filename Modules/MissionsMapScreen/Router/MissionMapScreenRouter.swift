//
//  MissionMapScreenRouter.swift
//  Agent
//
//  Created by Sher Locked on 13.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class MissionsMapScreenRouter: MissionsMapScreenRouterInput {
    weak var view: UIViewController!
    
    func selectMission(with mission: Mission) {
        let vc = ModulesAssembly.resolve(type: MissionDetailViewInput.self) as! UIViewController
        ((vc as? MissionDetailView)?.output as? MissionDetailPresenter)?.mission = mission
        vc.hidesBottomBarWhenPushed = true
        view.navigationController?.pushViewController(vc, animated: true)
    }
}

