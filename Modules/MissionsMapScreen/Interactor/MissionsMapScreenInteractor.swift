//
//  MissionsMapScreenInteractor.swift
//  Agent
//
//  Created by Sher Locked on 27.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class MissionsMapScreenInteractor: MissionsMapScreenInteractorInput {
    
    var categoriesRepository: CategoriesRepositoryProtocol!
    var missionsRepository: MissionsRepositoryProtocol!
    
    func getCategories() -> [Category] {
        return categoriesRepository.getCategories()
    }
    
    func getAllMissions() -> [Mission] {
        return missionsRepository.getAllMissions()
    }
    
    func getMissions(by category: Category) -> [Mission] {
        return missionsRepository.getMissions(by: category)
    }
}
