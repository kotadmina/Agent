//
//  ChatScreenView.swift
//  Agent
//
//  Created by Sher Locked on 01.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class ChatScreenView: UIViewController, ChatScreenViewInput {

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    func configure() {
        navigationController?.navigationBar.isHidden = true
    }
   

}
