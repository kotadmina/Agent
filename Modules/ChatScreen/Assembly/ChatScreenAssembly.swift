//
//  ChatScreenAssembly.swift
//  Agent
//
//  Created by Sher Locked on 01.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class ChatScreenAssembly: BaseAssembly {
    
    static func configure() {
        let container = defaultContainer()
        
        //RegisterView
        container.register(ChatScreenViewInput.self) { r in
            let storyboard = UIStoryboard(name: "ChatScreenView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ChatScreenView") as! ChatScreenView
            //vc.output = r.resolve(MissionsMapScreenOutput.self, argument: (vc as MissionsMapScreenInput))!
            return vc
            }.inObjectScope(.transient)
        
        //Register Presenter
        //        container.register(ProfileScreenOutput.self) { (r: Resolver, view: ProfileScreenInput) in
        //            let presenter = ProfileScreenPresenter()
        //            presenter.view = view
        //            return presenter
        //            }.inObjectScope(.transient)
        
    }
    
}
