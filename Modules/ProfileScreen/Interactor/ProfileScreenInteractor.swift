//
//  ProfileScreenInteractor.swift
//  Agent
//
//  Created by Sher Locked on 27.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class ProfileScreenInteractor: ProfileScreenInteractorInput {
    
    var userRepository: UserRepositoryProtocol!
    
    func getUserInfo() -> User {
        return userRepository.getUser()
    }
    
}
