//
//  ProfileScreenInteractorInput.swift
//  Agent
//
//  Created by Sher Locked on 27.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol ProfileScreenInteractorInput {
    func getUserInfo() -> User
}
