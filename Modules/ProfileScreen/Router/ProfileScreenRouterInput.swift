//
//  ProfileScreenRouterInput.swift
//  Agent
//
//  Created by Sher Locked on 13.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

protocol ProfileScreenRouterInput {
    func openReadyMissions()
    func openRewards()
    func openInProcessMissions()
    func settingsPressed()
}
