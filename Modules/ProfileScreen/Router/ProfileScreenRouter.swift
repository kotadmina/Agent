//
//  ProfileScreenRouter.swift
//  Agent
//
//  Created by Sher Locked on 13.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class ProfileScreenRouter: ProfileScreenRouterInput {
    
    weak var view: ProfileScreenView!
    var openedController: UIViewController?
    
    lazy var readyMissionsController: UIViewController = {
        return ModulesAssembly.resolve(type: MissionsListViewInput.self, tag: ReadyMissionsAssembly.tag) as! UIViewController
    }()
    
    lazy var rewardsController: UIViewController = {
        return ModulesAssembly.resolve(type: RewardsViewInput.self) as! UIViewController
    }()
    
    lazy var inProcessMissionsController: UIViewController = {
        return ModulesAssembly.resolve(type: MissionsListViewInput.self, tag: InProcessMissionsAssembly.tag) as! UIViewController
    }()
    
    private func add(asChildViewController viewController: UIViewController) {
        view.addChildViewController(viewController)
        view.containerView.addSubview(viewController.view)
        viewController.view.frame = view.containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParentViewController: view)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
    }
    
    func openReadyMissions() {
        if let openedController = self.openedController {
            remove(asChildViewController: openedController)
        }
        
        add(asChildViewController: readyMissionsController)
        openedController = readyMissionsController
    }
    
    func openRewards() {
        if let openedController = self.openedController {
            remove(asChildViewController: openedController)
        }
        
        add(asChildViewController: rewardsController)
        openedController = rewardsController
    }
    
    func openInProcessMissions() {
        if let openedController = self.openedController {
            remove(asChildViewController: openedController)
        }
        
        add(asChildViewController: inProcessMissionsController)
        openedController = inProcessMissionsController
    }
    
    func settingsPressed() {
        let vc = ModulesAssembly.resolve(type: SettingsScreenViewInput.self) as! UIViewController
        vc.hidesBottomBarWhenPushed = true
        view.navigationController?.pushViewController(vc, animated: true)
    }
    
}
