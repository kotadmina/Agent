//
//  ProfileScreenPresenter.swift
//  Agent
//
//  Created by Sher Locked on 13.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class ProfileScreenPresenter: ProfileScreenOutput {
    
    weak var view: ProfileScreenInput!
    var router: ProfileScreenRouterInput!
    var interactor: ProfileScreenInteractorInput!
    
    func viewDidLoad() {
        router.openInProcessMissions()
        let user = interactor.getUserInfo()
        view.updateHeaderInfo(fullName: user.name.uppercased() + " " + user.surname.uppercased(), level: user.levelCount, experience: user.experienceCount, avatarName: user.avatarImageName)
    }
    
    func segmentChanged(newState: ProfileTripleSegmentState) {
        switch newState {
        case .inProcessMission:
            router.openInProcessMissions()
        case .readyMission:
            router.openReadyMissions()
        case .rewards:
            router.openRewards()
        }
    }
    
    func seetingsButtonPressed() {
        router.settingsPressed()
    }

}
