//
//  RewardsInteractor.swift
//  Agent
//
//  Created by Sher Locked on 16.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class RewardsInteractor: RewardsInteractorInput {
    
    func getRewards() -> [Reward] {
       
        let rew1 = Reward(description: "Скидка 30% на корма для собак Royal Canin",
                          expireDate: Date().addingTimeInterval(600),
                          rewardImageName: "exhib_mission3",
                          rewardCategoryId: "1")
        let rew2 = Reward(description: "Подарочный билет в музей ИРРИ",
                          expireDate: Date().addingTimeInterval(600),
                          rewardImageName: "exhib_mission2",
                          rewardCategoryId: "1")
        let rew3 = Reward(description: "Какие-то плюшки",
                          expireDate: Date().addingTimeInterval(600),
                          rewardImageName: "kiaHeaderImage",
                          rewardCategoryId: "2")
        return [rew1, rew2, rew3]
    }
    
}
