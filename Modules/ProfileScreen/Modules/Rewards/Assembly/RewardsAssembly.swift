//
//  RewardsAssembly.swift
//  Agent
//
//  Created by Sher Locked on 16.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class RewardsAssembly: BaseAssembly {
    
    static func configure() {
        let container = defaultContainer()
        
        //register view
        container.register(RewardsViewInput.self) { r in
            let storyboard = UIStoryboard(name: "RewardsView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RewardsView") as! RewardsView
            vc.output = r.resolve(RewardsViewOutput.self, argument: (vc as RewardsViewInput))!
            return vc
            }.inObjectScope(.transient)
        
        //register presenter
        container.register(RewardsViewOutput.self) { (r: Resolver, view: RewardsViewInput) in
            let presenter = RewardsPresenter()
            presenter.view = view
            presenter.vmf = r.resolve(RewardsVMFProtocol.self)!
            presenter.interactor = r.resolve(RewardsInteractorInput.self)!
            //presenter.router = r.resolve(CategoryScreenRouterInput.self, argument: view as! UIViewController)!
            return presenter
            }.inObjectScope(.transient)
        
        //register interactor
        container.register(RewardsInteractorInput.self) { _ in
            let interactor = RewardsInteractor()
            return interactor
            }.inObjectScope(.transient)
        
        //register vmf
        container.register(RewardsVMFProtocol.self) { _ in
            let vmf = RewardsVMF()
            return vmf
            }.inObjectScope(.transient)
        
        //register router
//        container.register(CategoryScreenRouterInput.self) { (r: Resolver, view: UIViewController) in
//            let router = CategoryScreenRouter()
//            router.view = view
//            return router
//            }.inObjectScope(.transient)
    }
    
    
}

