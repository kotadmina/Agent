//
//  RewardsPresenter.swift
//  Agent
//
//  Created by Sher Locked on 16.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class RewardsPresenter: RewardsViewOutput {
    
    weak var view: RewardsViewInput!
    var vmf: RewardsVMFProtocol!
    var interactor: RewardsInteractorInput!
    var rewards: [Reward] = []
    
    func viewDidLoad() {
        rewards = interactor.getRewards()
        let viewModels = vmf.createRewardViewModels(with: rewards)
        view.updateCollectionView(with: viewModels)
    }
}
