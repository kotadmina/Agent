//
//  RewardsViewInput.swift
//  Agent
//
//  Created by Sher Locked on 14.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol RewardsViewInput: class {
    func updateCollectionView(with viewModels: [RewardCollectionCellViewModel])
}
