//
//  RewardsView.swift
//  Agent
//
//  Created by Sher Locked on 14.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class RewardsView: UIViewController, RewardsViewInput {

    @IBOutlet weak var rewardsCollectionView: UICollectionView!
    var output: RewardsViewOutput!
    var cellViewModels: [RewardCollectionCellViewModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        output.viewDidLoad()
    }
    
    func configure() {
        view.backgroundColor = ColorsLibrary.mainDarkViolet
        rewardsCollectionView.backgroundColor = UIColor.clear
        rewardsCollectionView.dataSource = self
        rewardsCollectionView.delegate = self
        let cellNib = UINib(nibName: "RewardsCollectionViewCell", bundle: nil)
        rewardsCollectionView.register(cellNib, forCellWithReuseIdentifier: "RewardsCollectionViewCell")
    }
    
    func updateCollectionView(with viewModels: [RewardCollectionCellViewModel]) {
        cellViewModels = viewModels
        rewardsCollectionView.reloadData()
    }

    
}

extension RewardsView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  16
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
}

extension RewardsView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "CollectionHeaderView",
                                                                             for: indexPath) as! CollectionHeaderView
            headerView.configure()
            return headerView
        default:
            assert(false, "Unexpected element kind")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellViewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = rewardsCollectionView.dequeueReusableCell(withReuseIdentifier: "RewardsCollectionViewCell", for: indexPath) as! RewardsCollectionViewCell
        cell.configure(with: cellViewModels[indexPath.row])
        return cell
    }
    
}

extension RewardsView: UICollectionViewDelegate {
    
}
