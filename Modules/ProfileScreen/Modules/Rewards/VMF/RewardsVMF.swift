//
//  RewardsVMF.swift
//  Agent
//
//  Created by Sher Locked on 16.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class RewardsVMF: RewardsVMFProtocol {
    
    func createRewardViewModels(with rewards: [Reward]) -> [RewardCollectionCellViewModel] {
        var viewModels: [RewardCollectionCellViewModel] = []
        for reward in rewards {
            if let imageName = reward.rewardImageName, let date = reward.expireDate {
                let viewModel = RewardCollectionCellViewModel(rewardImageName: imageName, rewardText: reward.description, date: date)
                viewModels.append(viewModel)
            }

        }
        return viewModels
    }
    
}
