//
//  ReadyMissionsRouterInput.swift
//  Agent
//
//  Created by Sher Locked on 22.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol ReadyMissionsRouterInput {
    func selectMission(with mission: Mission)
}
