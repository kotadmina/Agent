//
//  ReadyMissionsRouter.swift
//  Agent
//
//  Created by Sher Locked on 22.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class ReadyMissionsRouter: ReadyMissionsRouterInput {
    
    weak var view: UIViewController!
    
    func selectMission(with mission: Mission) {
        let vc = ModulesAssembly.resolve(type: MissionDetailViewInput.self) as! UIViewController
        ((vc as? MissionDetailView)?.output as? MissionDetailPresenter)?.mission = mission
        vc.hidesBottomBarWhenPushed = true
        view.parent?.navigationController?.pushViewController(vc, animated: true)
    }
}
