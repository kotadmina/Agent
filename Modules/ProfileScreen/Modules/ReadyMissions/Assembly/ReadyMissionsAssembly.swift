//
//  ReadyMissionsAssembly.swift
//  Agent
//
//  Created by Sher Locked on 16.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class ReadyMissionsAssembly: BaseAssembly {
    
    static let tag = "ReadyMissions"
    
    static func configure() {
        let container = defaultContainer()
        
        //Register View
        container.register(MissionsListViewInput.self, name: ReadyMissionsAssembly.tag) { r in
            let vc = MissionsListView()
            vc.output = r.resolve(MissionsListViewOutput.self, name: ReadyMissionsAssembly.tag, argument: (vc as MissionsListViewInput))!
            return vc
            }.inObjectScope(.transient)
        
        //Register Interactor
        container.register(ReadyMissionsInteractorInput.self) { r in
            let interactor = ReadyMissionsInteractor()
            interactor.missionsRepository = r.resolve(MissionsRepositoryProtocol.self)!
            return interactor
            }.inObjectScope(.transient)
        
        //Register Presenter
        container.register(MissionsListViewOutput.self, name: ReadyMissionsAssembly.tag) { (r: Resolver, view: MissionsListViewInput) in
            let presenter = ReadyMissionsPresenter()
            presenter.view = view
            presenter.interactor = r.resolve(ReadyMissionsInteractorInput.self)!
            presenter.router = r.resolve(ReadyMissionsRouterInput.self, argument: view as! UIViewController)!
            presenter.vmf = r.resolve(ReadyMissionsVMFProtocol.self)!
            return presenter
            }.inObjectScope(.transient)
        
        //Register Router
        container.register(ReadyMissionsRouterInput.self) { (r: Resolver, view: UIViewController) in
            let router = ReadyMissionsRouter()
            router.view = view
            return router
            }.inObjectScope(.transient)
        
        //Register VMF
        container.register(ReadyMissionsVMFProtocol.self) { _ in
            let vmf = ReadyMissionsVMF()
            return vmf
            }.inObjectScope(.transient)
    }
    
}
