//
//  ReadyMissionsInteractor.swift
//  Agent
//
//  Created by Sher Locked on 16.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class ReadyMissionsInteractor: ReadyMissionsInteractorInput {
    
    var missionsRepository: MissionsRepositoryProtocol!
    
    func getReadyMissions() -> [Mission] {
        return missionsRepository.getReadyMissions()
    }
    
}
