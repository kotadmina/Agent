//
//  ReadyMissionsPresener.swift
//  Agent
//
//  Created by Sher Locked on 16.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class ReadyMissionsPresenter: MissionsListViewOutput {
    
    weak var view: MissionsListViewInput!
    var vmf: ReadyMissionsVMFProtocol!
    var interactor: ReadyMissionsInteractorInput!
    var missions: [Mission] = []
    var router: ReadyMissionsRouterInput!
    
    func viewDidLoad() {
        
        missions = interactor.getReadyMissions()
        let viewModels = vmf.createReadyMissionsViewModels(with: missions)
        view.updateTable(with: viewModels, headerText: "Выполненные миссии:")
    }
    
    func selectMission(with id: String) {
        guard let mission = missions.first(where: {$0.id == id}) else {
            return
        }
        router.selectMission(with: mission)
    }
    
    
}
