//
//  ReadyMissionsVMF.swift
//  Agent
//
//  Created by Sher Locked on 16.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class ReadyMissionsVMF: ReadyMissionsVMFProtocol {
    
    func createReadyMissionsViewModels(with missions: [Mission]) -> [CategoryOpenCellViewModel] {
        var viewModels: [CategoryOpenCellViewModel] = []
        for mission in missions {
            let dateString = mission.dateWhenReady?.timeFromNow()
            let viewModel = CategoryOpenCellViewModel(missionImageName: mission.headerImageName,
                                                      missionDescription: mission.shortDescriptionText,
                                                      missionId: mission.id,
                                                      dateString: dateString,
                                                      starCount: "\(mission.expCount)")
            viewModels.append(viewModel)
        }
        
        return viewModels
    }
    
}
