//
//  MissionsListViewInput.swift
//  Agent
//
//  Created by Sher Locked on 14.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol MissionsListViewInput: class {
    func updateTable(with viewModels: [CategoryOpenCellViewModel], headerText: String)
}
