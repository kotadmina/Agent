//
//  MissionsListView.swift
//  Agent
//
//  Created by Sher Locked on 14.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class MissionsListView: UIViewController, MissionsListViewInput {
    
    var tableView: UITableView!
    var headerLabel: UILabel!
    var missionsViewModels: [CategoryOpenCellViewModel] = []
    var output: MissionsListViewOutput!

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        output.viewDidLoad()
    }
    
    func configure() {
        view.backgroundColor = UIColor.white
        tableView = UITableView(frame: view.bounds)
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 270
        let nib = UINib(nibName: "CategoryOpenCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "CategoryOpenCell")
        tableView.delegate = self
        tableView.dataSource = self
        configureHeaderView()
        view.addSubview(tableView)
    }
    
    func configureHeaderView() {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: 40))
        headerView.backgroundColor = UIColor.clear
        headerLabel = UILabel(frame: CGRect(x: 17, y: 1, width: view.bounds.width, height: 38))
        headerLabel.font = FontLibrary.pragmaticaCondLight(size: 30)
        headerLabel.textColor = ColorsLibrary.mainDarkViolet
        headerView.addSubview(headerLabel)
        self.tableView.tableHeaderView = headerView
    }
    
    func updateTable(with viewModels: [CategoryOpenCellViewModel], headerText: String) {
        missionsViewModels = viewModels
        tableView.reloadData()
        headerLabel.text = headerText
    }


}

extension MissionsListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = missionsViewModels[indexPath.row] as? CategoryOpenCellViewModel else {
            return
        }
        
        output.selectMission(with: viewModel.missionId)
    }
    
}

extension MissionsListView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return missionsViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryOpenCell") as! CategoryOpenCell
        cell.configure(with: missionsViewModels[indexPath.row])
        return cell
    }
}
