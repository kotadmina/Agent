//
//  InProcessMissionsPresenter.swift
//  Agent
//
//  Created by Sher Locked on 15.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class InProcessMissionsPresenter: MissionsListViewOutput {
    
    weak var view: MissionsListViewInput!
    var vmf: InProcessMissionsVMFProtocol!
    var interactor: InProcessMissionsInteractorInput!
    var router: InProcessMissionsRouterInput!
    var missions: [Mission] = []
    
    func viewDidLoad() {
        
        missions = interactor.getInProcessMissions()
        let viewModels = vmf.createInProcessViewModels(missions: missions)
        view.updateTable(with: viewModels, headerText: "Текущие миссии:")
    }
    
    func selectMission(with id: String) {
        guard let mission = missions.first(where: {$0.id == id}) else {
            return
        }
        router.selectMission(with: mission)
    }
    
    
    
    
}
