//
//  InProcessMissionsRouterInput.swift
//  Agent
//
//  Created by Sher Locked on 22.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol InProcessMissionsRouterInput {
    func selectMission(with mission: Mission)
}
