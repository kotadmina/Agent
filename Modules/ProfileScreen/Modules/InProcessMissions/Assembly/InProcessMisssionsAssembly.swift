//
//  InProcessMisssionsAssembly.swift
//  Agent
//
//  Created by Sher Locked on 15.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//
import UIKit
import Swinject

class InProcessMissionsAssembly: BaseAssembly {
    
    static let tag = "InProcessMissions"
    
    static func configure() {
        let container = defaultContainer()
        
        //Register View
        container.register(MissionsListViewInput.self, name: InProcessMissionsAssembly.tag) { r in
            let vc = MissionsListView()
            vc.output = r.resolve(MissionsListViewOutput.self, name: InProcessMissionsAssembly.tag, argument: (vc as MissionsListViewInput))!
            return vc
            }.inObjectScope(.transient)
        
        //Register Interactor
        container.register(InProcessMissionsInteractorInput.self) { r in
            let interactor = InProcessMissionsInteractor()
            interactor.missionsRepository = r.resolve(MissionsRepositoryProtocol.self)!
            return interactor
            }.inObjectScope(.transient)
        
        //Register Presenter
        container.register(MissionsListViewOutput.self, name: InProcessMissionsAssembly.tag) { (r: Resolver, view: MissionsListViewInput) in
            let presenter = InProcessMissionsPresenter()
            presenter.view = view
            presenter.interactor = r.resolve(InProcessMissionsInteractorInput.self)!
            presenter.router = r.resolve(InProcessMissionsRouterInput.self, argument: view as! UIViewController)!
            presenter.vmf = r.resolve(InProcessMissionsVMFProtocol.self)!
            return presenter
            }.inObjectScope(.transient)
        
        //Register Router
        container.register(InProcessMissionsRouterInput.self) { (r: Resolver, view: UIViewController) in
            let router = InProcessMissionsRouter()
            router.view = view
            return router
            }.inObjectScope(.transient)
        
        //Register VMF
        container.register(InProcessMissionsVMFProtocol.self) { _ in
            let vmf = InProcessMissionsVMF()
            return vmf
            }.inObjectScope(.transient)
    }
    
}

