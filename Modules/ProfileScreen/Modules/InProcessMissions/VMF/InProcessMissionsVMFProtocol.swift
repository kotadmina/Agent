//
//  InProcessMissionsVMFProtocol.swift
//  Agent
//
//  Created by Sher Locked on 15.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol InProcessMissionsVMFProtocol {
    
    func createInProcessViewModels(missions: [Mission]) -> [CategoryOpenCellViewModel]
}
