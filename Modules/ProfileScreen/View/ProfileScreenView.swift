//
//  ProfileScreenView.swift
//  Agent
//
//  Created by Sher Locked on 27.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class ProfileScreenView: UIViewController, ProfileScreenInput {

    @IBOutlet weak var agentFileLabel: UILabel!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var experienceLabel: UILabel!
    @IBOutlet weak var segmentControlView: ProfiletripleSegment!
    @IBOutlet weak var containerView: UIView!
    
    var output: ProfileScreenOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewDidLoad()
        configure()
    }
    
    func configure() {
        self.navigationController?.navigationBar.isHidden = true
        view.backgroundColor = UIColor.white
        avatarImageView.layer.cornerRadius = 9
        avatarImageView.clipsToBounds = true
        segmentControlView.backgroundColor = ColorsLibrary.mainDarkViolet
        segmentControlView.firstButtonPressed()
        segmentControlView.delegate = self
        agentFileLabel.text = "Досье агента:".uppercased()
        agentFileLabel.textColor = ColorsLibrary.mainDarkViolet
        agentFileLabel.font = FontLibrary.pragmaticaCondRegular(size: 36)
        nameLabel.textColor = ColorsLibrary.mainDarkViolet
        nameLabel.font = FontLibrary.pragmaticaCondRegular(size: 24)
        levelLabel.textColor = ColorsLibrary.mainDarkViolet
        levelLabel.font = FontLibrary.pragmaticaCondRegular(size: 20)
        experienceLabel.textColor = ColorsLibrary.mainDarkViolet
        experienceLabel.font = FontLibrary.pragmaticaCondRegular(size: 20)
        let settingsImage = UIImage(named: "settings_button")?.withRenderingMode(.alwaysOriginal)
        settingsButton.setImage(settingsImage, for: .normal)
        settingsButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 0)
        settingsButton.addTarget(self, action: #selector(settingsButtonPressed), for: .touchUpInside)
    }
    
    func updateHeaderInfo(fullName: String, level: Int, experience: Int, avatarName: String) {
        nameLabel.text = fullName
        levelLabel.text = "Уровень \(level)"
        experienceLabel.text = "Опыт \(experience)"
        avatarImageView.image = UIImage(named: avatarName)
    }
    
    @objc func settingsButtonPressed() {
        output.seetingsButtonPressed()
    }

}

extension ProfileScreenView: TripleSegmentDelegate {
    func segmentChanged(newState: ProfileTripleSegmentState) {
        output.segmentChanged(newState: newState)
    }
    
    
}
