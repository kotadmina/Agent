//
//  ProfileScreenInput.swift
//  Agent
//
//  Created by Sher Locked on 27.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol ProfileScreenInput: class {
    func updateHeaderInfo(fullName: String, level: Int, experience: Int, avatarName: String)
}
