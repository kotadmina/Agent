//
//  ProfileScreenAssembly.swift
//  Agent
//
//  Created by Sher Locked on 01.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class ProfileScreenAssembly: BaseAssembly {
    
    
    static func configure() {
        let container = defaultContainer()
        
        //RegisterView
        container.register(ProfileScreenInput.self) { r in
            let storyboard = UIStoryboard(name: "ProfileScreenView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileScreenView") as! ProfileScreenView
            vc.output = r.resolve(ProfileScreenOutput.self, argument: (vc as ProfileScreenInput))!
            return vc
            }.inObjectScope(.transient)
        
        //register presenter
        container.register(ProfileScreenOutput.self) { (r: Resolver, view: ProfileScreenInput) in
            let presenter = ProfileScreenPresenter()
            presenter.view = view
            presenter.router = r.resolve(ProfileScreenRouterInput.self, argument: view as! ProfileScreenView)!
            presenter.interactor = r.resolve(ProfileScreenInteractorInput.self)!
            return presenter
            }.inObjectScope(.transient)
        
        //register interactor
        container.register(ProfileScreenInteractorInput.self) { r in
            let interactor = ProfileScreenInteractor()
            interactor.userRepository = r.resolve(UserRepositoryProtocol.self)!
            return interactor
        }.inObjectScope(.transient)
        
        //register router
        container.register(ProfileScreenRouterInput.self) { (r: Resolver, view: ProfileScreenView) in
            let router = ProfileScreenRouter()
            router.view = view
            return router
        }.inObjectScope(.transient)
        
    }
    
    
    
}
