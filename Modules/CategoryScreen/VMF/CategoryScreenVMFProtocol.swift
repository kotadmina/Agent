//
//  CategoryScreenVMFProtocol.swift
//  Agent
//
//  Created by Sher Locked on 09.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol CategoryScreenVMFProtocol {
    
    func createHeaderViewModel(with category: Category, missionCount: Int?) -> CategoryHeaderCellViewModel
    func createCellViewModels(with missions: [Mission]) -> [CategoryOpenCellViewModel]
}
