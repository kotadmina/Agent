//
//  CategoryScreenVMF.swift
//  Agent
//
//  Created by Sher Locked on 09.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class CategoryScreenVMF: CategoryScreenVMFProtocol {
    
    func createHeaderViewModel(with category: Category, missionCount: Int?) -> CategoryHeaderCellViewModel {
        let viewModel = CategoryHeaderCellViewModel(mainImageName: category.imageName, categoryNameText: category.name, missionCount: missionCount)
        return viewModel
    }
    
    func createCellViewModels(with missions: [Mission]) -> [CategoryOpenCellViewModel] {
        var viewModels: [CategoryOpenCellViewModel] = []
        for mission in missions {
            let viewModel = CategoryOpenCellViewModel(missionImageName: mission.headerImageName, missionDescription: mission.shortDescriptionText, missionId: mission.id)
            viewModels.append(viewModel)
        }
        return viewModels
    }
    
}
