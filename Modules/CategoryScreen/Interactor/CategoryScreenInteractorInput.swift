//
//  CategoryScreenInteractorInput.swift
//  Agent
//
//  Created by Sher Locked on 08.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol CategoryScreenInteractorInput {
    func getMissions(from category: Category) -> [Mission] 
}
