//
//  CategoryScreenInteractor.swift
//  Agent
//
//  Created by Sher Locked on 08.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class CategoryScreenInteractor: CategoryScreenInteractorInput {
    
    var missionsRepository: MissionsRepositoryProtocol!
    
    func getMissions(from category: Category) -> [Mission] {
        return missionsRepository.getMissions(by: category)
    }
    
}
