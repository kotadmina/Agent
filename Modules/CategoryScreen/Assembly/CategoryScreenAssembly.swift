//
//  CategoryScreenAssembly.swift
//  Agent
//
//  Created by Sher Locked on 08.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class CategoryScreenAssembly: BaseAssembly {
    
    static func configure() {
        let container = defaultContainer()
        
        //register view
        container.register(CategoryScreenViewInput.self) { r in
            let storyboard = UIStoryboard(name: "CategoryScreenView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CategoryScreenView") as! CategoryScreenView
           vc.output = r.resolve(CategoryScreenViewOutput.self, argument: (vc as CategoryScreenViewInput))!
            return vc
            }.inObjectScope(.transient)
        
        //register presenter
        container.register(CategoryScreenViewOutput.self) { (r: Resolver, view: CategoryScreenViewInput) in
            let presenter = CategoryScreenPresenter()
            presenter.view = view
            presenter.vmf = r.resolve(CategoryScreenVMFProtocol.self)!
            presenter.interactor = r.resolve(CategoryScreenInteractorInput.self)!
            presenter.router = r.resolve(CategoryScreenRouterInput.self, argument: view as! UIViewController)!
            return presenter
            }.inObjectScope(.transient)
        
        //register interactor
        container.register(CategoryScreenInteractorInput.self) { r in
            let interactor = CategoryScreenInteractor()
            interactor.missionsRepository = r.resolve(MissionsRepositoryProtocol.self)!
            return interactor
        }.inObjectScope(.transient)
        
        //register vmf
        container.register(CategoryScreenVMFProtocol.self) { _ in
            let vmf = CategoryScreenVMF()
            return vmf
            }.inObjectScope(.transient)
        
        //register router
        container.register(CategoryScreenRouterInput.self) { (r: Resolver, view: UIViewController) in
            let router = CategoryScreenRouter()
            router.view = view
            return router
            }.inObjectScope(.transient)
    }
    
    
}
