//
//  CategoryScreenRouter.swift
//  Agent
//
//  Created by Sher Locked on 08.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class CategoryScreenRouter: CategoryScreenRouterInput {
    weak var view: UIViewController!
    
    func back() {
        view.navigationController?.popViewController(animated: true)
    }
    
    func selectMission(with mission: Mission) {
        let vc = ModulesAssembly.resolve(type: MissionDetailViewInput.self) as! UIViewController
        ((vc as? MissionDetailView)?.output as? MissionDetailPresenter)?.mission = mission
        vc.hidesBottomBarWhenPushed = true
        view.navigationController?.pushViewController(vc, animated: true)
    }
}


