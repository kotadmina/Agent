//
//  CategoryScreenViewOutput.swift
//  Agent
//
//  Created by Sher Locked on 08.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol CategoryScreenViewOutput {
    
    func viewDidLoad()
    func backButtonPressed()
    func selectMission(with id: String)

}
