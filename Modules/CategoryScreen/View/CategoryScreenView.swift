//
//  CategoryScreenView.swift
//  Agent
//
//  Created by Sher Locked on 08.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class CategoryScreenView: UIViewController, CategoryScreenViewInput {

    var output: CategoryScreenViewOutput!
    var headerViewModel: CategoryHeaderCellViewModel?
    var cellViewModels: [CategoryOpenCellViewModel] = []
    @IBOutlet weak var categoryTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewDidLoad()
        configure()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if #available(iOS 11.0, *) {
            categoryTableView.contentInset.top = -view.safeAreaInsets.top
        } else {
            categoryTableView.contentInset.top = -topLayoutGuide.length
        }
    }
    
    func configure() {
        view.backgroundColor = ColorsLibrary.mainDarkViolet
        categoryTableView.backgroundColor = ColorsLibrary.mainDarkViolet
        categoryTableView.delegate = self
        categoryTableView.dataSource = self
        let headerNib = UINib(nibName: "OpenCategoryHeaderCell", bundle: nil)
        categoryTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "OpenCategoryHeaderCell")
        categoryTableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNormalMagnitude))
        let cellNib = UINib(nibName: "CategoryOpenCell", bundle: nil)
        categoryTableView.register(cellNib, forCellReuseIdentifier: "CategoryOpenCell")
        categoryTableView.separatorStyle = .none
        categoryTableView.rowHeight = UITableViewAutomaticDimension
        categoryTableView.estimatedRowHeight = 270
    }
    
    func updateHeaderTable(with viewModel: CategoryHeaderCellViewModel) {
        headerViewModel = viewModel
        categoryTableView.reloadData()
    }
    
    func updateTableView(with viewModels: [CategoryOpenCellViewModel], headerVM: CategoryHeaderCellViewModel) {
        cellViewModels = viewModels
        categoryTableView.reloadData()
        headerViewModel = headerVM
        updateHeaderTable(with: headerVM)
    }


}

extension CategoryScreenView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 270
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = cellViewModels[indexPath.row] as? CategoryOpenCellViewModel else {
            return
        }
        
        output.selectMission(with: viewModel.missionId)
    }
    
}

extension CategoryScreenView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = categoryTableView.dequeueReusableCell(withIdentifier: "CategoryOpenCell") as! CategoryOpenCell
        cell.configure(with: cellViewModels[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = categoryTableView.dequeueReusableHeaderFooterView(withIdentifier: "OpenCategoryHeaderCell") as? OpenCategoryHeaderCell
        headerCell?.delegate = self
        if let viewModel = headerViewModel {
            headerCell?.configure(with: viewModel)
        }
        return headerCell
    }
}

extension CategoryScreenView: OpenCategoryHeaderDelegate {
    func buttonPressed() {
        output.backButtonPressed()
    }
    
    
}
