//
//  CategoryScreenPresenter.swift
//  Agent
//
//  Created by Sher Locked on 08.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class CategoryScreenPresenter: CategoryScreenViewOutput {
    
    weak var view: CategoryScreenViewInput!
    var category: Category?
    var vmf: CategoryScreenVMFProtocol!
    var interactor: CategoryScreenInteractorInput!
    var router: CategoryScreenRouterInput!
    var missions: [Mission] = []
    
    func viewDidLoad() {
        guard let category = category else {
            return
        }
        
        let headerVM = vmf.createHeaderViewModel(with: category, missionCount: nil)
        view.updateHeaderTable(with: headerVM)
        
        missions = interactor.getMissions(from: category)
        
        let cellVM = vmf.createCellViewModels(with: missions)
        let headerVMWithCount = vmf.createHeaderViewModel(with: category, missionCount: missions.count)
        view.updateTableView(with: cellVM, headerVM: headerVMWithCount)
    }
    
    func backButtonPressed() {
        router.back()
    }
    
    func selectMission(with id: String) {
        guard let mission = missions.first(where: {$0.id == id}) else {
            return
        }
        router.selectMission(with: mission)
    }
    
    
}
