//
//  TermsScreenAssembly.swift
//  Agent
//
//  Created by Sher Locked on 14.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//
import UIKit
import Swinject

class TermsScreenAssembly: BaseAssembly {
    
    static func configure() {
        let container = defaultContainer()
        
        //register view
        container.register(TermsScreenViewInput.self) { r in
            let storyboard = UIStoryboard(name: "TermsScreenView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TermsScreenView") as! TermsScreenView
            vc.output = r.resolve(TermsScreenViewOutput.self, argument: (vc as TermsScreenViewInput))!
            return vc
            }.inObjectScope(.transient)
        
        //register presenter
        container.register(TermsScreenViewOutput.self) { (r: Resolver, view: TermsScreenViewInput) in
            let presenter = TermsScreenPresenter()
            presenter.view = view
            //presenter.vmf = r.resolve(SettingsScreenVMFProtocol.self)!
            //presenter.interactor = r.resolve(SettingsScreenInteractorInput.self)!
            presenter.router = r.resolve(TermsScreenRouterInput.self, argument: view as! UIViewController)!
            return presenter
            }.inObjectScope(.transient)
        
        //register interactor
//        container.register(SettingsScreenInteractorInput.self) { r in
//            let interactor = SettingsScreenInteractor()
//            interactor.userRepository = r.resolve(UserRepositoryProtocol.self)!
//            return interactor
//            }.inObjectScope(.transient)
//
        //register vmf
//        container.register(SettingsScreenVMFProtocol.self) { _ in
//            let vmf = SettingsScreenVMF()
//            return vmf
//            }.inObjectScope(.transient)
        
        //register router
        container.register(TermsScreenRouterInput.self) { (r: Resolver, view: UIViewController) in
            let router = TermsScreenRouter()
            router.view = view
            return router
            }.inObjectScope(.transient)
    }
    
    
}
