//
//  TermsScreenRouter.swift
//  Agent
//
//  Created by Sher Locked on 14.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class TermsScreenRouter: TermsScreenRouterInput {
    
    weak var view: UIViewController!
    
    func back() {
        view.navigationController?.popViewController(animated: true)
    }
}
