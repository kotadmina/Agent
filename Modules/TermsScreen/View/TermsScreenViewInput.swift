//
//  TermsScreenViewInput.swift
//  Agent
//
//  Created by Sher Locked on 14.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol TermsScreenViewInput: class {
    func updateFillInfo(with term: Terms)
}
