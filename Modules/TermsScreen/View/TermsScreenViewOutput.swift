//
//  TermsScreenViewOutput.swift
//  Agent
//
//  Created by Sher Locked on 14.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol TermsScreenViewOutput {
    func viewDidLoad()
    func backButtonTapped()
}
