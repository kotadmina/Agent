//
//  TermsScreenView.swift
//  Agent
//
//  Created by Sher Locked on 14.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class TermsScreenView: UIViewController, TermsScreenViewInput {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var termsTextView: UITextView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var footerImageView: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    
    var fillInfo: Terms = Terms(titleText: "", text: "")
    
    var output: TermsScreenViewOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        output.viewDidLoad()
    }
    
    func configure() {
        view.backgroundColor = ColorsLibrary.mainDarkViolet
        titleLabel.textColor = UIColor.white
        titleLabel.font = FontLibrary.pragmaticaCondRegular(size: 24)
        separatorView.backgroundColor = ColorsLibrary.mainYellow
        termsTextView.textColor = UIColor.white
        termsTextView.font = FontLibrary.pragmaticaExtraLightReg(size: 16)
        termsTextView.backgroundColor = UIColor.clear
        termsTextView.textAlignment = .left
        footerView.backgroundColor = UIColor.clear
        footerImageView.image = UIImage(named: "footer_image")
        let backImage = UIImage(named: "back_button")?.withRenderingMode(.alwaysOriginal)
        backButton.setImage(backImage, for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 25)
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
    }
    
    func updateFillInfo(with term: Terms) {
        titleLabel.text = term.titleText
        termsTextView.text = term.text
    }
    
    @objc func backButtonTapped() {
        output.backButtonTapped()
    }
    
}
