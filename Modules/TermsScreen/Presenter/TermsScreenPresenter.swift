//
//  TermsScreenPresenter.swift
//  Agent
//
//  Created by Sher Locked on 14.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class TermsScreenPresenter: TermsScreenViewOutput {
    
    weak var view: TermsScreenViewInput!
    var router: TermsScreenRouterInput!
    var terms: Terms?
    
    func viewDidLoad() {
        guard let terms = terms else {
            return
        }
        view.updateFillInfo(with: terms)
    }
    
    func backButtonTapped() {
        router.back()
    }
}
