//
//  TaskScreenVMFProtocol.swift
//  Agent
//
//  Created by Sher Locked on 05.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol TaskScreenVMFProtocol {
    func createCategoryViewModels(with categories: [Category], selectedId: String?) -> [CategoriesIconViewModel]
    func createSections(with user: User, rewards: [Int: [Reward]], categories: [Category], selectedCategory: Category?) -> [TaskScreenSection]
}
