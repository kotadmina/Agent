//
//  TaskScreenVMF.swift
//  Agent
//
//  Created by Sher Locked on 05.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct TaskScreenSection {
    var name: String
    var viewModels: [TaskRewardTableCellViewModel]
    var isLocked: Bool
}

class TaskScreenVMF: TaskScreenVMFProtocol {
    
    func createCategoryViewModels(with categories: [Category], selectedId: String?) -> [CategoriesIconViewModel] {
        var viewModels: [CategoriesIconViewModel] = [CategoriesIconViewModel(id: nil,
                                                                             iconName: "all_categories_icon",
                                                                             isSelected: selectedId == nil)]
        let categoriesVM = categories.map { category -> CategoriesIconViewModel in
            let isSelected = category.id == selectedId
            return CategoriesIconViewModel(id: category.id,
                                           iconName: category.iconName,
                                           isSelected: isSelected)
        }
        viewModels.append(contentsOf: categoriesVM)
        return viewModels
    }
    
    func createSections(with user: User, rewards: [Int: [Reward]], categories: [Category], selectedCategory: Category?) -> [TaskScreenSection] {
        var sections: [TaskScreenSection] = []
        for level in 2...8 {
            let levelHeaderText = "УРОВЕНЬ \(level)"
            let isLevelLocked = level > user.levelCount
            let rewardsForLevel = rewards[level] ?? []
            var viewModels: [TaskRewardTableCellViewModel] = []
            for reward in rewardsForLevel {
                let categoryForReward = categories.first(where: { reward.rewardCategoryId == $0.id })
                if selectedCategory?.id == categoryForReward?.id || selectedCategory == nil {
                    let viewModel = TaskRewardTableCellViewModel(rewardName: reward.description,
                                                                 categoryImageName: categoryForReward?.iconName,
                                                                 cost: reward.rewardCost)
                    viewModels.append(viewModel)
                }
            }
            let section = TaskScreenSection(name: levelHeaderText,
                                            viewModels: viewModels,
                                            isLocked: isLevelLocked)
            sections.append(section)
        }
        return sections
    }
    
}
