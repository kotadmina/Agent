//
//  TaskScreenInteractor.swift
//  Agent
//
//  Created by Sher Locked on 05.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class TaskScreenInteractor: TaskScreenInteractorInput {
    
    var categoriesRepository: CategoriesRepositoryProtocol!
    var userRepository: UserRepositoryProtocol!
    
    func getUserInfo() -> User {
        return userRepository.getUser()
    }
    
    func getCategories() -> [Category] {
        return categoriesRepository.getCategories()
    }
    
    func getRewards() -> [Int: [Reward]] {
        // get reward dictionary for each level
        let secondLevelRewards = [Reward(description: "Скидка 10% в сети магазинов «Перекресток», «Пятерочка» и «Магнит»",
                                         expireDate: nil,
                                         rewardImageName: nil,
                                         rewardCategoryId: "4"),
                                  Reward(description: "Деньги на карту",
                                         expireDate: nil,
                                         rewardImageName: nil,
                                         rewardCost: "+50₽")]
        
        let thirdLevelRewards = [Reward(description: "Штрудель с корицей в подарок от сети кофеен «Шоколадница»",
                                         expireDate: nil,
                                         rewardImageName: nil,
                                         rewardCategoryId: "5"),
                                 Reward(description: "Деньги на карту",
                                        expireDate: nil,
                                        rewardImageName: nil,
                                        rewardCost: "+70₽")]
        
        let forthLevelRewards = [Reward(description: "+30 дополнительных минут прыжков в батутном центре «Небо»",
                                         expireDate: nil,
                                         rewardImageName: nil,
                                         rewardCategoryId: "7"),
                                 Reward(description: "Деньги на карту",
                                        expireDate: nil,
                                        rewardImageName: nil,
                                        rewardCost: "+100₽")]
        
        let fifthLevelRewards = [Reward(description: "Скидка 30% на билеты в кино в любом из кинотеатров «КАРО»",
                                         expireDate: nil,
                                         rewardImageName: nil,
                                         rewardCategoryId: "6"),
                                 Reward(description: "Деньги на карту",
                                        expireDate: nil,
                                        rewardImageName: nil,
                                        rewardCost: "+120₽")]
        return [2: secondLevelRewards,
                3: thirdLevelRewards,
                4: forthLevelRewards,
                5: fifthLevelRewards]
    }
}
