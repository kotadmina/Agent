//
//  TaskScreenPresenter.swift
//  Agent
//
//  Created by Sher Locked on 05.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class TaskScreenPresenter: TaskScreenViewOutput {
    
    weak var view: TaskScreenViewInput!
    var interactor: TaskScreenInteractorInput!
    var vmf: TaskScreenVMFProtocol!
    var categories: [Category] = []
    var selectedCategoryId: String?
    
    func viewDidLoad() {
        let user = interactor.getUserInfo()
        view.configureHeaderInfo(imageName: user.avatarImageName, fullName: user.name.uppercased() + " " + user.surname.uppercased(), level: user.levelCount, experience: user.experienceCount)
        categories = interactor.getCategories()
        let categoriesVM = vmf.createCategoryViewModels(with: categories, selectedId: nil)
        view.updateCollectionView(with: categoriesVM)
        updateLevels()
    }
    
    func updateLevels() {
        let user = interactor.getUserInfo()
        let rewards = interactor.getRewards()
        let selectedCategory = categories.first(where: { $0.id == self.selectedCategoryId })
        let sections = vmf.createSections(with: user, rewards: rewards, categories: categories, selectedCategory: selectedCategory)
        view.updateTable(with: sections)
    }
    
    func selectCategory(with id: String?) {
        guard id != selectedCategoryId else {
            return
        }
        selectedCategoryId = id
        let viewModels = vmf.createCategoryViewModels(with: categories, selectedId: id)
        view.updateCollectionView(with: viewModels)
        updateLevels()
    }
}
