//
//  TaskScreenViewOutput.swift
//  Agent
//
//  Created by Sher Locked on 01.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol TaskScreenViewOutput {
    func viewDidLoad()
    func selectCategory(with id: String?)
}
