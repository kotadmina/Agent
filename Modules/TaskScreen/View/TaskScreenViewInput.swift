//
//  TaskScreenViewInput.swift
//  Agent
//
//  Created by Sher Locked on 01.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol TaskScreenViewInput: class {
    func configureHeaderInfo(imageName: String, fullName: String, level: Int, experience: Int)
    func updateCollectionView(with viewModels: [CategoriesIconViewModel])
    func updateTable(with viewModels: [TaskScreenSection])
}
