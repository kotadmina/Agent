//
//  TaskScreenView.swift
//  Agent
//
//  Created by Sher Locked on 01.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class TaskScreenView: UIViewController, TaskScreenViewInput {
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var experienceLabel: UILabel!
    @IBOutlet weak var firstSeparatorView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var secondSeparatorView: UIView!
    @IBOutlet weak var levelTableView: UITableView!
    var cellViewModels: [CategoriesIconViewModel] = []
    var tableViewSections: [TaskScreenSection] = []
    
    var output: TaskScreenViewOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewDidLoad()
        configure()
    }
    
    func configure() {
        navigationController?.navigationBar.isHidden = true
        view.backgroundColor = ColorsLibrary.mainDarkViolet
        avatarImageView.layer.cornerRadius = 8
        avatarImageView.clipsToBounds = true
        nameLabel.textColor = UIColor.white
        nameLabel.font = FontLibrary.pragmaticaCondRegular(size: 24)
        levelLabel.textColor = UIColor.white
        levelLabel.font = FontLibrary.pragmaticaCondRegular(size: 20)
        experienceLabel.textColor = ColorsLibrary.mainYellow
        experienceLabel.font = FontLibrary.pragmaticaRegular(size: 24)
        firstSeparatorView.backgroundColor = ColorsLibrary.mainYellow
        secondSeparatorView.backgroundColor = ColorsLibrary.mainYellow
        infoLabel.text = "Достигай новых уровней и получай награды!"
        infoLabel.textColor = ColorsLibrary.mainYellow
        infoLabel.font = FontLibrary.pragmaticaCondRegular(size: 24)
        categoriesCollectionView.backgroundColor = ColorsLibrary.mainDarkViolet
        categoriesCollectionView.delegate = self
        categoriesCollectionView.dataSource = self
        let nib = UINib(nibName: "CategoriesCollectionViewCell", bundle: nil)
        categoriesCollectionView.register(nib, forCellWithReuseIdentifier: "CategoriesCollectionViewCell")
        let tableNib = UINib(nibName: "TaskRewardTableViewCell", bundle: nil)
        levelTableView.register(tableNib, forCellReuseIdentifier: "TaskRewardTableViewCell")
        levelTableView.delegate = self
        levelTableView.dataSource = self
        levelTableView.rowHeight = UITableViewAutomaticDimension
        levelTableView.estimatedRowHeight = 50
        levelTableView.backgroundColor = .clear
        levelTableView.separatorStyle = .none
        levelTableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: .leastNormalMagnitude))
        levelTableView.tableFooterView = createFooterView()
    }
    
    func configureHeaderInfo(imageName: String, fullName: String, level: Int, experience: Int) {
        avatarImageView.image = UIImage(named: imageName)
        nameLabel.text = fullName
        levelLabel.text = "Уровень \(level)"
        experienceLabel.text = "\(experience)"
    }
    
    func updateCollectionView(with viewModels: [CategoriesIconViewModel]) {
        cellViewModels = viewModels
        categoriesCollectionView.reloadData()
    }
    
    func updateTable(with sections: [TaskScreenSection]) {
        tableViewSections = sections
        levelTableView.reloadData()
    }
    
    private func createTableHeader(with section: TaskScreenSection) -> UIView {
        let view = UIView(frame: .zero)
        let label = UILabel()
        label.font = FontLibrary.pragmaticaCondLight(size: 24)
        label.textColor = UIColor.white
        label.text = section.name.uppercased()
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 28),
                                     label.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                                     label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 28)])
        return view
    }
    
    private func createLockedTableHeader(with section: TaskScreenSection) -> UIView {
        let view = UIView(frame: .zero)
        let roundedView = UIView(frame: .zero)
        roundedView.backgroundColor = ColorsLibrary.rewardLockedGrey
        roundedView.layer.cornerRadius = 8
        roundedView.layer.borderWidth = 1
        roundedView.layer.borderColor = ColorsLibrary.rewardLockedBorderGrey.cgColor
        view.addSubview(roundedView)
        let label = UILabel()
        label.font = FontLibrary.pragmaticaCondLight(size: 24)
        label.textColor = UIColor.white
        label.text = section.name.uppercased()
        roundedView.addSubview(label)
        let imageView = UIImageView(image: UIImage(named: "icon_lock"))
        imageView.contentMode = .scaleAspectFit
        roundedView.addSubview(imageView)
        label.translatesAutoresizingMaskIntoConstraints = false
        roundedView.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([label.leadingAnchor.constraint(equalTo: roundedView.leadingAnchor, constant: 12),
                                     label.centerYAnchor.constraint(equalTo: roundedView.centerYAnchor),
                                     imageView.trailingAnchor.constraint(equalTo: roundedView.trailingAnchor, constant: -12),
                                     imageView.centerYAnchor.constraint(equalTo: roundedView.centerYAnchor),
                                     imageView.heightAnchor.constraint(equalToConstant: 25),
                                     imageView.widthAnchor.constraint(equalToConstant: 25),
                                     roundedView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
                                     roundedView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -19),
                                     roundedView.topAnchor.constraint(equalTo: view.topAnchor, constant: 8),
                                     roundedView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -8)])
        return view
    }
    
    private func createFooterView() -> UIView {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 80))
        view.backgroundColor = UIColor.clear
        let footerImageView = UIImageView(image: UIImage(named: "footer_image"))
        footerImageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(footerImageView)
        NSLayoutConstraint.activate([footerImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                                     footerImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                     footerImageView.widthAnchor.constraint(equalToConstant: 149),
                                     footerImageView.heightAnchor.constraint(equalToConstant: 49)])
        return view
    }
    

}

extension TaskScreenView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let section = tableViewSections[section]
        if section.isLocked {
            return 63
        } else {
            return 47
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

extension TaskScreenView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewSections[section].viewModels.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewSections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = tableViewSections[indexPath.section].viewModels[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskRewardTableViewCell") as! TaskRewardTableViewCell
        cell.configure(with: viewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let section = tableViewSections[section]
        if section.isLocked {
            return createLockedTableHeader(with: section)
        } else {
            return createTableHeader(with: section)
        }
    }
    
}

extension TaskScreenView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewModel = cellViewModels[indexPath.row]
        output.selectCategory(with: viewModel.id)
    }
}

extension TaskScreenView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellViewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = categoriesCollectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCollectionViewCell", for: indexPath) as! CategoriesCollectionViewCell
        cell.configure(with: cellViewModels[indexPath.row])
        return cell
    }
    
    
}

extension TaskScreenView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewHeight = categoriesCollectionView.frame.size.height
        
        return CGSize(width: 50, height: collectionViewHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
