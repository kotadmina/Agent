//
//  TaskScreenAssembly.swift
//  Agent
//
//  Created by Sher Locked on 01.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class TaskScreenAssembly: BaseAssembly {
    
    static func configure() {
        let container = defaultContainer()
        
        //RegisterView
        container.register(TaskScreenViewInput.self) { r in
            let storyboard = UIStoryboard(name: "TaskScreenView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TaskScreenView") as! TaskScreenView
            vc.output = r.resolve(TaskScreenViewOutput.self, argument: (vc as TaskScreenViewInput))!
            return vc
            }.inObjectScope(.transient)
        
        //register presenter
        container.register(TaskScreenViewOutput.self) { (r: Resolver, view: TaskScreenViewInput) in
            let presenter = TaskScreenPresenter()
            presenter.view = view
            presenter.vmf = r.resolve(TaskScreenVMFProtocol.self)!
            presenter.interactor = r.resolve(TaskScreenInteractorInput.self)!
            //presenter.router = r.resolve(TaskScreenRouterInput.self, argument: view as! UIViewController)!
            return presenter
            }.inObjectScope(.transient)
        
        //register interactor
        container.register(TaskScreenInteractorInput.self) { r in
            let interactor = TaskScreenInteractor()
            interactor.categoriesRepository = r.resolve(CategoriesRepositoryProtocol.self)!
            interactor.userRepository = r.resolve(UserRepositoryProtocol.self)!
            return interactor
            }.inObjectScope(.transient)
        
        //register vmf
        container.register(TaskScreenVMFProtocol.self) { _ in
            let vmf = TaskScreenVMF()
            return vmf
            }.inObjectScope(.transient)
        
        //register router
        container.register(TaskScreenRouterInput.self) { (r: Resolver, view: UIViewController) in
            let router = TaskScreenRouter()
            //router.view = view
            return router
            }.inObjectScope(.transient)
      
        
    }

}


