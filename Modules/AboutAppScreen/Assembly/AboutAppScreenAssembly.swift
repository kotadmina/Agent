//
//  AboutAppScreenAssembly.swift
//  Agent
//
//  Created by Sher Locked on 14.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class AboutAppScreenAssembly: BaseAssembly {
    
    static func configure() {
        let container = defaultContainer()
        
        //register view
        container.register(AboutAppScreenViewInput.self) { r in
            let storyboard = UIStoryboard(name: "AboutAppScreenView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AboutAppScreenView") as! AboutAppScreenView
            //vc.output = r.resolve(TermsScreenViewOutput.self, argument: (vc as TermsScreenViewInput))!
            return vc
            }.inObjectScope(.transient)
        
        //register presenter
        container.register(AboutAppScreenViewOutput.self) { (r: Resolver, view: AboutAppScreenViewInput) in
            let presenter = AboutAppScreenPresenter()
            //presenter.view = view
            //presenter.vmf = r.resolve(SettingsScreenVMFProtocol.self)!
            //presenter.interactor = r.resolve(SettingsScreenInteractorInput.self)!
            //presenter.router = r.resolve(TermsScreenRouterInput.self, argument: view as! UIViewController)!
            return presenter
            }.inObjectScope(.transient)
        
        //register interactor
        //        container.register(SettingsScreenInteractorInput.self) { r in
        //            let interactor = SettingsScreenInteractor()
        //            interactor.userRepository = r.resolve(UserRepositoryProtocol.self)!
        //            return interactor
        //            }.inObjectScope(.transient)
        //
        //register vmf
        //        container.register(SettingsScreenVMFProtocol.self) { _ in
        //            let vmf = SettingsScreenVMF()
        //            return vmf
        //            }.inObjectScope(.transient)
        
        //register router
        container.register(AboutAppScreenRouterInput.self) { (r: Resolver, view: UIViewController) in
            let router = AboutAppScreenRouter()
            //router.view = view
            return router
            }.inObjectScope(.transient)
    }
    
    
}
