//
//  FormScreenRouter.swift
//  Agent
//
//  Created by Sher Locked on 02.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class FormScreenRouter: FormScreenRouterInput {
    weak var view: UIViewController!
    
    func back() {
        view.navigationController?.popViewController(animated: true)
    }
}
