//
//  FormScreenVMFProtocol.swift
//  Agent
//
//  Created by Sher Locked on 02.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol FormScreenVMFProtocol {
    func createFormViewModels(with categories: [Category], selectedCategories: [String], user: User?) -> [FormSection]
}
