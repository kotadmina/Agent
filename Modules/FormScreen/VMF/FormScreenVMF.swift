//
//  FormScreenVMF.swift
//  Agent
//
//  Created by Sher Locked on 02.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation
import YandexMapKit

enum FormScreenViewModelId: String {
    case instagram
    case vk
    case fb
    case odnoklassniki
    case age
    case gender
    case city
    case status
    case study
    case work
    case interests
    case map
    case hobby
    case homeLocation
    case workLocation
}

class FormScreenVMF: FormScreenVMFProtocol {
    
    func createFormViewModels(with categories: [Category], selectedCategories: [String], user: User?) -> [FormSection] {
        let instVM = FormTextFieldViewModel(name: "Instagram",
                                            value: user?.userInfo.instagramLink,
                                            id: FormScreenViewModelId.instagram.rawValue)
        let vkVM = FormTextFieldViewModel(name: "Вконтакте",
                                          value: user?.userInfo.vkLink,
                                          id: FormScreenViewModelId.vk.rawValue)
        let fbVM = FormTextFieldViewModel(name: "Facebook",
                                          value: user?.userInfo.facebookLink,
                                          id: FormScreenViewModelId.fb.rawValue)
        let odnVM = FormTextFieldViewModel(name: "Одноклассники",
                                           value: user?.userInfo.okLink,
                                           id: FormScreenViewModelId.odnoklassniki.rawValue)
        
        let socialSection = FormSection(title: "Социальные сети:", viewModels: [instVM, vkVM, fbVM, odnVM])
        
        let birthdayFormatter = DateFormatter()
        birthdayFormatter.dateFormat = "dd.MM.yyyy"
        var birthdayString = ""
        if let birthday = user?.userInfo.birthday {
            birthdayString = birthdayFormatter.string(from: birthday)
        }
        let ageVM = FormAgeViewModel(name: "Возраст", value: birthdayString, id: FormScreenViewModelId.age.rawValue)
        let genderVM = FormGenderViewModel(name: "Пол",
                                           id: FormScreenViewModelId.gender.rawValue,
                                           gender: user?.userInfo.gender ?? .notDetermined)
        let cityVM = FormInfoWithArrowViewModel(name: "Место рождения", value: "Москва", id: FormScreenViewModelId.city.rawValue)
        let maritalStatusVM = FormInfoWithArrowViewModel(name: "Семейное положение", value: "Не замужем", id: FormScreenViewModelId.status.rawValue)
        let studyVM = FormInfoWithArrowViewModel(name: "Образование", value: "Бакалавр", id: FormScreenViewModelId.study.rawValue)
        let workVM = FormInfoWithArrowViewModel(name: "Сфера деятельности", value: "IT", id: FormScreenViewModelId.work.rawValue)
        
        let personalInfoSection = FormSection(title: "Личная информация:", viewModels: [ageVM, genderVM, cityVM, maritalStatusVM, studyVM, workVM])
        
        let interests = categories.map { category -> InterestCollectionCellViewModel in
            let isSelected = selectedCategories.contains(category.id)
            return InterestCollectionCellViewModel(id: category.id,
                                                   imageName: category.iconName,
                                                   intetestName: category.name,
                                                   isSelected: isSelected)
        }
        let collectionCellVM = InterestsCellViewModel(id: FormScreenViewModelId.interests.rawValue, collectionViewModels: interests)
        let hobbyCellVM = FormTextFieldViewModel(name: "Хобби", value: "", id: FormScreenViewModelId.hobby.rawValue)
        let interestSection = FormSection(title: "Интересы:", viewModels: [collectionCellVM, hobbyCellVM])
        
        let mockHomeLocation = YMKPoint(latitude: 30, longitude: 60)
        let mockWorkLocation = YMKPoint(latitude: 40, longitude: 65)
        let mapVM = MapViewViewModel(id: FormScreenViewModelId.map.rawValue, homeLocation: mockHomeLocation, workLocation: mockWorkLocation)
        let homeLocationVM = FormInfoWithArrowViewModel(name: "Домашний адрес:", value: "ул.Большая Гр…", id: FormScreenViewModelId.homeLocation.rawValue)
        let workLocationVM = FormInfoWithArrowViewModel(name: "Рабочий адрес:", value: "ул.Земляной вал…", id: FormScreenViewModelId.workLocation.rawValue)
        let placesSection = FormSection(title: "Мои места:", viewModels: [mapVM, homeLocationVM, workLocationVM])
        
        return [socialSection, personalInfoSection, interestSection, placesSection]
    }
    
}
