//
//  FormSection.swift
//  Agent
//
//  Created by Sher Locked on 03.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct FormSection {
    var title: String
    var viewModels: [ProfileScreenCellViewModel]
}
