//
//  FormScreenView.swift
//  Agent
//
//  Created by Sher Locked on 02.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class FormScreenView: UIViewController, FormScreenViewInput {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var formTableView: TableViewWithMap!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var nameSeparatorView: UIView!
    @IBOutlet weak var emailSeparatorView: UIView!
    @IBOutlet weak var yellowView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    var sections: [FormSection] = []
    var output: FormScreenViewOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        output.viewDidLoad()
    }
    
    func configure() {
        view.backgroundColor = ColorsLibrary.mainDarkViolet
        formTableView.delegate = self
        formTableView.dataSource = self
        formTableView.backgroundColor = ColorsLibrary.mainDarkViolet
        let nib1 = UINib(nibName: "FormTextFieldCell", bundle: nil)
        formTableView.register(nib1, forCellReuseIdentifier: "FormTextFieldCell")
        let nib2 = UINib(nibName: "FormAgeCell", bundle: nil)
        formTableView.register(nib2, forCellReuseIdentifier: "FormAgeCell")
        let nib3 = UINib(nibName: "FormGenderCell", bundle: nil)
        formTableView.register(nib3, forCellReuseIdentifier: "FormGenderCell")
        let nib4 = UINib(nibName: "FormInfoWithArrowCell", bundle: nil)
        formTableView.register(nib4, forCellReuseIdentifier: "FormInfoWithArrowCell")
        let nib5 = UINib(nibName: "InterestsCell", bundle: nil)
        formTableView.register(nib5, forCellReuseIdentifier: "InterestsCell")
        let nib6 = UINib(nibName: "MapViewCell", bundle: nil)
        formTableView.register(nib6, forCellReuseIdentifier: "MapViewCell")
        let backImage = UIImage(named: "back_button")?.withRenderingMode(.alwaysOriginal)
        backButton.setImage(backImage, for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 25)
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        photoImageView.layer.cornerRadius = 8
        photoImageView.clipsToBounds = true
        nameLabel.text = "Имя"
        nameLabel.textColor = UIColor.white
        nameLabel.font = FontLibrary.pragmaticaCondLight(size: 24)
        nameSeparatorView.backgroundColor = ColorsLibrary.mainYellow
        emailLabel.text = "Эл. адрес"
        emailLabel.textColor = UIColor.white
        emailLabel.font = FontLibrary.pragmaticaCondLight(size: 24)
        emailSeparatorView.backgroundColor = ColorsLibrary.mainYellow
        yellowView.backgroundColor = ColorsLibrary.mainYellow
        nameTextField.textColor = UIColor.white
        nameTextField.font = FontLibrary.pragmaticaCondLight(size: 20)
        emailTextField.textColor = UIColor.white
        emailTextField.font = FontLibrary.pragmaticaCondLight(size: 20)
        formTableView.tableFooterView = createFooterView()
    }
    
    func updateTable(with sectionsVM: [FormSection]) {
        sections = sectionsVM
        formTableView.reloadData()
    }
    
    func updateUserHeader(username: String?, email: String?, imageName: String?) {
        nameTextField.text = username
        emailTextField.text = email
        photoImageView.image = UIImage(named: imageName ?? "no_image_icon")
    }
    
    @objc func backButtonPressed() {
        output.backButtonTaped()
    }
    
    private func createFooterView() -> UIView {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 80))
        view.backgroundColor = UIColor.clear
        let footerImageView = UIImageView(image: UIImage(named: "footer_image"))
        footerImageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(footerImageView)
        NSLayoutConstraint.activate([footerImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                                     footerImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                     footerImageView.widthAnchor.constraint(equalToConstant: 149),
                                     footerImageView.heightAnchor.constraint(equalToConstant: 49)])
        return view
    }

}

extension FormScreenView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 37
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let viewModel = sections[indexPath.section].viewModels[indexPath.row]
        if let collectionCellViewModel = viewModel as? InterestsCellViewModel {
            var lineCount = 0
            if collectionCellViewModel.collectionViewModels.count > 0 {
                lineCount = (collectionCellViewModel.collectionViewModels.count - 1) / 4 + 1
            }
            let verticalOffset: CGFloat = 25
            let horizontalPadding: CGFloat = 19
            let verticalPadding: CGFloat = 16
            let collectionViewWidth: CGFloat = view.frame.width - 32
            let itemWidth = (collectionViewWidth - 3 * horizontalPadding) / 4
            return verticalOffset + CGFloat(lineCount) * itemWidth + max(0, CGFloat(lineCount - 1)) * verticalPadding
        }
        return viewModel.height
    }
    
}

extension FormScreenView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: .zero)
        let label = UILabel()
        label.font = FontLibrary.pragmaticaCondRegular(size: 24)
        label.textColor = UIColor.white
        label.text = sections[section].title
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 14),
                                     label.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                                     label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 14)])
        return view
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = sections[indexPath.section].viewModels[indexPath.row]
        switch viewModel.type {
        case .textFieldCell:
            let cell = formTableView.dequeueReusableCell(withIdentifier: "FormTextFieldCell") as! FormTextFieldCell
            cell.delegate = self
            cell.configure(with: viewModel)
            return cell
        case .formAgeCell:
            let cell = formTableView.dequeueReusableCell(withIdentifier: "FormAgeCell") as! FormAgeCell
            cell.configure(with: viewModel)
            return cell
        case .formGenderCell:
            let cell = formTableView.dequeueReusableCell(withIdentifier: "FormGenderCell") as! FormGenderCell
            cell.configure(with: viewModel)
            return cell
        case .formInfoCell:
            let cell = formTableView.dequeueReusableCell(withIdentifier: "FormInfoWithArrowCell") as! FormInfoWithArrowCell
            cell.configure(with: viewModel)
            return cell
        case .collectionViewCell:
            let cell = formTableView.dequeueReusableCell(withIdentifier: "InterestsCell") as! InterestsCell
            cell.delegate = self
            cell.configure(with: viewModel)
            return cell
        case .mapCell:
            let cell = formTableView.dequeueReusableCell(withIdentifier: "MapViewCell") as! MapViewCell
            cell.configure(with: viewModel)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}

extension FormScreenView: FormTextFieldCellDelegate {
    func textFieldTapped(id: String) {
        var indexPath: IndexPath?
        for (i, section) in sections.enumerated() {
            guard let row = section.viewModels.index(where: { $0.id == id }) else {
                continue
            }
            indexPath = IndexPath(row: row, section: i)
            break
        }
        guard let indexPathUnwrapped = indexPath else {
            return
        }
        formTableView.scrollToRow(at: indexPathUnwrapped, at: .top, animated: true)
    }
}

extension FormScreenView: InterestsCellDelegate {
    func didPressCategory(with id: String) {
        output.didPressCategory(with: id)
    }
}
