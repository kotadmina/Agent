//
//  FormScreenViewInput.swift
//  Agent
//
//  Created by Sher Locked on 02.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol FormScreenViewInput: class {
    func updateTable(with sectionsVM: [FormSection])
    func updateUserHeader(username: String?, email: String?, imageName: String?)
}
