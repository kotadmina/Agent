//
//  TableViewWithMap.swift
//  Agent
//
//  Created by sisupov on 19.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class TableViewWithMap: UITableView {
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        isScrollEnabled = true
        for subview in subviews {
            let convertedPoint = convert(point, to: subview)
            if subview.point(inside: convertedPoint, with: event) && subview is MapViewCell {
                isScrollEnabled = false
            }
        }
        return super.point(inside: point, with: event)
    }
}
