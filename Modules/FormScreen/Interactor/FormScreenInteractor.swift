//
//  FormScreenInteractor.swift
//  Agent
//
//  Created by Sher Locked on 02.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class FormScreenInteractor: FormScreenInteractorInput {
    
    var categoriesRepository: CategoriesRepositoryProtocol!
    var userRepository: UserRepositoryProtocol!
    
    func getCategories() -> [Category] {
        return categoriesRepository.getCategories()
    }
    
    func getUserInfo() -> User {
        return userRepository.getUser()
    }
    
}
