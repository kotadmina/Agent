//
//  FormScreenPresenter.swift
//  Agent
//
//  Created by Sher Locked on 02.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class FormScreenPresenter: FormScreenViewOutput {
    
    weak var view: FormScreenViewInput!
    var vmf: FormScreenVMFProtocol!
    var router: FormScreenRouterInput!
    var interactor: FormScreenInteractorInput!
    var categories: [Category] = []
    var user: User?
    var selectedCategoryIds: [String] = []
    
    func viewDidLoad() {
        categories = interactor.getCategories()
        user = interactor.getUserInfo()
        updateView()
    }
    
    func backButtonTaped() {
        router.back()
    }
    
    func updateView() {
        let sections = vmf.createFormViewModels(with: categories, selectedCategories: selectedCategoryIds, user: user)
        view.updateTable(with: sections)
        var username: String = ""
        if let name = user?.name, let surname = user?.surname {
            username = name + " " + surname
        }
        
        view.updateUserHeader(username: username, email: user?.email, imageName: user?.avatarImageName)
    }
    
    func didPressCategory(with id: String) {
        if let index = selectedCategoryIds.index(of: id) {
            selectedCategoryIds.remove(at: index)
        } else {
            selectedCategoryIds.append(id)
        }
        updateView()
    }
}
