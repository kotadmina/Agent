//
//  FormScreenAssembly.swift
//  Agent
//
//  Created by Sher Locked on 02.09.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class FormScreenAssembly: BaseAssembly {
    
    static func configure() {
        let container = defaultContainer()
        
        //register view
        container.register(FormScreenViewInput.self) { r in
            let storyboard = UIStoryboard(name: "FormScreenView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "FormScreenView") as! FormScreenView
            vc.output = r.resolve(FormScreenViewOutput.self, argument: (vc as FormScreenViewInput))!
            return vc
            }.inObjectScope(.transient)
        
        //register presenter
        container.register(FormScreenViewOutput.self) { (r: Resolver, view: FormScreenViewInput) in
            let presenter = FormScreenPresenter()
            presenter.view = view
            presenter.vmf = r.resolve(FormScreenVMFProtocol.self)!
            presenter.interactor = r.resolve(FormScreenInteractorInput.self)!
            presenter.router = r.resolve(FormScreenRouterInput.self, argument: view as! UIViewController)!
            return presenter
            }.inObjectScope(.transient)
        
        //register interactor
        container.register(FormScreenInteractorInput.self) { r in
            let interactor = FormScreenInteractor()
            interactor.categoriesRepository = r.resolve(CategoriesRepositoryProtocol.self)!
            interactor.userRepository = r.resolve(UserRepositoryProtocol.self)!
            return interactor
            }.inObjectScope(.transient)
        
        //register vmf
        container.register(FormScreenVMFProtocol.self) { _ in
            let vmf = FormScreenVMF()
            return vmf
            }.inObjectScope(.transient)
        
        //register router
        container.register(FormScreenRouterInput.self) { (r: Resolver, view: UIViewController) in
            let router = FormScreenRouter()
            router.view = view
            return router
            }.inObjectScope(.transient)
    }
}
