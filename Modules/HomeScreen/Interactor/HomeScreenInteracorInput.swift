//
//  HomeScreenInteracorInput.swift
//  Agent
//
//  Created by Sher Locked on 24.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol HomeScreenInteractorInput {
    
    func getCategories() -> [Category]
    
    func getMissions() -> [Mission]
    
}
