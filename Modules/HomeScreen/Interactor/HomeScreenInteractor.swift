//
//  HomeScreenInteractor.swift
//  Agent
//
//  Created by Sher Locked on 24.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class HomeScreenInteractor: HomeScreenInteractorInput {
    
    var categoriesRepository: CategoriesRepositoryProtocol!
    var missionsRepository: MissionsRepositoryProtocol!
    
    func getCategories() -> [Category] {
       return categoriesRepository.getCategories()
    }
    
    func getMissions() -> [Mission] {
        return missionsRepository.getFeaturedMissions()
    }
    
}
