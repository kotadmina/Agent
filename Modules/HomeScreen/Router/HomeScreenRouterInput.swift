//
//  HomeScreenRouterInput.swift
//  Agent
//
//  Created by Sher Locked on 27.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

protocol HomeScreenRouterInput {
    func openDetailMission(with mission: Mission)
    func openCategory(with category: Category)
}
