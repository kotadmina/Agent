//
//  HomeScreenRouter.swift
//  Agent
//
//  Created by Sher Locked on 27.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class HomeScreenRouter: HomeScreenRouterInput {
    
    weak var view: UIViewController!
    
    func openDetailMission(with mission: Mission) {
        let vc = ModulesAssembly.resolve(type: MissionDetailViewInput.self) as! UIViewController
        ((vc as? MissionDetailView)?.output as? MissionDetailPresenter)?.mission = mission
        vc.hidesBottomBarWhenPushed = true
        view.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openCategory(with category: Category) {
        let vc = ModulesAssembly.resolve(type: CategoryScreenViewInput.self) as! UIViewController
        ((vc as? CategoryScreenView)?.output as? CategoryScreenPresenter)?.category = category
        vc.hidesBottomBarWhenPushed = true
        view.navigationController?.pushViewController(vc, animated: true)
    }
}
