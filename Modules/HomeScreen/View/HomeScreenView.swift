//
//  HomeScreenView.swift
//  Agent
//
//  Created by Sher Locked on 24.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class HomeScreenView: UIViewController, HomeScreenViewInput {
    
    var output: HomeScreenViewOutput!
    
    @IBOutlet weak var verticalScrollView: TrapeziumScrollView!
    @IBOutlet weak var horizontalScrollView: TrapeziumHorizontalScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        output.viewDidLoad()
    }
    
    func configure() {
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = ColorsLibrary.mainDarkViolet
        verticalScrollView.trapeziumDelegate = self
        horizontalScrollView.trapeziumDelegate = self
        horizontalScrollView.showsHorizontalScrollIndicator = false
    }
    
    
    func updateVerticalScroll(viewModels: [TrapeziumVerticalCellViewModel]) {
        let views = viewModels.map { viewModel -> TrapeziumVerticalCell in
            let item = TrapeziumVerticalCell(frame: .zero)
            item.configure(with: viewModel)
            return item
        }
        verticalScrollView.items = views
    }
    
    func updateHorizontalScroll(viewModels: [TrapeziumHorizontalCellViewModel]) {
        let views = viewModels.map { viewModel -> TrapeziumHorizontalCell in
            let item = TrapeziumHorizontalCell(frame: .zero)
            item.configure(with: viewModel)
            return item
        }
        horizontalScrollView.horizontalItems = views
    }

}

extension HomeScreenView: TrapeziumVerticalScrollViewDelegate {
    func verticalItemPressed(with viewModel: TrapeziumVerticalCellViewModel) {
        output.selectCategory(viewModel.id)
    }
}

extension HomeScreenView: TrapeziumHorizontalScrollViewDelegate {
    func horizontalItemPressed(with viewModel: TrapeziumHorizontalCellViewModel) {
        output.selectMission(viewModel.id)
    }
}
