//
//  HomeScreenAssembly.swift
//  Agent
//
//  Created by Sher Locked on 24.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class HomeScreenAssembly: BaseAssembly {
    
    static func configure() {
        let container = defaultContainer()
        
        //Register View
        container.register(HomeScreenViewInput.self) { r in
            let storyboard = UIStoryboard(name: "HomeScreenView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "HomeScreenView") as! HomeScreenView
            vc.output = r.resolve(HomeScreenViewOutput.self, argument: (vc as HomeScreenViewInput))!
            return vc
        }.inObjectScope(.transient)
        
        //Register Interactor
        container.register(HomeScreenInteractorInput.self) { r in
            let interactor = HomeScreenInteractor()
            interactor.categoriesRepository = r.resolve(CategoriesRepositoryProtocol.self)!
            interactor.missionsRepository = r.resolve(MissionsRepositoryProtocol.self)!
            return interactor
        }.inObjectScope(.transient)
        
        //Register Presenter
        container.register(HomeScreenViewOutput.self) { (r: Resolver, view: HomeScreenViewInput) in
            let presenter = HomeScreenPresenter()
            presenter.view = view
            presenter.interactor = r.resolve(HomeScreenInteractorInput.self)!
            presenter.router = r.resolve(HomeScreenRouterInput.self, argument: view as! UIViewController)!
            presenter.vmf = r.resolve(HomeScreenVMFProtocol.self)!
            return presenter
        }.inObjectScope(.transient)
        
        //Register Router
        container.register(HomeScreenRouterInput.self) { (r: Resolver, view: UIViewController) in
            let router = HomeScreenRouter()
            router.view = view
            return router
        }.inObjectScope(.transient)
        
        //Register VMF
        container.register(HomeScreenVMFProtocol.self) { _ in
            let vmf = HomeScreenVMF()
            return vmf
        }.inObjectScope(.transient)
    }

}
