//
//  HomeScreenPresenter.swift
//  Agent
//
//  Created by Sher Locked on 26.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class HomeScreenPresenter: HomeScreenViewOutput {
    
    weak var view: HomeScreenViewInput!
    var vmf: HomeScreenVMFProtocol!
    var interactor: HomeScreenInteractorInput!
    var router: HomeScreenRouterInput!
    var missions: [Mission] = []
    var categories: [Category] = []
    
    func viewDidLoad() {
        categories = interactor.getCategories()
        let viewModels = vmf.createViewModels(with: categories)
        view.updateVerticalScroll(viewModels: viewModels)
        
        missions = interactor.getMissions()
        let horizontalVM = vmf.createHorizontalViewModels(with: missions)
        view.updateHorizontalScroll(viewModels: horizontalVM)
        
    }
    
    func selectMission(_ id: String) {
        //openScreen
        guard let mission = missions.first(where: {$0.id == id}) else {
            return
        }
        router.openDetailMission(with: mission)
    }
    
    func selectCategory(_ id: String) {
        //openCatscreen
        guard let category = categories.first(where: {$0.id == id}) else {
            return
        }
        router.openCategory(with: category) 
    }
    
}
