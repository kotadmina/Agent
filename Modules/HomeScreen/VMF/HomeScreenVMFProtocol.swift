//
//  HomeScreenVMFProtocol.swift
//  Agent
//
//  Created by Sher Locked on 29.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol HomeScreenVMFProtocol {
    func createViewModels(with categories: [Category]) -> [TrapeziumVerticalCellViewModel]
    func createHorizontalViewModels(with missions: [Mission]) -> [TrapeziumHorizontalCellViewModel] 
    
}
