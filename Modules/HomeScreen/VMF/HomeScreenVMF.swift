//
//  HomeScreenVMF.swift
//  Agent
//
//  Created by Sher Locked on 29.07.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class HomeScreenVMF: HomeScreenVMFProtocol {
    func createViewModels(with categories: [Category]) -> [TrapeziumVerticalCellViewModel] {
        var viewModels: [TrapeziumVerticalCellViewModel] = []
        for (index, category) in categories.enumerated() {
            var alignment: NSTextAlignment
            if index % 2 == 0 {
                alignment = .right
            } else {
                alignment = .left
            }
            
            let viewModel = TrapeziumVerticalCellViewModel(name: category.name, imageName: category.imageName, alignment: alignment, textColor: category.textColor, id: category.id)
            viewModels.append(viewModel)
        }
        
        return viewModels
    }
    
    func createHorizontalViewModels(with missions: [Mission]) -> [TrapeziumHorizontalCellViewModel] {
        var viewModels: [TrapeziumHorizontalCellViewModel] = []
        for (index, mission) in missions.enumerated() {
            var backgroundColor: UIColor
            var textColor: UIColor
            if index % 2 == 0 {
                backgroundColor = ColorsLibrary.mainYellow
                textColor = ColorsLibrary.mainDarkViolet
            } else {
                backgroundColor = ColorsLibrary.mainDarkViolet
                textColor = UIColor.white
            }
            
            let viewModel = TrapeziumHorizontalCellViewModel(headerText: mission.headerText,
                                                             imageName: mission.exampleScreenshotName,
                                                             descriptionText: mission.shortDescriptionText,
                                                             headerViewBackgroundColor: backgroundColor,
                                                             textColor: textColor,
                                                             nickname: mission.nickname,
                                                             comment: mission.comment,
                                                             id: mission.id)
            viewModels.append(viewModel)
        }
        
        return viewModels
    }
    
}
