//
//  MissionDetailRouter.swift
//  Agent
//
//  Created by Sher Locked on 07.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class MissionDetailRouter: MissionDetailRouterInput {
    
    weak var view: UIViewController!
    
    func back() {
        view.navigationController?.popViewController(animated: true)
    }
}
