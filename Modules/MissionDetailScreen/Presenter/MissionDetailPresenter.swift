//
//  MissionDetailPresenter.swift
//  Agent
//
//  Created by Sher Locked on 02.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class MissionDetailPresenter: MissionDetailViewOutput {
    
    weak var view: MissionDetailViewInput!
    var mission: Mission?
    var vmf: MissionDetailScreenVMFProtocol!
    var router: MissionDetailRouterInput!
    
    func viewDidLoad() {
        guard let mission = mission else {
            return
        }
        let headerViewModel = vmf.createHeaderViewModels(with: mission)
        view.updateHeader(with: headerViewModel)
        view.updateLabels(greeting: mission.greetings, description: mission.description, header: mission.shortDescriptionText)
        let instructionVM = vmf.createInstructionViewModels(with: mission)
        view.updateInstructionTableView(with: instructionVM)
        let rewardVM = vmf.createRewardViewModels(with: mission)
        view.updateRewardsTableView(with: rewardVM)
    }
    
    func backButtonPressed() {
        router.back()
    }
    
}
