//
//  MissionDetailView.swift
//  Agent
//
//  Created by Sher Locked on 02.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class MissionDetailView: UIViewController, MissionDetailViewInput {
    
    var output: MissionDetailViewOutput!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var missionHeaderView: MissionHeaderView!
    @IBOutlet weak var greetingLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var instructionsTableView: UITableView!
    @IBOutlet weak var screenshotButton: UIButton!
    @IBOutlet weak var instructionTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var rewardLabel: UILabel!
    @IBOutlet weak var rewardsTableView: UITableView!
    @IBOutlet weak var rewardTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var exampleImageView: UIImageView!
    @IBOutlet weak var exampleImageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var exampleHeaderView: UIView!
    @IBOutlet weak var exampleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var missionAvailableLabel: UILabel!
    @IBOutlet weak var readyButton: ReadyAnimatedButton!
    @IBOutlet weak var notReadyButton: UIButton!
    @IBOutlet weak var missionLabel: UILabel!
    @IBOutlet weak var missionHeaderLabel: UILabel!
    
    var instructionViewModels: [InstructionCellViewModel] = []
    var rewardViewModels: [RewardCellViewModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewDidLoad()
        configure()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if #available(iOS 11.0, *) {
            scrollView.contentInset.top = -view.safeAreaInsets.top
        } else {
            scrollView.contentInset.top = -topLayoutGuide.length
        }
        instructionTableViewHeight.constant = instructionsTableView.contentSize.height
        rewardTableViewHeight.constant = rewardsTableView.contentSize.height
        if let exampleImage = exampleImageView.image {
            let imageAspectRatio = exampleImage.size.height / exampleImage.size.width
            let imageHeight = imageAspectRatio * exampleImageView.frame.width
            exampleImageViewHeight.constant = imageHeight
        }
    }
    
    private func configure() {
        self.view.backgroundColor = ColorsLibrary.mainDarkViolet
        greetingLabel.font = FontLibrary.pragmaticaCondLight(size: 24)
        greetingLabel.textColor = UIColor.white
        descriptionLabel.font = FontLibrary.pragmaticaExtraLightReg(size: 13)
        descriptionLabel.textColor = UIColor.white
        setupInstructionTableView()
        setupRewardTableView()
        rewardLabel.font = FontLibrary.pragmaticaCondLight(size: 24)
        rewardLabel.text = "Вознаграждение"
        rewardLabel.textColor = UIColor.white
        let screenshotImage = UIImage(named: "screenshotButton")?.withRenderingMode(.alwaysOriginal)
        screenshotButton.setImage(screenshotImage, for: .normal)
        
        exampleHeaderView.layer.shadowColor = UIColor.gray.cgColor
        exampleHeaderView.layer.shadowOffset = CGSize(width: 0, height: 1)
        exampleHeaderView.layer.shadowOpacity = 0.7
        exampleHeaderView.layer.shadowRadius = 5
        
        exampleImageView.image = UIImage(named: "example_screenshot")
        exampleHeaderView.backgroundColor = ColorsLibrary.fairIvory
        exampleLabel.text = "Пример выполненной миссии:"
        exampleLabel.font = FontLibrary.pragmaticaRegular(size: 18)
        exampleLabel.textColor = ColorsLibrary.mainDarkViolet
        let backImage = UIImage(named: "back_button")?.withRenderingMode(.alwaysOriginal)
        backButton.setImage(backImage, for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 25)
        missionAvailableLabel.text = "*Миссия доступна"
        missionAvailableLabel.textColor = UIColor.white
        missionAvailableLabel.font = FontLibrary.pragmaticaExtraLightReg(size: 10)
        readyButton.backgroundColor = ColorsLibrary.mainYellow
        readyButton.setTitle("ГОТОВ!", for: .normal)
        readyButton.setTitleColor(ColorsLibrary.mainDarkViolet, for: .normal)
        readyButton.titleLabel?.font = FontLibrary.pragmaticaCondRegular(size: 30)
        readyButton.addTarget(self, action: #selector(readyButtonPressed), for: .touchUpInside)
        notReadyButton.backgroundColor = UIColor.clear
        notReadyButton.setTitle("Отложить", for: .normal)
        notReadyButton.setTitleColor(UIColor.white, for: .normal)
        notReadyButton.titleLabel?.font = FontLibrary.pragmaticaCondRegular(size: 24)
        missionLabel.text = "МИССИЯ:"
        missionLabel.textColor = UIColor.white
        missionLabel.font = FontLibrary.pragmaticaCondRegular(size: 30)
        missionHeaderLabel.textColor = UIColor.white
        missionHeaderLabel.font = FontLibrary.pragmaticaCondRegular(size: 30)
    }
    
    private func setupInstructionTableView() {
        instructionsTableView.isScrollEnabled = false
        instructionsTableView.rowHeight = UITableViewAutomaticDimension
        instructionsTableView.estimatedRowHeight = 26
        instructionsTableView.delegate = self
        instructionsTableView.dataSource = self
        let instructionNib = UINib(nibName: "InstructionsTableViewCell", bundle: nil)
        instructionsTableView.register(instructionNib, forCellReuseIdentifier: "InstructionTableViewCell")
        instructionsTableView.separatorStyle = .none
    }
    
    private func setupRewardTableView() {
        rewardsTableView.isScrollEnabled = false
        rewardsTableView.rowHeight = UITableViewAutomaticDimension
        rewardsTableView.estimatedRowHeight = 26
        rewardsTableView.delegate = self
        rewardsTableView.dataSource = self
        let rewardNib = UINib(nibName: "RewardTableViewCell", bundle: nil)
        rewardsTableView.register(rewardNib, forCellReuseIdentifier: "RewardTableViewCell")
        rewardsTableView.separatorStyle = .none
    }
    
    func updateHeader(with viewModel: MissionHeaderViewModel) {
        missionHeaderView.configure(with: viewModel)
    }
    
    func updateLabels(greeting: String, description: String, header: String) {
        greetingLabel.text = greeting
        descriptionLabel.text = description
        missionHeaderLabel.text = "«" + header + "»".uppercased()
    }
    
    func updateInstructionTableView(with viewModels: [InstructionCellViewModel]) {
        instructionViewModels = viewModels
        instructionsTableView.reloadData()
    }
    
    func updateRewardsTableView(with viewModels: [RewardCellViewModel]) {
        rewardViewModels = viewModels
        rewardsTableView.reloadData()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        output.backButtonPressed()
    }
    
    @objc func readyButtonPressed() {
        //add gif
    }
    
}

extension MissionDetailView: UITableViewDelegate {
    
}

extension MissionDetailView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == instructionsTableView {
            return instructionViewModels.count
        } else if tableView == rewardsTableView {
            return rewardViewModels.count
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == instructionsTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InstructionTableViewCell") as! InstructionsTableViewCell
            cell.configure(with: instructionViewModels[indexPath.row])
            return cell
        } else if tableView == rewardsTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RewardTableViewCell") as! RewardTableViewCell
            cell.configure(with: rewardViewModels[indexPath.row])
            return cell
        } else {
            return UITableViewCell()
        }
    }
}

















