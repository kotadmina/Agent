//
//  MissionDetailViewInput.swift
//  Agent
//
//  Created by Sher Locked on 02.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol MissionDetailViewInput: class {
    
    func updateHeader(with viewModel: MissionHeaderViewModel)
    func updateLabels(greeting: String, description: String, header: String)
    func updateInstructionTableView(with viewModels: [InstructionCellViewModel])
    func updateRewardsTableView(with viewModels: [RewardCellViewModel])
    
}
