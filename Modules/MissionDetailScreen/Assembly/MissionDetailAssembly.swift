//
//  MissionDetailAssembly.swift
//  Agent
//
//  Created by Sher Locked on 02.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class MissionDetailAssembly: BaseAssembly {
    
    static func configure() {
        let container = defaultContainer()
        
        //register view
        container.register(MissionDetailViewInput.self) { r in
            let storyboard = UIStoryboard(name: "MissionDetailView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MissionDetailView") as! MissionDetailView
            vc.output = r.resolve(MissionDetailViewOutput.self, argument: (vc as MissionDetailViewInput))!
            return vc
        }.inObjectScope(.transient)
        
        //register presenter
        container.register(MissionDetailViewOutput.self) { (r: Resolver, view: MissionDetailViewInput) in
            let presenter = MissionDetailPresenter()
            presenter.view = view
            presenter.vmf = r.resolve(MissionDetailScreenVMFProtocol.self)!
            presenter.router = r.resolve(MissionDetailRouterInput.self, argument: view as! UIViewController)!
            return presenter
        }.inObjectScope(.transient)
        
        //register interactor
        
        //register vmf
        container.register(MissionDetailScreenVMFProtocol.self) { _ in
            let vmf = MissionDetailScreenVMF()
            return vmf
            }.inObjectScope(.transient)
        
        //register router
        container.register(MissionDetailRouterInput.self) { (r: Resolver, view: UIViewController) in
            let router = MissionDetailRouter()
            router.view = view
            return router
            }.inObjectScope(.transient)
    }
    
}
