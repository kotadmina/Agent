//
//  MissionDetailScreenVMF.swift
//  Agent
//
//  Created by Sher Locked on 06.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class MissionDetailScreenVMF: MissionDetailScreenVMFProtocol {
    
    func createHeaderViewModels(with mission: Mission) -> MissionHeaderViewModel {
        let viewModel = MissionHeaderViewModel(headerImageName: mission.headerImageName)
        return viewModel
    }
    
    func createInstructionViewModels(with mission: Mission) -> [InstructionCellViewModel] {
        var viewModels: [InstructionCellViewModel] = []
        for (index, instruction) in mission.instuctions.enumerated() {
            let images = instruction.images.map { $0.rawValue }
            let viewModel = InstructionCellViewModel(number: String(index + 1),
                                                     text: instruction.text,
                                                     images: images)
            viewModels.append(viewModel)
        }
        return viewModels
    }
    
    func createRewardViewModels(with mission: Mission) -> [RewardCellViewModel] {
        var viewModels: [RewardCellViewModel] = []
        for reward in mission.rewards {
            let viewModel = RewardCellViewModel(rewardText: reward.description)
            viewModels.append(viewModel)
        }
        return viewModels
    }
    
    
}
