//
//  MissionDetailScreenVMFProtocol.swift
//  Agent
//
//  Created by Sher Locked on 06.08.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol MissionDetailScreenVMFProtocol {
    func createHeaderViewModels(with mission: Mission) -> MissionHeaderViewModel
    func createInstructionViewModels(with mission: Mission) -> [InstructionCellViewModel]
    func createRewardViewModels(with mission: Mission) -> [RewardCellViewModel]
}
